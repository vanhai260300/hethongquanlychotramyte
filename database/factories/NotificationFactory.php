<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'articler_id' => 1,
            'title' => $this->faker->sentence($nbWords = 8, $variableNbWords = true) ,
            'content' => $this->faker->text($maxNbChars = 300) ,
        ];
    }
}
