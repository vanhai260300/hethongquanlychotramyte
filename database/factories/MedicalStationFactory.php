<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MedicalStationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $phone = 975746376;
        static $address = 1;
        static $wardy = 9425;
        static $ward = 9425;
        return [
            'name' => 'Tram Y Te '.$wardy++,
            'email' => "tramyte".$address++."@gmail.com",
            'password' => bcrypt('123'),
            'phone_number' => '0'.$phone++,
            'address' => 'Thanh Son'.$address++,
            'province_id' => 51,
            'district_id' => 594, //589-598 (594: Hai Lang)
            'ward_id' => $ward++,
        ];
    }
}
