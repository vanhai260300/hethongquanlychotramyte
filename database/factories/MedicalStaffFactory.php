<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MedicalStaffFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $number = 1;
        static $phone = 329565083;
        return [
            'name' => 'Nguyen Phuong Anh '.$number++,
            'phone_number' => '0'.$phone++,
            'medical_station_id' => 1,
        ];
    }
}
