<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'articler_id' => 1,
            'title' => $this->faker->sentence($nbWords = 8, $variableNbWords = true) ,
            'content' => $this->faker->text($maxNbChars = 500) ,
            'image' => $this->faker->imageUrl(246, 138, 'cats', true, 'Faker', true),
            'view' => 10,
        ];
    }
}
