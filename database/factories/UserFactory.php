<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $cmnd = 10000000;
        static $phone = 376995796;
        static $address = 1;
        return [
            'name' => 'Nguyen Thanh '.$address++,
            'email' => $this->faker->unique()->safeEmail(),
            'birthdate' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'gender' => 2,
            'CMND' => '1'.$cmnd++,
            'phone_number' => $phone++,
            'address' => 'Thanh Son'.$address++,
            'province_id' => 51,
            'district_id' => 594,
            'ward_id' => 9443,
            'medical_station_id' => 23,
            'status' => 2,
            'email_verified_at' => now(),
            'password' => bcrypt('123'),
            'user_id' => 2,
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
