<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisterVaccinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_vaccinations', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('object_id');
            $table->date('date_register');
            $table->integer('injection')->nullable();
            $table->date('last_injection')->nullable();
            $table->integer('type_vaccine')->nullable();
            $table->string('guardian')->nullable();
            $table->string('relationship')->nullable();
            $table->string('phone_number_of_guardian')->nullable();
            $table->string('medical_history')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_vaccinations');
    }
}
