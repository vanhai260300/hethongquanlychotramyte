<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemaneColumnAreacolorToTableQuarantineAthomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quarantine_athomes', function (Blueprint $table) {
            $table->renameColumn("areacolor", "total_date");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quarantine_athomes', function (Blueprint $table) {
//            $table->renameColumn("areacolor", "total_date");
        });
    }
}
