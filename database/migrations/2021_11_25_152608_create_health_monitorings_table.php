<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthMonitoringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_monitorings', function (Blueprint $table) {
            $table->id();
            $table->integer('quarantine_id');
            $table->date('date_moni')->nullable();
            $table->string('temperature')->nullable();
            $table->string('breathing')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->boolean('fever')->nullable();
            $table->boolean('cough')->nullable();
            $table->boolean('difficulty_of_breathing')->nullable();
            $table->boolean('diarrhea')->nullable();
            $table->boolean('chills')->nullable();
            $table->boolean('tired')->nullable();
            $table->boolean('loss_taste')->nullable();
            $table->boolean('suspicious')->nullable();
            $table->string('note')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_monitorings');
    }
}
