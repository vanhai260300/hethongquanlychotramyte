<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('gender');
            $table->string('CMND')->nullable();
            $table->string('phone_number');
            $table->string('address');
            $table->integer('province_id');
            $table->integer('district_id');
            $table->integer('ward_id');
            $table->integer('status')->default(0);
            $table->string('token')->nullable();
            $table->integer('medical_station_id');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->integer("user_id")->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
