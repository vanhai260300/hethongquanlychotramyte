<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMutipleToRegisterVaccinations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_vaccinations', function (Blueprint $table) {
            $table->integer("relationship_of_guardian");
            $table->integer("tsphanve");
            $table->integer("tscovid");
            $table->integer("tssuygiammd");
            $table->integer("benhcaptinh");
            $table->integer("tsmantinh");
            $table->integer("age");
            $table->integer("mangthai");
            $table->text("note")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_vaccinations', function (Blueprint $table) {
            $table -> dropColumn('relationship_of_guardian');
            $table -> dropColumn('tsphanve');
            $table -> dropColumn('tscovid');
            $table -> dropColumn('tssuygiammd');
            $table -> dropColumn('benhcaptinh');
            $table -> dropColumn('tsmantinh');
            $table -> dropColumn('age');
            $table -> dropColumn('mangthai');
            $table -> dropColumn('note');
        });
    }
}
