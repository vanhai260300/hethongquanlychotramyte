<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealthDeclarationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('health_declarations', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('object_id');
            $table->date('declaration_date')->nullable();
            $table->integer('relationship')->nullable();
            $table->boolean('fever');
            $table->boolean('cough');
            $table->boolean('sore_throat');
            $table->boolean('difficulty_of_breathing');
            $table->boolean('diarrhea');
            $table->boolean('tired');
            $table->boolean('loss_taste');
            $table->boolean('got_covid');
            $table->boolean('exposure_covid');
            $table->boolean('ever_had_covid');
            $table->boolean('quarantined');
            $table->text('note')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('health_declarations');
    }
}
