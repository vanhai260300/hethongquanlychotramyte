<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuarantineAthomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quarantine_athomes', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('object_id');
            $table->integer('medical_staff_id')->nullable();
            $table->string('guardian')->nullable();
            $table->string('relationship_of_guardian')->nullable();
            $table->string('relationship')->default(7);
            $table->string('injection')->default(0);
            $table->date('quarantine_date')->nullable();
            $table->integer('update_status')->default(0);
            $table->integer('object_quarantine_id')->nullable();
            $table->text("phone_of_guardian")->nullable();
            $table->integer("areacolor")->nullable();
            $table->integer("province_id")->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quarantine_athomes');
    }
}
