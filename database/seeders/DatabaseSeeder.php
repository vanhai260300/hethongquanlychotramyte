<?php

namespace Database\Seeders;

use App\Models\MedicalStaff;
use App\Models\MedicalStation;
use App\Models\News;
use App\Models\Notification;
use App\Models\ObjectQuarantine;
use App\Models\Relationship;
use App\Models\TypeVaccine;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    //    User::insert(
    //        [
    //            'name' => 'Nguyen Van Hai',
    //            'email' => 'admin@gmail.com',
    //            'birthdate' => '2000-03-26',
    //            'gender' => 2,
    //            'CMND' => '197415914',
    //            'phone_number' => '0376445796',
    //            'address' => 'Thanh Son',
    //            'province_id' => 51,
    //            'district_id' => 594,
    //            'ward_id' => 9443,
    //            'status' => 1,
    //            'medical_station_id' => 1,
    //            'password' => bcrypt('123'),

    //        ]
    //    );
       User::factory(50)->create();
        // MedicalStation::factory(20)->create();
    //    MedicalStaff::factory(5)->create();
    //    Relationship::insert([[
    //            'name' => 'Bố'
    //        ], [
    //            'name' => 'Mẹ'
    //        ], [
    //            'name' => 'Anh'
    //        ], [
    //            'name' => 'Chị'
    //        ], [
    //            'name' => 'Em'
    //        ], [
    //            'name' => 'Ông/Bà'
    //        ], [
    //            'name' => 'Khác'
    //        ]]);
    //    ObjectQuarantine::insert([[
    //            'name' => 'Không xác định'
    //        ],[
    //            'name' => 'F0'
    //        ], [
    //            'name' => 'F1'
    //        ], [
    //            'name' => 'F2'
    //        ], [
    //            'name' => ''
    //        ], [
    //            'name' => 'Nghi ngờ'
    //        ], [
    //            'name' => 'Trở về từ vùng đỏ'
    //        ], [
    //            'name' => 'Trở về từ vùng xanh'
    //        ]
    //    ]);
    //    TypeVaccine::insert([[
    //        'name' => 'Pfizer–BioNTech'
    //    ], [
    //        'name' => 'Moderna'
    //    ], [
    //        'name' => 'ZyCoV-D'
    //    ], [
    //        'name' => 'Convidecia'
    //    ], [
    //        'name' => 'AstraZeneca'
    //    ], [
    //        'name' => 'Sputnik V'
    //    ], [
    //        'name' => 'Novavax'
    //    ], [
    //        'name' => 'Sputnik Light'
    //    ], [
    //        'name' => 'Loại khác'
    //    ]]);
    //    News::factory(20)->create();
    //    Notification::factory(20)->create();
    }
}
