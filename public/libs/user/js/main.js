$( "#messenger" ).click(function() {
    $( "#show-mess" ).toggle();
});
$( "#close-mess" ).click(function() {
    $( "#show-mess" ).hide();
});
$("#collapse-menu").click(function(){
    $("#toggle-menu").toggle();
    var rootStyle = document.documentElement.style;
    rootStyle.setProperty('--width-sidebar', "50px");
    $("#collapse-menu").toggle();
});
$("#toggle-menu").click(function(){
    $("#collapse-menu").toggle();
    var rootStyle = document.documentElement.style;
    rootStyle.setProperty('--width-sidebar', "240px");
    $("#toggle-menu").toggle();
});
