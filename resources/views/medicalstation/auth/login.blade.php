<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('libs/user/imgs/logoboyte.png') }}">

    @include('user.layout.header-libs')
    <title>Đăng Nhập</title>
</head>
<body>
<main>
    <div class="login-container">
        <form action="{{ route('medical_station.auth.login') }}" method="POST">
            @csrf
            <div class="login-box">
                <div class="header-login">
                    <img src="{{ asset('libs/user/imgs/logoSigin.png') }}" alt="" width="80">
                    <div class="text-uppercase text">Ban chỉ đạo Tỉnh Quảng Trị</div>
                    <div class="text-uppercase text">Phòng chống dịch bệnh Covid-19</div>
                </div>
                <div class="main-login">
                    <div class="logo mb-30">
                        <h4 class="text-primary">Quản Lý Thông Tin Y Tế</h4>
                    </div>
                    <div class="content">
                        <p class="content-title">Cán bộ y tế đăng nhập</p>
                        <input class="input-number" type="text" name="email" id="email" placeholder="Nhập Email" value="{{ old('email') }}">
                    </div>
                    <div class="content">
                        <p class="content-title">Mật khẩu</p>
                        <input class="input-number" type="password" name="password" id="password" placeholder="**********">
                    </div>
                    <div class="btn-submit m-2">
                        <input class="btn btn-primary" type="submit" value="Đăng nhập">
                    </div>
                    <div class="login-other">
                        <a href="{{ route('user.auth.register') }}" class="text-primary">Đăng ký tài khoản</a>
                        <div>
                            <a href="{{ route('user.auth.login') }}" class="text-primary">Đăng nhập phía người dùng</a>
                        </div>
                        <p>Hoặc đăng nhập bằng</p>
                        <div class="d-flex justify-content-center login-social">
                            <i class="fab fa-facebook facebook"></i>
                            <i class="fab fa-google google"></i>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</main>
@include('user.layout.footer-libs')
<script type="text/javascript">
    {{--    @if ($errors->any())--}}
    {{--        @foreach ($errors->all() as $item)--}}
    {{--            toastr.success('{{ $item }}', 'Thông báo!');--}}
    {{--        @break--}}
    {{--        @endforeach--}}
    {{--    @endif--}}
</script>
</body>
</html>


