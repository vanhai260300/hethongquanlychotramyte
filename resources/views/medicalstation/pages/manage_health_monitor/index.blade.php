@extends('medicalstation.layout.main',['title' => 'Thông tin theo dõi sức khỏe'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item" aria-current="page">Danh sách đối tượng theo dõi sức khỏe tại nhà</li>
            <li class="breadcrumb-item active" aria-current="page">Thông tin theo dõi sức khỏe</li>
        </ol>
    </nav>
    <div class="content">
        <div class="container-fluid">
            @include("medicalstation.shared._button-back")
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Thông tin theo dõi sức khỏe</h1>
                </div>
                <div class="declaration-form">
                    <div class="">
                        <div class="row my-3">
                            <div class="right-option col-12">
                                <div class="form-group inline-block form-inline label-width mb-10">
                                    <label class="text-bold">Họ và tên: </label>
                                    <span class="inline-block form-inline text-uppercase ms-4">{{ $quanratine_athome->object->name }}</span>
                                </div>
                                <div class="row ">
                                    <div class="col-sm-6 mb-10">
                                        <div class="form-group form-inline gender-box ">
                                            <label class="text-bold">Năm sinh: </label>
                                            <span class="inline-block form-inline ms-4">{{ date('d-m-Y', strtotime($quanratine_athome->object->birthdate)) }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6  ">
                                        <div class="form-group form-inline ">
                                            <label class="text-bold">Giới tính: </label>
                                            <span class="inline-block form-inline">{{ $quanratine_athome->object->gender == 1 ? "Nam" : "Nữ" }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-6  inline-block form-inline label-width mb-10">
                                        <label class="text-bold">Số điện thoại </label>
                                        <span class="inline-block form-inline text-uppercase ms-4">{{ $quanratine_athome->object->phone_number }}</span>
                                    </div>
                                    <div class="form-group col-sm-6  inline-block form-inline mb-10">
                                        <label class="text-bold">CMND / CCCD: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->object->CMND }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-4 col-sm-4  col-xs-12 mb-10">
                                    <div class="form-group  ">
                                        <label class="text-bold ">Tỉnh / thành: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->object->province->_name }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4  col-xs-12 ">
                                    <div class="form-group  ">
                                        <label class="text-bold">Quận / huyện: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->object->district->_name }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4  col-xs-12 ">
                                    <div class="form-group  ">
                                        <label class="text-bold">Phường / xã: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->object->ward->_name }}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group  mb-10 ">
                                <label class="text-bold">Số nhà, phố, tổ dân phố/thôn/đội</label>
                                <span class="inline-block form-inline ms-4">{{ $quanratine_athome->object->address }}</span>
                            </div>
                            <div class="row ">
                                <div class="col-md-4 col-sm-4  col-xs-12 mb-10">
                                    <div class="form-group  ">
                                        <label class="text-bold ">Người dám hộ: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->guardian }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4  col-xs-12 ">
                                    <div class="form-group  ">
                                        <label class="text-bold">Quan hệ: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->relationshipgua->name ?? "" }}</span>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4  col-xs-12 ">
                                    <div class="form-group  ">
                                        <label class="text-bold">Số điện thoại: </label>
                                        <span class="inline-block form-inline ms-4">{{ $quanratine_athome->phone_of_guardian }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group inline-block col-sm-6 form-inline label-width mb-10">
                                    <label class="text-bold">Đối tượng theo dõi sức khỏe: </label>
                                    <span class="inline-block form-inline text-uppercase ms-4">{{ $quanratine_athome->object_quarantine->name ?? "" }}</span>
                                </div>
                                <div class="form-group inline-block col-sm-6 form-inline label-width mb-10">
                                    <label class="text-bold">Ngày bắt đầu: </label>
                                    <span class="inline-block form-inline text-uppercase ms-4">{{ date('d-m-Y', strtotime($quanratine_athome->quarantine_date)) }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group inline-block col-sm-6 form-inline label-width mb-10">
                                    <label class="text-bold"> </label>
                                    <span class="inline-block form-inline text-uppercase ms-4"></span>
                                </div>
                                <div class="form-group inline-block col-sm-6 form-inline label-width mb-10">
                                    <label class="text-bold">Người theo dõi: </label>
                                    <span class="inline-block form-inline text-uppercase ms-4">{{ $quanratine_athome->medicalStaff->name ?? "" }}</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group d-flex col-sm-4 form-inline label-width mb-10">
                                    <label class="text-bold">Trạng thái: </label>
                                    <div class="status_quarantine">

                                        @if($quanratine_athome->status == 0)
                                            <p class="bg-secondary rounded-pill ms-4 pt-0 px-2 fs-6 text-white">Chờ xác nhận</p>
                                        @elseif ($quanratine_athome->status == 1)
                                            <p class="bg-info rounded-pill ms-4 pt-0 px-2 fs-6 text-white">Đang theo dõi</p>
                                        @else
                                            <p class="bg-success rounded-pill ms-4 pt-0 px-2 fs-6 text-white">Hoàn tất</p>
                                        @endif
                                    </div>
                                </div>
                                <div>
                                    @if ($quanratine_athome->status == 0)
                                        <button id="btn_confirm" onclick="confirmObject({{ $quanratine_athome->id }})" class="btn btn-primary">Xác nhận</button>
                                    @endif
                                    <button onclick="changeStatus({{ $quanratine_athome->id }})" {{ $quanratine_athome->healt_monitorings_count >= $quanratine_athome->total_date ? "" : "disabled" }}  class="btn btn-success">Xác nhận hoàn tất theo dõi sức khỏe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="border-top my-3"></div>
                    <div>
                        <h5 class="fw-bold">Bảng theo dõi sức khỏe (Đã theo dõi {{ $quanratine_athome->healt_monitorings_count }} ngày / {{ $quanratine_athome->total_date }} [Dự tính])</h5>
                    </div>
                    <table class="table table-bordered table-hover table-striped inlineEditTable">
                        <thead class="noselect">
                        <tr style="height: 57.5px;">
                            <th scope="col">STT</th>
                            <th scope="col">Thời gian</th>
                            <th scope="col">Nhiệt độ</th>
                            <th scope="col">Nhịp thở</th>
                            <th scope="col">Huyết áp</th>
                            <th scope="col">Sốt</th>
                            <th scope="col">Ho</th>
                            <th scope="col">Khó thở</th>
                            <th scope="col">Tiêu chảy</th>
                            <th scope="col">Ớn lạnh</th>
                            <th scope="col">Mệt mỏi</th>
                            <th scope="col">Mất vị giác</th>
                            <th>Khả nghi</th>
                            <th>Ghi chú</th>
                        </tr>
                        </thead>
                        {{-- {{ dd($quanratine_athome->healtMonitorings) }}--}}
                        <tbody>
                        @foreach($quanratine_athome->healtMonitorings as $key => $healtMonitoring)
                            <tr>
                                <td>
                                    {{ $key+1 }}
                                </td>
                                <td>
                                    {{ date('d/m/Y', strtotime($healtMonitoring->date_moni)) }}
                                </td>
                                <td>
                                    {{ $healtMonitoring->temperature }}
                                </td>
                                <td>
                                    {{ $healtMonitoring->breathing }}
                                </td>
                                <td>
                                    {{ $healtMonitoring->blood_pressure }}
                                </td>
                                <td>
                                    @if($healtMonitoring->fever == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->cough == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->difficulty_of_breathing == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->diarrhea == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->chills == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->tired == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->loss_taste == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>
                                <td>
                                    @if($healtMonitoring->suspicious == 0)
                                        <p class='text-success fw-bold'>BT</p>
                                    @else
                                        <p class='text-danger fw-bold'>Có</p>
                                    @endif
                                </td>

                                <td>
                                    {{ $healtMonitoring->note }}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="number-pages card-footer clearfix">
        {{-- {{ $schedules->links() }}--}}
    </div>
    @push("custom-scripts")
        <script type="text/javascript">
            function getIdMonitor(id) {
                var now = new Date();
                let id_num = $("#id_" + id);
                let tr_td = id_num.parent().parent();
                let id_moni = $("input[name=id]");
                let date_moni_ud = $("#date_moni_ud");
                let temperature_ud = $("#temperature_ud");
                let breathing_ud = $("#breathing_ud");
                let blood_pressure_ud = $("#blood_pressure_ud");
                id_moni.val(id);
                console.log(id_moni.val());
                var today = new Date().toISOString().split('T')[0];
                date_moni_ud.val(today);
                console.log((tr_td.find("td:eq(1)").text().trim()));
                // date_moni_ud.val(tr_td.find("td:eq(1)").text().trim());
                temperature_ud.val(tr_td.find("td:eq(2)").text().trim());
                breathing_ud.val(tr_td.find("td:eq(3)").text().trim());
                blood_pressure_ud.val(tr_td.find("td:eq(4)").text().trim());
                // console.log(tr_td.find("td:eq(1)").text().trim());
            }
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            let datemotni = $(".date_moni");
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            today_2 = yyyy + '-' + mm + '-' + (dd - 2);
            today = yyyy + '-' + mm + '-' + dd;
            datemotni.attr("max", (today));
            datemotni.val(today);
            // datemotni.attr("min", (today));
            function changeStatus(id){
                $.ajax({
                    url: '{{ route("medical_station.quanrantine_athome.update_status_quarantine") }}',
                    method: 'POST',
                    data: {
                        id: id,
                        status: 2,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        if(result == 1){
                            toastr.success("Cập nhật thành công", "Thông báo");
                        } else {
                            toastr.error("Cập nhật thất bại", "Thông báo");
                        }
                    }
                });
                $(".status_quarantine").html(`<p class="bg-success rounded-pill ms-4 pt-0 px-2 fs-6 text-white">Hoàn tất</p>`)
            }
            function confirmObject(id){
                $.ajax({
                    url: '{{ route("medical_station.quanrantine_athome.update_status_quarantine") }}',
                    method: 'POST',
                    data: {
                        id: id,
                        status: 1,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        if(result == 1){
                            toastr.success("Cập nhật thành công", "Thông báo");
                            $("#btn_confirm").remove();
                            $(".status_quarantine").html(`<p class="bg-info rounded-pill ms-4 pt-0 px-2 fs-6 text-white">Đang theo dõi</p>`)
                        } else {
                            toastr.error("Cập nhật thất bại", "Thông báo");
                        }
                    }
                });
            }
        </script>
    @endpush
@endsection
