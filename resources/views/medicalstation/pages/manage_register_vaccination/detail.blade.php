@extends('medicalstation.layout.main',['title' => 'Thông tin đăng ký vaccin chi tiết'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item" aria-current="page">Danh sách đăng ký tiêm vaccin</li>
            <li class="breadcrumb-item active" aria-current="page">Thông tin chi tiết</li>
        </ol>
    </nav>
    <div class="content-container">
        @include("medicalstation.shared._button-back")
        <div class="container_form-detail">
            <div class="row">
                <div class="col-12">
                    <div class="national-brand text-center mb-10">
                        <div class="text-uppercase "><b>Thông tin đăng ký tiêm vaccine</b>
                        </div>
                    </div>
                    <div class="text-center text-uppercase mb-4">
                    </div>
                    <div class="form-group inline-block form-inline label-width mb-10">
                        <label class="text-bold">Họ tên: </label>
                        <span class="inline-block form-inline text-uppercase">{{ $object->name }}</span>
                    </div>
                    <div class="form-group form-inline mb-10">
                        <label class="text-bold">Số hộ chiếu / CMND / CCCD: </label>
                        <span class="inline-block form-inline">{{ $object->CMND }}</span>
                    </div>
                    <div class="row ">
                        <div class="col-md-5 col-sm-5  col-xs-12 mb-10">
                            <div class="form-group form-inline gender-box ">
                                <label class="text-bold">Năm sinh: </label>
                                <span class="inline-block form-inline">{{ date('d-m-Y', strtotime($object->birthdate)) }}</span> </div></div><div class="col-md-3 col-sm-3  col-xs-12 ">
                            <div class="form-group form-inline ">
                                <label class="text-bold">Giới tính: </label>
                                <span class="inline-block form-inline">{{ $object->gender == 1 ? "Nam" : "Nữ" }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group form-inline ">
                                <label class="text-bold"></label>
                                <span class="inline-block form-inline"></span> </div></div>
                    </div>
                    <div class="mb-10 fw-bold"><b>Địa chỉ liên lạc tại</b>
                    </div>
                    <div class="row ">
                        <div class="col-md-4 col-sm-4  col-xs-12 mb-10">
                            <div class="form-group  ">
                                <label class="text-bold">Tỉnh / thành: </label>
                                <span class="inline-block form-inline">{{ $object->province->_name }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Quận / huyện: </label>
                                <span class="inline-block form-inline">{{ $object->district->_prefix." ".$object->district->_name }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Phường / xã: </label>
                                <span class="inline-block form-inline">{{ $object->ward->_name }}</span> </div></div>
                    </div>
                    <div class="form-group  mb-10 ">
                        <label class="text-bold">Số nhà, phố, tổ dân phố/thôn/đội</label>
                        <span class="inline-block form-inline">{{ $object->address }}</span>
                    </div>
                    <div class="row ">
                        <div class="col-md-6 col-sm-6  col-xs-12 mb-10">
                            <div class="form-group inline-block form-inline label-width ">
                                <label class="text-bold">Điện thoại: </label>
                                <span class="inline-block form-inline">{{ $object->phone_number }}</span> </div></div><div class="col-md-6 col-sm-6  col-xs-12 ">
                            <div class="form-group inline-block form-inline label-width ">
                                <label class="text-bold">Email: </label>
                                <span class="inline-block form-inline">{{ $object->email }}</span> </div></div>
                    </div>
                    <div class="row ">
                        <div class="col-md-4 col-sm-4  col-xs-12 mb-10">
                            <div class="form-group  ">
                                <label class="text-bold">Người dám hộ: </label>
                                <span class="inline-block form-inline">{{ $registerVaccin->guardian }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Quan hệ: </label>
                                <span class="inline-block form-inline">{{ $registerVaccin->relationship_guardian->name }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Số điện thoại: </label>
                                <span class="inline-block form-inline">{{ $registerVaccin->phone_number_of_guardian }}</span> </div></div>
                    </div>
                    <div class="row ">
                        <div class="col-md-4 col-sm-4  col-xs-12 mb-10">
                            <div class="form-group  ">
                                <label class="text-bold">Mũi tiêm thứ: </label>
                                <span class="inline-block form-inline">{{ $registerVaccin->injection == 1 ? "Đầu tiên" : "Mũi tiếp theo" }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            @if($registerVaccin->typeVaccin != null)
                                <div class="form-group  ">
                                    <label class="text-bold">Loại đã tiêm: </label>
                                    <span class="inline-block form-inline">{{ $registerVaccin->typeVaccin->name }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Thời gian mũi tiêm gần nhất : </label>
                                <span class="inline-block form-inline">{{ date('d-m-Y', strtotime( $registerVaccin->last_injection)) }}</span> </div></div>
                        @endif
                    </div>
                    <div class="form-group  mb-10 ">
                        <label class="text-bold">Đăng ký lúc: </label>
                        <span class="inline-block form-inline">{{ date('d-m-Y H:i:s', strtotime( $registerVaccin->updated_at)) }}</span>
                    </div>
                    <div>
                        <table class="table table-bordered tableData2">
                            <thead>
                            <tr>
                                <th scope="col" style="width:80%;">Tiền sử bệnh nền</th>
                                <th scope="col" class="text-center">Có</th>
                                <th scope="col" class="text-center">Không</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td >1. Tiền sử phản vệ từ độ 2 trở lên </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->tsphanve == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->tsphanve == 0 ? "checked" : "") }} class="form-check-input"  >
                                </td>
                            </tr>
                            <tr>
                                <td >2. Tiền sử bị COVID-19 trong vòng 6 tháng </td>
                                <td class="text-center"> <input disabled="" {{ ($registerVaccin->tscovid == 1 ? "checked" : "") }} type="Radio" class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->tscovid == 0 ? "checked" : "") }} class="form-check-input"  >
                                </td>
                            </tr>
                            <tr>
                                <td >3. Tiền sử suy giảm miễn dịch, ung thư giai đoạn cuối, cắt lách, xơ gan mất bù ...</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->tssuygiammd == 1 ? "checked" : "") }} class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" {{ ($registerVaccin->tssuygiammd == 0 ? "checked" : "") }} type="radio" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td >4. Bệnh cấp tính</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->benhcaptinh == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->benhcaptinh == 0 ? "checked" : "") }} class="form-check-input" >
                                </td>
                            </tr>
                            <tr>
                                <td >5. Tiền sử bệnh mạn tính, đang tiến triển </td>
                                <td class="text-center"> <input disabled="" {{ ($registerVaccin->tsmantinh == 1 ? "checked" : "") }} type="Radio" class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->tsmantinh == 0 ? "checked" : "") }} class="form-check-input" >
                                </td>
                            </tr>
                            <tr>
                                <td >6. Độ tuổi: ≥65 tuổi</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->age == 1 ? "checked" : "") }} class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input disabled class="form-check-input" {{ ($registerVaccin->age == 0 ? "checked" : "") }} type="radio" >
                                </td>
                            </tr>
                            <tr>
                                <td >7. Đang mang thai, phụ nữ đang nuôi con bằng sữa mẹn</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($registerVaccin->mangthai == 1 ? "checked" : "") }} class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" {{ ($registerVaccin->mangthai == 0 ? "checked" : "") }} type="radio"   disabled>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="{{ URL::previous() }}" class="btn btn-info text-white">Quay lại</a>
            </div>
        </div>
    </div>
@endsection

