@extends('medicalstation.layout.main',['title' => 'Thêm tin tức'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item">Tin tức</li>
            <li class="breadcrumb-item active" aria-current="page">THêm tin tức</li>
        </ol>
    </nav>
    <div id="module9" class="ModuleWrapper">
        <div id="wrapper9" class="passenger-wrapper">
            <h1 class="text-center mtb-20">Thêm tin tức</h1>
        </div>
        <div class="declaration-form px-2">
            <form action="{{ route('medical_station.news.store') }}" class="p-5" method="POST">
                @csrf
                <div class="row">
                    <div class="mb-3 col-12">
                        <label for="title" class="form-label">Tiêu đề<span
                                class="obligatory">(*)</span></label>
                        <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}" placeholder="Nhập tiêu đề">
                    </div>
                </div>
                <div class="mb-3 ">
                    <label for="content" class="form-label">Nội dung</label>
                    <textarea class="form-control" name="content" id="description" cols="" rows="5" placeholder="Nhập nội dung">{{ old("content") }}</textarea>
                </div>
                <div class="text-center">
                    <a href="{{ url()->previous() }}" class="btn btn-info text-white">Quay lại</a>
                    <button type="submit" class="btn btn-primary btn-submit btn-success">Thêm mới</button>
                </div>
            </form>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            createCKEditor();
        </script>
    @endpush
@endsection
