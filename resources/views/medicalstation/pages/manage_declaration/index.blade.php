@extends('medicalstation.layout.main',['title' => 'Quản lý thông tin khai báo y tế'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item active" aria-current="page">Danh sách khai báo y tế</li>
        </ol>
    </nav>
    <div class="content-container">
        <div class="title">Danh sách khai báo y tế<span>{{ count($declarations) }} kết quả</span></div>
        <div class="controls d-flex justify-content-between">
            <div class="row col-12">
                <div class="col-2 d-block">
                    <label for="relationship_of_guardian" class="form-label">.<span
                            class="obligatory"></span></label>
                    <div class="d-flex">
                        <a href="{{ route("medical_station.declaration.export") }}" class="btn btn-success border-radius-0 btn-sm">Xuất dữ liệu
                            <i class="fa fa-download" aria-hidden="true"></i>
                        </a>
                        <a href="{{ route("medical_station.declaration.index") }}" class="btn btn-primary border-radius-0 btn-sm ms-2"><i class="fas fa-retweet"></i></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <form action="{{ route('medical_station.declaration.index') }}" id="formmain" method="GET">
                        <div class="row col-12 d-flex">
                            <div class="col-3">
                                <label for="" class="form-label">Tên đối tượng<span
                                        class="obligatory"></span></label>
                                <input class="form-control form-control-sm" name="name" value="{{ $name ? $name : "" }}" type="text" placeholder="Nhập tên đối tượng">
                            </div>
                            <div class="col-3">
                                <label for="" class="form-label">Số điện thoại<span
                                        class="obligatory"></span></label>
                                <input class="form-control form-control-sm" name="phone_number" value="{{ $phone_number ? $phone_number : "" }}" type="number" placeholder="Nhập số điện thoại">
                            </div>
                            <div class="col-2">
                                <label for="" class="form-label">Nguy cơ<span
                                        class="obligatory"></span></label>
                                <select class="form-select form-select-sm" name="rate_lavel" aria-label="Default select example">
                                    <option value="0" selected>--Chọn--</option>
                                    <option value="1" {{ $rate_lavel == 1 ? "selected" : "" }}>1</option>
                                    <option value="2" {{ $rate_lavel == 2 ? "selected" : "" }}>2</option>
                                    <option value="3" {{ $rate_lavel == 3 ? "selected" : "" }}>3</option>
                                </select>                            </div>
                            <div class="col-2 d-block">
                                <label for="submit" class="form-label">.<span class="obligatory"></span></label>
                                <input class="form-control btn-primary form-control-sm" type="submit" value="Tìm kiếm">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="min-height-300 mt-3">
            <table id="datatableex" class="table table-bordered table-hover table-striped inlineEditTable" >
                <thead class="noselect">
                    <tr>
                        <th scope="col"  data-column-index="1">#</th>
                        <th scope="col" data-column-index="2">Họ tên</th>
                        <th scope="col"  data-column-index="3">Số điện thoại</th>
                        <th scope="col"  data-column-index="4">Năm sinh</th>
                        <th>Giới tính</th>
                        <th scope="col"  data-column-index="5">Số CMND</th>
                        <th scope="col"  data-column-index="6">Khai báo lúc</th>
                        <th>Nguy cơ</th>
                        <th scope="col" class="text-center"  data-column-index="9"></th>
                    </tr>
                </thead>
                <tfoot class="border-top-2">
                    <tr>
                        <th scope="col" data-column-index="1"></th>

                        <th scope="col"  data-column-index="2"><input type="text" class="form-control form-control-sm" value="" placeholder="Tên khách hàng"></th>
                        <th scope="col" data-column-index="3"><input type="text" class="form-control form-control-sm" value="" placeholder="Số điện thoại"></th>
                        <th scope="col" data-column-index="4"></th>
                        <th></th>
                        <th scope="col"  data-column-index="5"><input type="text" class="form-control form-control-sm" value="" placeholder="Nhập CMND"></th>
                        <th scope="col"  data-column-index="6">
                        </th>
                        <th></th>
                        <th scope="col" class="text-center" data-column-index="9"></th>
                    </tr>
                </tfoot>
                <tbody class="border-top-0">
                @foreach($declarations as $key => $declaration)
                    <tr >
                        <td  data-column-index="1">{{ $key+1 }}</td>

                        <td data-column-index="2">{{ $declaration->name }}</td>
                        <td data-column-index="3">{{ $declaration->phone_number }}</td>
                        <td data-column-index="4">{{ date("d/m/Y",strtotime($declaration->birthdate))  }}</td>
                        <td>{{ $declaration->gender == 1 ? "Nam" : "Nữ" }}</td>
                        <td data-column-index="5">{{ $declaration->CMND }}</td>
                        <td data-column-index="6">{{ $declaration->declaration_date." ng trước" }}</td>
{{--                        <td data-column-index="6">{{ (int) round((strtotime(\Carbon\Carbon::now()) - strtotime($declaration->declaration_date))/86400 - 1)." Ngày trước" }}</td>--}}
                        <td>{{ $declaration->rate_lavel }}</td>
                        <td class="text-center" data-column-index="9">
                            <a href="{{ route("medical_station.declaration.detail", $declaration->object_id) }}" class="text-decoration-none text-success" alt="Xem chi tiết">
                                <i class="fas fa-eye"></i>
                            </a>

                            <a href="" alt="Xem chi tiết tờ khai">

                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div></div>
    @push("custom-scripts")
        <script>
            $(document).ready(function() {
                // DataTable
                var table = $('#datatableex').DataTable({
                    searching: true,
                    info: false,
                    initComplete: function () {
                        // Apply the search
                        this.api().columns().every( function () {
                            var that = this;

                            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                        } );
                    },
                    "lengthMenu": [15, 50, 100, 500, 1000, 5000],
                    "language": {
                        "sZeroRecords": "Không tìm thấy dữ liệu",
                        "sInfoEmpty": "Không có dữ liệu nào",
                        "sInfoFiltered": "(được lọc từ tổng sô MAX trong dữ liệu)",
                        "lengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
                        "oPaginate": {
                            "sNext": "Sau",
                            "sPrevious": "Trước"
                        },
                    },
                });

            } );
        </script>
    @endpush
@endsection
