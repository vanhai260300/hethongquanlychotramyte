@extends('medicalstation.layout.main',['title' => 'Quản lý thông tin theo dõi sức khỏe tại nhà'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item active" aria-current="page">Danh sách đối tượng cách ly tại nhà</li>
        </ol>
    </nav>
    <div class="content-container">
        <div class="title">Danh sách đối tượng TDSK tại nhà<span>{{ count($quanrantine_athomes) }} kết quả</span></div>
        <div class="new-register">
            <div>
                <h5 class="fw-bold">Đối tượng đăng ký mới</h5>
                <a href="{{ route("medical_station.quanrantine_athome.confirmmm") }}" class="btn btn-primary btn-sm">xác nhận</a>
            </div>
            <div class=" mt-3">
                <table class="table table-bordered table-hover table-striped inlineEditTable" >
                    <thead class="noselect">
                    <tr>
                        <th scope="col"  data-column-index="1">#</th>
                        <th scope="col" data-column-index="2">Họ tên</th>
                        <th scope="col"  data-column-index="3">Số điện thoại</th>
                        <th scope="col"  data-column-index="4">Địa chỉ</th>
                        <th>Đối tượng</th>
                        <th scope="col"  data-column-index="5">đăng ký lúc</th>
                        <th class="text-center">TT</th>
                        <th scope="col" class="text-center"  data-column-index="9"></th>
                    </tr>
                    </thead>
                    <tfoot class="border-top-2">
                    <tr>
                        <th scope="col" data-column-index="1"></th>
                        <th scope="col"  data-column-index="2"><input type="text" class="form-control form-control-sm" value="" placeholder="Tên khách hàng"></th>
                        <th scope="col" data-column-index="3"><input type="text" class="form-control form-control-sm" value="" placeholder="Số điện thoại"></th>
                        <th scope="col" data-column-index="4"><input type="text" class="form-control form-control-sm" value="" placeholder="Nhập địa chỉ"></th>
                        <th scope="col"  data-column-index="6">
                        </th>
                        <th></th>
                        <th></th>
                        <th scope="col" class="text-center" data-column-index="9"></th>
                    </tr>
                    </tfoot>
                    <tbody class="border-top-0">
                    @foreach($quanrantine_athomes as $key => $quanrantine_athome)
                        @if($quanrantine_athome->status == 0)
                            <tr >
                                <td  data-column-index="1">{{ $key+1 }}</td>
                                <td data-column-index="2">{{ $quanrantine_athome->object->name }}</td>
                                <td data-column-index="3">{{ $quanrantine_athome->object->phone_number }}</td>
                                <td data-column-index="4">{{ $quanrantine_athome->object->address  }}</td>
                                <td>{{ $quanrantine_athome->object_quarantine->name ?? "" }}</td>
                                <td data-column-index="5">
                                    <div>
                                        {{ $quanrantine_athome->quarantine_date }}
                                    </div>
                                </td>
                                @if($quanrantine_athome->status == -1)
                                    <td class="text-center"><small class="pt-0 px-2 text-danger"><i class="fas fa-circle"></i></small></td>
                                @endif
                                @if($quanrantine_athome->status == 0)
                                    <td class="text-center"><small class="pt-0 px-2 text-secondary"><i class="fas fa-circle"></i></small></td>
                                @elseif ($quanrantine_athome->status == 1)
                                    <td class="text-center"><small class="pt-0 px-2 text-info"><i class="fas fa-circle"></i> </small></td>
                                @elseif($quanrantine_athome->status == 2)
                                    <td class="text-center"><small class="pt-0 px-2 text-success"><i class="fas fa-circle"></i> </small></td>
                                @endif
                                <td class="text-center" data-column-index="9">
                                    <a title="Xác nhận" onclick="confirmObject({{ $quanrantine_athome->id }})" class="text-decoration-none text-primary" alt="Xem chi tiết">
                                        <i class="fa-solid fa-check"></i>
                                    </a>
                                    <a title="Xem chi tiết" href="{{ route("medical_station.quanrantine_athome.detail", $quanrantine_athome->id) }}" class="text-decoration-none text-success" alt="Xem chi tiết">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                        <a onclick="NotiReject({{ $quanrantine_athome->id }})" class="text-decoration-none text-danger">
                                            <i class="fa-solid fa-xmark"></i>
                                        </a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
        <div class="controls mt-5">
            <h5 class="fw-bold">Đối tượng đang trong quá trình theo dõi</h5>
            <div class="row col-12">
                <div class="col-md-12">
                    <form action="{{ route('medical_station.quanrantine_athome.index') }}" id="formmain" method="GET">
                        <div class="row col-12 d-flex">
                            <div class="col-3">
                                <label for="" class="form-label">Tên đối tượng<span
                                        class="obligatory"></span></label>
                                <input class="form-control form-control-sm" name="name" value="{{ $name ? $name : "" }}" type="text" placeholder="Nhập tên đối tượng">
                            </div>
                            <div class="col-2">
                                <label for="" class="form-label">Số điện thoại<span
                                        class="obligatory"></span></label>
                                <input class="form-control form-control-sm" name="phone_number" value="{{ $phone_number ? $phone_number : "" }}" type="number" placeholder="Nhập số điện thoại">
                            </div>
                            <div class="col-2">
                                <label for="" class="form-label">Chọn loại đối tượng<span
                                        class="obligatory"></span></label>
                                <select class="form-select form-select-sm" name="object_quarantine" aria-label="Default select example">
                                    <option value="0" selected>--Chọn loại đối tượng--</option>
                                    @foreach ($object_quarantines as $key => $object_quarantine)
                                        <option value="{{ $object_quarantine->id }}" {{ $object_quarantine_id == $object_quarantine->id ? "selected" : "" }}>{{ $object_quarantine->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="" class="form-label">Chọn người theo dõi<span
                                        class="obligatory"></span></label>
                                <select class="form-select form-select-sm" name="medical_staff" aria-label="Default select example">
                                    <option value="0" selected>--Chọn người theo dõi--</option>
                                    <option value="9999999" {{ $medical_staff_id == 9999999 ? "selected" : "" }}>Chưa có người theo dõi</option>
                                    @foreach ($mediccal_staffs as $key => $mediccal_staff)
                                        <option value="{{ $mediccal_staff->id }}" {{ $medical_staff_id == $mediccal_staff->id ? "selected" : "" }}>{{ $mediccal_staff->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-2 d-block">
                                <label for="submit" class="form-label">.<span class="obligatory"></span></label>
                                <input class="form-control btn-primary form-control-sm" type="submit" value="Tìm kiếm">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="py-3"> <strong>Trạng thái</strong>
                <a class="pt-0 px-2 text-danger"><i class="fas fa-circle"></i> Đã hủy</a>
                <a class="pt-0 px-2 text-secondary"><i class="fas fa-circle"></i> Chờ xác nhận</a>
                <a class="pt-0 px-2 text-info"><i class="fas fa-circle"></i> Đang theo dõi</a>
                <a class="pt-0 px-2 text-success"><i class="fas fa-circle"></i> Hoàn tất</a>
            </div>
        </div>
        <div>
            <div class="min-height-300 mt-3">
                <table id="datatableex" class="table table-bordered table-hover table-striped inlineEditTable" >
                    <thead class="noselect">
                    <tr>
                        <th scope="col"  data-column-index="1">#</th>
                        <th scope="col" data-column-index="2">Họ tên</th>
                        <th scope="col"  data-column-index="3">Số điện thoại</th>
                        <th scope="col"  data-column-index="4">Địa chỉ</th>
                        <th>Đối tượng</th>
                        <th scope="col"  data-column-index="5">Bắt đầu lúc</th>
                        <th>Người theo dõi</th>
                        <th class="text-center">TT</th>
                        <th scope="col" class="text-center"  data-column-index="9"></th>
                    </tr>
                    </thead>
                    <tfoot class="border-top-2">
                    <tr>
                        <th scope="col" data-column-index="1"></th>

                        <th scope="col"  data-column-index="2"><input type="text" class="form-control form-control-sm" value="" placeholder="Tên khách hàng"></th>
                        <th scope="col" data-column-index="3"><input type="text" class="form-control form-control-sm" value="" placeholder="Số điện thoại"></th>
                        <th scope="col" data-column-index="4"><input type="text" class="form-control form-control-sm" value="" placeholder="Nhập địa chỉ"></th>
                        <th scope="col"  data-column-index="5"></th>
                        <th scope="col"  data-column-index="6">
                        </th>
                        <th></th>
                        <th></th>
                        <th scope="col" class="text-center" data-column-index="9"></th>
                    </tr>
                    </tfoot>
                    <tbody class="border-top-0">
                    @foreach($quanrantine_athomes as $key => $quanrantine_athome)
                        <tr >
                            <td  data-column-index="1">{{ $key+1 }}</td>
                            <td data-column-index="2">{{ $quanrantine_athome->object->name }}</td>
                            <td data-column-index="3">{{ $quanrantine_athome->object->phone_number }}</td>
                            <td data-column-index="4">{{ $quanrantine_athome->object->address  }}</td>
                            <td>{{ $quanrantine_athome->object_quarantine->name ?? "" }}</td>
                            <td data-column-index="5">
                                <div>
                                    {{ $quanrantine_athome->quarantine_date }}
                                </div>
                                <div class="text-center">
                                    <strong>({{ $quanrantine_athome->healt_monitorings_count.'/'.$quanrantine_athome->total_date }})</strong>
                                </div>
                            </td>
                            <td class="position-relative">
                                <a class="text-primary btn-show-form pe-auto">{{ $quanrantine_athome->medicalStaff->name ?? "Chon" }}</a>
                                <div class="form-popup position-absolute form-medical-staff">
                                    <form action="" class="form-container bg-white p-3 shadow-lg bg-body rounded">
                                        <div class="modal-header bg-gradient p-2">
                                            <h6 class="modal-title text-white">Chọn người theo dõi</h6>
                                        </div>
                                        <select class="form-select col-8 form-select-sm my-2 medical_staff" name="medical_staff" style="width: 300px;" aria-label="Default select example">
                                            <option disabled selected>-- Chọn người theo dõi --</option>
                                            @foreach ($mediccal_staffs as $key => $mediccal_staff)
                                                <option value="{{ $mediccal_staff->id }}">{{ $mediccal_staff->name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="id_quaranti" class="quarantine_athome" value="{{ $quanrantine_athome->id }}">
                                        <div class="d-flex justify-content-around my-2">
                                            <a type="submit" class="btn btn-primary btn-submit-form btn-sm bg-gradient text-white pe-auto"><i class="fas fa-check"></i></a>
                                            <a type="button" class="btn cancel text-black btn-sm pe-auto" onclick="closeForm()"><i class="fas fa-times"></i></a>
                                        </div>

                                    </form>
                                </div>
                            </td>
                            @if($quanrantine_athome->status == -1)
                                <td class="text-center"><small class="pt-0 px-2 text-danger"><i class="fas fa-circle"></i></small></td>
                            @endif
                            @if($quanrantine_athome->status == 0)
                                <td class="text-center"><small class="pt-0 px-2 text-secondary"><i class="fas fa-circle"></i></small></td>
                            @elseif ($quanrantine_athome->status == 1)
                                <td class="text-center"><small class="pt-0 px-2 text-info"><i class="fas fa-circle"></i> </small></td>
                            @elseif($quanrantine_athome->status == 2)
                                <td class="text-center"><small class="pt-0 px-2 text-success"><i class="fas fa-circle"></i> </small></td>
                            @endif
                            <td class="text-center" data-column-index="9">
                                <a href="{{ route("medical_station.quanrantine_athome.detail", $quanrantine_athome->id) }}" class="text-decoration-none text-success" alt="Xem chi tiết">
                                    <i class="fas fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <div></div>
    @push("custom-scripts")
        <script>
            $(document).ready(function() {
                var table = $('#datatableex').DataTable({
                    searching: true,
                    info: false,
                    initComplete: function () {
                        // Apply the search
                        this.api().columns().every( function () {
                            var that = this;

                            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                        } );
                    },
                    "lengthMenu": [10,20, 50, 100, 500, 1000],
                    "language": {
                        "sZeroRecords": "Không tìm thấy dữ liệu",
                        "sInfoEmpty": "Không có dữ liệu nào",
                        "sInfoFiltered": "(được lọc từ tổng sô MAX trong dữ liệu)",
                        "lengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
                        "oPaginate": {
                            "sNext": "Sau",
                            "sPrevious": "Trước"
                        },
                    },
                });
            } );

            $(".btn-show-form").click(function (re){
                $(".form-medical-staff").hide();
                $(this).parents("tr").find('.form-medical-staff').show();
            });

            $(".btn-submit-form").click(function (re){
                let parents = $(this).parents("tr");
                medical_staff_id = parents.find('.medical_staff').val();
                quarantine_id = parents.find('.quarantine_athome').val();
                medical_staff_text = parents.find('.medical_staff :selected').text();
                $(".form-medical-staff").hide();
                parents.find('.btn-show-form').text(medical_staff_text);
                $.ajax({
                    url: '{{ route("medical_station.quanrantine_athome.update_medical_staff") }}',
                    method: 'POST',
                    data: {
                        id: quarantine_id,
                        medical_staff_id: medical_staff_id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        if(result == 1){
                            toastr.success("Thêm thành công", "Thông báo");
                        } else {
                            toastr.error("Thêm thất bại", "Thông báo");
                        }
                    }
                });
            });

            function NotiReject(id){
                Swal.fire({
                    icon: 'warning',
                    title: 'Bạn chắc chắn muốn hủy?',
                    text: 'Hủy bản đăng ký này!',
                    showCancelButton: true,
                    confirmButtonText: '<a style = "Color:White;" onclick="rejectObject(' + id + ')">Xóa</a>',
                    cancelButtonText: 'Quay lại'
                });
            }

            function confirmObject(id){
                console.log(id);
                $.ajax({
                    url: '{{ route("medical_station.quanrantine_athome.update_status_quarantine") }}',
                    method: 'POST',
                    data: {
                        id: id,
                        status: 1,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        if(result == 1){
                            toastr.success("Cập nhật thành công", "Thông báo");
                            location.reload()
                        } else {
                            toastr.error("Cập nhật thất bại", "Thông báo");
                        }
                    }
                });
            }

            function rejectObject(id){
                $.ajax({
                    url: '{{ route("medical_station.quanrantine_athome.update_status_quarantine") }}',
                    method: 'POST',
                    data: {
                        id: id,
                        status: -1,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        if(result == 1){
                            toastr.success("Hủy thành công", "Thông báo");
                            location.reload()
                        } else {
                            toastr.error("Hủy thất bại", "Thông báo");
                        }
                    }
                });
            }

            function closeForm() {
                $(".form-medical-staff").hide();
            }
        </script>
    @endpush
@endsection
