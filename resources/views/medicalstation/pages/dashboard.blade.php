@extends('medicalstation.layout.main',['title' => 'Trang Chủ'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item active" aria-current="page">Tổng quan</li>
        </ol>
    </nav>
    <div class="content-container">
        <div>
            <h2 class="title my-3">Tổng quan</h2>
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info bg-gradient text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">150</h3>
                            <p class="fw-bold">Số lượng nhân viên y tế</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info bg-gradient text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">200</h3>
                            <p class="fw-bold">Số lượng đối tượng đang quản lý</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info bg-gradient text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">1000</h3>
                            <p class="fw-bold">Thông báo</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info bg-gradient text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">500</h3>
                            <p class="fw-bold">Tin tức</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <h2 class="title my-3">Thống kê số lượng khai báo y tế</h2>
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">700</h3>
                            <p class="fw-bold">Đã khai báo</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">504</h3>
                            <p class="fw-bold">Nguy cơ cấp độ 1</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">450</h3>
                            <p class="fw-bold">Nguy cơ cấp độ 2</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-danger text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">110</h3>
                            <p class="fw-bold">Nguy cơ cấp độ 3</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-4">
            <h2 class="title my-3">Thống kê số lượng đăng ký tiêm vaccin</h2>
            <div class="row">
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">700</h3>
                            <p class="fw-bold">Tất cả</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">132</h3>
                            <p class="fw-bold">Tiêm mũi đầu tiên</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">600</h3>
                            <p class="fw-bold">Tiêm mũi tiếp theo</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-4" >
            <h2 class="title my-3">Thống kê số lượng theo dõi y tế tại nhà</h2>
            <div class="row">
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">300</h3>
                            <p class="fw-bold">Tất cả</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">200</h3>
                            <p class="fw-bold">Hoàn tất theo dõi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-6">
                    <!-- small box -->
                    <div class="small-box bg-warning text-white p-2 rounded">
                        <div class="inner">
                            <h3 class="fw-bold">100</h3>
                            <p class="fw-bold">Đang theo dõi</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Xem thêm <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="">
        </div>

    </div>
    <div class="footer-content d-flex align-items-center justify-content-end">
        <div class="pd-t_50 mr-10">
        </div>
        <div class="btn-group dropup">
        </div>
    </div>
@endsection
