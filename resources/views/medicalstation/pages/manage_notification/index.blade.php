@extends('medicalstation.layout.main',['title' => 'Quản lý thông báo'])
@section('content')
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Trang chủ</li>
            <li class="breadcrumb-item active" aria-current="page">Danh sách thông báo</li>
        </ol>
    </nav>
    <div class="content-container">
        <div class="title">Danh sách thông báo<span>{{ count($notifications) }} kết quả</span></div>
        <div class="controls d-flex justify-content-between">
            <div class="row col-12">
                <div class="col-2 d-block">
                    <label for="relationship_of_guardian" class="form-label">.<span
                            class="obligatory"></span></label>
                    <div class="d-flex">
                        <a href="{{ route("medical_station.notification.create") }}" class="btn btn-primary border-radius-0 btn-sm ms-2">Thêm thông báo</a>
                    </div>
                </div>
                <div class="col-1 d-block">
                    <label for="relationship_of_guardian" class="form-label">.<span
                            class="obligatory"></span></label>
                    <div class="d-flex">
                        <a href="{{ route("medical_station.notification.index") }}" class="btn btn-primary border-radius-0 btn-sm ms-2"><i class="fas fa-retweet"></i></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <form action="{{ route('medical_station.notification.index') }}" id="formmain" method="GET">
                        <div class="row col-12 d-flex">
                            <div class="col-3">
                                <label for="" class="form-label">Tiêu đề thông báo<span
                                        class="obligatory"></span></label>
                                <input class="form-control form-control-sm" name="name" value="" type="text" placeholder="Nhập tên đối tượng">
                            </div>
                            <div class="col-4">
                                <label for="" class="form-label">Nội dung thông báo<span
                                        class="obligatory"></span></label>
                                <input class="form-control form-control-sm" name="phone_number" value="" type="text" placeholder="Nhập số điện thoại">
                            </div>
                            <div class="col-2 d-block">
                                <label for="submit" class="form-label">.<span class="obligatory"></span></label>
                                <input class="form-control btn-primary form-control-sm" type="submit" value="Tìm kiếm">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="min-height-300 mt-3">
            <table id="datatableex" class="table table-bordered table-hover table-striped inlineEditTable" >
                <thead class="noselect">
                <tr>
                    <th scope="col"  data-column-index="1">#</th>
                    <th scope="col" data-column-index="2">Tiêu đề</th>
                    <th scope="col"  data-column-index="3">Nội dung</th>
                    <th scope="col" class="text-center"  data-column-index="9">Thao tác</th>
                </tr>
                </thead>
                <tfoot class="border-top-2">
                <tr>
                    <th scope="col" data-column-index="1"></th>
                    <th scope="col"  data-column-index="2"><input type="text" class="form-control form-control-sm" value="" placeholder="Nhập tiêu đề"></th>
                    <th scope="col" data-column-index="3"><input type="text" class="form-control form-control-sm" value="" placeholder="Nhập nội dung"></th>
                    <th scope="col" class="text-center" data-column-index="9"></th>
                </tr>
                </tfoot>
                <tbody class="border-top-0">
                @foreach($notifications as $key => $new)
                    <tr >
                        <td  data-column-index="1">{{ $key+1 }}</td>
                        <td data-column-index="2">{{ $new->title }}</td>
                        <td data-column-index="3">{{ $new->content }}</td>
                        <td class="text-center" data-column-index="9">
                            <a href="{{ route("medical_station.notification.show", $new->id) }}" class="text-decoration-none text-success" alt="Xem chi tiết">
                                <i class="fas fa-eye"></i>
                            </a>
                            <a href="" class="text-info">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="" class="text-danger">
                                <i class="fas fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div></div>
    @push("custom-scripts")
        <script>
            $(document).ready(function() {
                // DataTable
                var table = $('#datatableex').DataTable({
                    searching: true,
                    info: false,
                    initComplete: function () {
                        // Apply the search
                        this.api().columns().every( function () {
                            var that = this;

                            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                        } );
                    },
                    "lengthMenu": [10, 50, 100, 500, 1000, 5000],
                    "language": {
                        "sZeroRecords": "Không tìm thấy dữ liệu",
                        "sInfoEmpty": "Không có dữ liệu nào",
                        "sInfoFiltered": "(được lọc từ tổng sô MAX trong dữ liệu)",
                        "lengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
                        "oPaginate": {
                            "sNext": "Sau",
                            "sPrevious": "Trước"
                        },
                    },
                });

            } );
        </script>
    @endpush
@endsection
