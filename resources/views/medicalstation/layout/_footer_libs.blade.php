<script src="{{ asset('libs/user/bootstraps/js/jquery.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="{{ asset('libs/user/bootstraps/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('libs/user/bootstraps/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('libs/user/js/main.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/29.2.0/classic/ckeditor.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    function createCKEditor(){
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .catch( error => {
                console.error( error );
            } );
    }
</script>
<script type="text/javascript">
    @if(session('success'))
    toastr.success("{{ session('success') }}", "Thông báo");
    @endif
    @if(session('notification'))
    toastr.info("{{ session('notification') }}", "Thông báo");
    @endif
    @if(session('error'))
    toastr.error("{{ session('error') }}", "Thông báo");
    @endif
    @if($errors-> any())
    @foreach($errors->all() as $item)
    toastr.error('{{ $item }}', 'Thông báo!');
    @break
    @endforeach
    @endif
</script>
