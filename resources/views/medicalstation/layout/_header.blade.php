<div class="header-container">
    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="d-flex align-items-center">
                <div class="img-logo">
                    <img src="../assets/imgs/logoSigin.png" alt="" width="90px">
                </div>
                <div class="header-text text-uppercase">
                    BAN CHỈ ĐẠO TỈNH QUẢNG TRỊ<br>
                    Hệ thống thông tin quản lý thông tin trạm y tế<br>
                </div>
            </div>
            <div class="d-flex align-items-center">
                <div class="icon-notif">
                    <i class="fas fa-bell"></i>
                </div>
                <div class="name-admin d-flex text-uppercase align-items-center">
                    <div class="avatar">
                        H
                    </div>
                    <div class="full-name">
                        <a class="nav-link dropdown-toggle text-uppercase fw-bold" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Auth::guard("medical_station")->user()->name }}
                        </a>
                        <ul class="dropdown-menu sub_menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item fs-6 text-capitalize" href="#">Trang Cá Nhân</a></li>
                            <li><a class="dropdown-item fs-6 text-capitalize" href="{{ route("medical_station.logout") }}">Đăng xuất</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
