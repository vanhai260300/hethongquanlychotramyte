<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('libs/user/imgs/logoboyte.png') }}">
    @include('medicalstation.layout._header_libs')
    <title>{{ $title ?? 'Y Tế' }}</title>
</head>
<body>
    <header>
        @include('medicalstation.layout._header')
    </header>

    <nav>
        @include('medicalstation.layout._navbar')
    </nav>

    <main>
        @yield("content")
    </main>

    @include('medicalstation.layout._footer_libs')
    @stack('custom-scripts')
</body>
</html>
