<div class="sidebar-container">
    <ul class="navbar-nav-root">
        <li class="menu_item menu-item-group py-2 {{ Route::is('medical_station.dashboard.index') ? 'active' : '' }}" >
            <a href="{{ route("medical_station.dashboard.index") }}"  class="ant-menu-submenu-title">
                <i class="icon fas fa-tachometer-alt"></i>
                <span class="text ml-5">Tổng quan</span>
            </a>
        </li>
        @if(Auth::guard("medical_station")->user()->role == 1)
        <li class="menu_item menu-item-group py-2 {{ Route::is('medical_station.medical-station.index') ? 'active' : '' }}">
            <a href="{{ route("medical_station.medical-station.index") }}" class="ant-menu-submenu-title ">
                <i class="fas fa-clinic-medical"></i>
                <span class="text ml-5">QL trạm y tế</span>
            </a>
        </li>
        @endif
        <li class="menu_item menu-item-group py-2 {{ Route::is('medical_station.medical-staff.index') ? 'active' : '' }}">
            <a href="{{ route("medical_station.medical-staff.index") }}" class="ant-menu-submenu-title ">
                <i class="icon fa fas fa-user-md"></i>
                <span class="text ml-5">QL nhân viên</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2">
            <a href="" class="ant-menu-submenu-title ">
                <i class="icon fa fas fa-user"></i>
                <span class="text ml-5">QL đối tượng</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2 {{ (Route::is('medical_station.declaration.index') || Route::is('medical_station.declaration.detail')) ? 'active' : '' }}" >
            <a href="{{ route("medical_station.declaration.index") }}" class="ant-menu-submenu-title">
                <i class="fas fa-heartbeat"></i>
                <span class="text ml-5">QL khai báo y tế</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2 {{ (Route::is('medical_station.quanrantine_athome.index') || Route::is('medical_station.quanrantine_athome.detail')) ? 'active' : '' }}">
            <a href="{{ route("medical_station.quanrantine_athome.index") }}" class="ant-menu-submenu-title ">
                <i class="fas fa-house-user"></i>
                <span class="text ml-5">QL đối tượng TDSK</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2 {{ (Route::is('medical_station.vaccination.index') || Route::is('medical_station.vaccination.detail')) ? 'active' : '' }}">
            <a href="{{ route("medical_station.vaccination.index") }}" class="ant-menu-submenu-title">
                <i class="fas fa-syringe"></i>
                <span class="text ml-5">QL thông tin tiêm vaccin</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2 {{ (Route::is('medical_station.news.index') || Route::is('medical_station.news.create')) ? 'active' : '' }}">
            <a href="{{ route("medical_station.news.index") }}" class="ant-menu-submenu-title">
                <i class="fas fa-newspaper"></i>
                <span class="text ml-5">QL tin tức</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2 {{ (Route::is('medical_station.notification.index') || Route::is('medical_station.notification.create')) ? 'active' : '' }}">
            <a href="{{ route("medical_station.notification.index") }}" class="ant-menu-submenu-title">

                <i class="fas fa-bell"></i>
                <span class="text ml-5">QL thông báo</span>
            </a>
        </li>
        <li class="menu_item menu-item-group py-2" id="toggle-menu">
            <a class="ant-menu-submenu-title">
                <i class="fas fa-bars"></i>
            </a>
        </li>

        <div class="icon-collapse py-2" id="collapse-menu">
            <i class="fas fa-long-arrow-alt-left"></i>
        </div>

    </ul>
</div>
