<div class="header_container">
    <div class="container-fluid ">
        <div class="header_box" style="background-image: url({{ asset('libs/user/imgs/banner_vi_01945.png') }});">
            <div class="row">
                <div class="col-6">
                    <div class="header_box_left">
                        <div class="mr-10">
                            <a href="/">
                                <img src="{{ asset('libs/user/imgs/logoboyte.png') }}" alt="" width="80">
                            </a>
                        </div>

                        <div class="heading-title">
                            <div>Ban chỉ đạo Tỉnh Quảng Trị</div>
                            <div></div>
                            <div>Hệ thống quản lý thông tin y tế</div>
                        </div>
                    </div>
                </div>
                <div class="col-6 text-end">
                    <img src="{{ asset('libs/user/imgs/logo-groups.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
