<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('libs/user/imgs/logoboyte.png') }}">
    @include('user.layout.header-libs')
    <title>{{ $title ?? 'Y Tế' }}</title>
</head>
<body>
    <header>
        @include('user.layout.header')
    </header>
    <nav>
        @include('user.layout.navbar')
    </nav>
    <main>
        @yield('content')
    </main>
    <footer>
        @include('user.layout.footer')
    </footer>
    @include('user.layout.contact')
@include('user.layout.footer-libs')
    @stack('custom-scripts')
</body>
</html>
