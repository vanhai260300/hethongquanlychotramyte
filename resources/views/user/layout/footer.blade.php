<div class="footer padding-TB">
    <div class="container footer_container">
        <div class="row">
            <div class="col-12">
                <div class="footer-left d-flex row">
                    <div class="d-flex col-md-4 align-items-center flex-fill">
                        <img height="60px" src="{{ asset('libs/user/imgs/tttt-logo.svg') }}">
                        <span class="organization-name" style="margin-left: 16px">
                          <div> BỘ THÔNG TIN </div>
                          <div> VÀ TRUYỀN THÔNG </div>
                        </span>
                    </div>
                    <div class="d-flex col-md-4 align-items-center flex-fill">
                        <img height="60px" src="{{ asset('libs/user/imgs/byt-logo.svg') }}">
                        <span class="organization-name" style="margin-left: 16px">BỘ Y TẾ</span>
                    </div>
                    <div class="flex-fill col-md-4 mb-3 mb-md-0">
                        <div class="panel-title">VẬN HÀNH BỞI: </div>
                        <div class="d-flex align-items-center">
                            <img height="40px" src="{{ asset('libs/user/imgs/logo-ttpc-covid.svg') }}">
                            <div class="organization-name" style="margin-left: 12px">
                                <div>TRUNG TÂM CÔNG NGHỆ</div>
                                <div>PHÒNG CHỐNG DỊCH COVID-19 QUỐC GIA</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
