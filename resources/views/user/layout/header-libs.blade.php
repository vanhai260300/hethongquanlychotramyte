<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

<link rel="stylesheet" href="{{ asset('libs/user/bootstraps/css/bootstrap.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<link rel="stylesheet" href="{{ asset('libs/user/css/base.css') }}">
<link rel="stylesheet" href="{{ asset('libs/user/css/login.css') }}">
<link rel="stylesheet" href="{{ asset('libs/user/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('libs/user/css/responsive.css') }}">
