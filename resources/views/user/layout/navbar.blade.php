
<div class="register-translation m-1">
    <div class="register-translation-top d-flex">
            <div class="navbar_container container">
                <nav class="navbar navbar-expand-lg navbar-light ">
                    <div class="container">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase text-white fw-bold" aria-current="page" href="{{ route("user.home") }}">Trang Chủ</a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{ route('user.declaration.index') }}" class="nav-link text-uppercase text-white fw-bold" href="#">Khai báo y tế</a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{ route('user.destination.index') }}" class="nav-link text-uppercase text-white fw-bold" >Quản lý điển đến</a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{ route("user.vaccination.index") }}" class="nav-link text-uppercase text-white fw-bold" >Đăng tiêm vacxin</a>
                                </li>
                                <li class="nav-item ">
                                    <a href="{{ route("user.quarantine-athome.index") }}" class="nav-link text-uppercase text-white fw-bold" >Theo sức khỏe tại nhà</a>
                                </li>
                                @if(!\Illuminate\Support\Facades\Auth::check())
                                <li class="nav-item ">
                                    <a href="{{route('user.auth.login')}}" class="nav-link text-uppercase text-white fw-bold">Đăng Nhập</a>
                                </li >
                                <li class="nav-item mx-2">
                                    <a href="{{route('user.auth.register')}}" class="nav-link text-uppercase text-white fw-bold">Đăng ký</a>
                                </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link dropdown-toggle text-uppercase text-white fw-bold" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{ "  ".Auth::user()->name." " }} <i class="fas fa-user ps-2"></i></a>
                                        <ul class="dropdown-menu sub_menu" aria-labelledby="navbarDropdown">
                                            <li><a class="dropdown-item" href="#">Trang Cá Nhân</a></li>
                                            <li><a class="dropdown-item" href="#">Đổi mật khẩu</a></li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
    </div>
    <div class="text-right">
        @if(\Illuminate\Support\Facades\Auth::check())
            <a type="button" href="{{ route('user.logout') }}" class="btn text-uppercase btn-logout">
                <i class="fas fa-sign-out-alt"></i> Đăng xuất
            </a>
        @endif
    </div>
</div>
