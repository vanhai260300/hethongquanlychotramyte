
    <div class="row my-4">
        <div class=" col-md-4">
            <label for="province" class=" form-label">Tỉnh thành<span
                    class="obligatory">(*)</span></label>
            <select name="province_id" id="province" class="chosen form-control  form-select">
                <option value="0">-- Chọn tỉnh/thành --</option>
                @foreach($provinces as $key => $province)
                    <option value="{{ $province->id }}" {{ $province->id == old("province_id") ? "selected" : "" }}> {{$province->_name }}</option>
                @endforeach
            </select>
        </div>
        <div class=" col-md-4">
            <label for="district" class="form-label">Quận/huyện <span
                    class="obligatory">(*)</span></label>
            <select name="district_id" id="district" class="chosen form-select ">
                <option value="0">-- Chọn Quận/huyện --</option>

            </select>
        </div>
        <div class=" col-md-4">
            <label for="ward_id" class="form-label">Phường/xã <span
                    class="obligatory">(*)</span></label>
            <select name="ward_id" id="ward" class="chosen form-control form-select">
                <option value="0">-- Chọn Phường/xã --</option>
            </select>
        </div>

    </div>

@push('custom-scripts')
    <script>
        $(document).ready(function(){
            $("#district").attr('disabled', true);
            $("#ward").attr('disabled', true);

            $("#province").change(function(){
                let id = $(this).val();
                $.ajax({
                    url: '{{ route('district.get') }}',
                    method:'POST',
                    data: {
                        id : id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (responsive){
                        $("#district").html(`<option value="0">-- Chọn Quận/Huyện --</option>`);
                        $("#ward").html(`<option value="0">-- Chon Phường/Xã --</option>`);
                        $("#district").attr('disabled', false);

                        $.each( responsive, function( key, value ) {
                            $("#district").append(`<option value="${value['id']}" > ${value['_prefix']} ${value['_name']} </option>`);
                        });

                    }
                });
            });
            $("#district").change(function(){
                // console.log($(this).val());
                let id = $(this).val();
                $.ajax({
                    url: '{{ route('ward.get') }}',
                    method:'POST',
                    data: {
                        id : id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (responsive){
                        console.log(responsive);
                        $("#ward").html(`<option value="0">-- Chon Phường/Xã --</option>`);
                        $("#ward").attr('disabled', false);
                        $.each( responsive, function( key, value ) {
                            $("#ward").append(`<option value="${value['id']}"> ${value['_prefix']} ${value['_name']} </option>`);
                        });

                    }
                });
            });

        });
        $('.chosen').select2({
            selectOnClose: true
        });
    </script>
@endpush
