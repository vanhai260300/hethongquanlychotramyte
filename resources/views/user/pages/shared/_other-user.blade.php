<div class="row">
    <div class="mb-3 col-6">
        <label for="name" class="form-label">Họ Tên<span
                class="obligatory">(*)</span></label>
        <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="Nhập họ tên">
    </div>
    <div class=" col-md-6">
        <label for="medical_station_id" class="form-label">Mối quan hệ<span
                class="obligatory"></span></label>
        <select name="relationship" id="relationship" class="form-control form-select">
            <option value="0" disabled selected>-- Chọn --</option>
            @foreach($relationship as $key => $value)
                <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="mb-3 col-6">
        <label for="phone_number" class="form-label">Số điện thoại<span
                class="obligatory">(*)</span></label>
        <input type="number" class="form-control" name="phone_number" value="{{ old('phone_number') }}" id="phone_number" placeholder="Nhâp số điện thoại">
    </div>
    <div class="mb-3 col-6">
        <label for="CMND" class="form-label">CMND/CCCD<span
                class="obligatory"></span></label>
        <input type="text" class="form-control" name="CMND" value="{{ old('CMND') }}" id="CMND" placeholder="Nhập CMND/CCCD">
    </div>
    <div class="mb-3 col-6">
        <label for="birthdate" class="form-label">Ngày tháng năm sinh</label>
        <input type="date" class="form-control" name="birthdate" value="{{ old('birthdate', date('Y-m-d')) }}" id="birthdate" aria-describedby="emailHelp">
    </div>
    <div class="mb-3 col-6 ">
        <label for="gender" class="form-label">Giới tính <span
                class="obligatory">(*)</span></label>
        <div class="d-flex">
            <div class="form-check">
                <input class="form-check-input" type="radio" @if(old('gender')) checked @endif checked name="gender" id="genderm"
                       value="1">
                <label class="form-check-label" for="genderm">
                    Nam
                </label>
            </div>
            <div class="form-check mx-4">
                <input class="form-check-input" type="radio" @if(old('gender')) checked @endif name="gender" id="genderf" value="2">
                <label class="form-check-label" for="genderf">
                    Nữ
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" @if(old('gender')) checked @endif name="gender" id="gendero" value="3">
                <label class="form-check-label" for="gendero">
                    Khác
                </label>
            </div>
        </div>
    </div>
    @include('user.pages.location')
    <div class=" col-md-4 my-3">
        <label for="medical_station_id" class="form-label">Trạm tế<span
                class="obligatory">(*)</span></label>
        <select name="medical_station_id" id="medical_station_id" class="chosen form-control form-select">
            <option value="0">-- Chọn trạm y tế --</option>
        </select>
    </div>
    <div class="mb-3 ">
        <label for="address" class="form-label">Số nhà, phố, tổ dân phố/thôn/đội <span
                class="obligatory">(*)</span></label>
        <input type="text" class="form-control" name="address" id="address" placeholder="Số nhà, phố, tổ dân phố/thôn/đội">
    </div>
</div>
