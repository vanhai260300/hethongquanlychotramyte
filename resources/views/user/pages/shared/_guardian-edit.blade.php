<div class="mb-3 col-4">
    <label for="guardian" class="form-label">Họ và tên người giám hộ <span
            class="obligatory">(*)</span></label>
    <input type="text" name="guardian" class="form-control" id="guardian" value="{{ old('guardian') ? old('guardian') : $var_guardian->guardian }}" placeholder="Họ và tên người giám hộ">
</div>
<div class="col-md-4">
    <label for="relationship_of_guardian" class="form-label">Mối quan hệ<span
            class="obligatory">(*)</span></label>
    <select name="relationship_of_guardian" id="relationship_of_guardian" class="form-control form-select">
        <option value="0">-- Chọn --</option>
        @foreach($relationship as $key => $value)
            <option value="{{ $value['id'] }}" {{ $value["id"] == (old('relationship_of_guardian') ? old('relationship_of_guardian') : $var_guardian->relationship_of_guardian) ? "selected" : "" }}>{{ $value['name'] }}</option>
        @endforeach
    </select>
</div>
<div class=" col-4">
    <label for="phone_number_of_guardian" class="form-label">Số điện thoại người giám hộ <span
            class="obligatory">(*)</span></label>
    <input type="number" name="phone_number_of_guardian" class="form-control" id="phone_number_of_guardian" value="{{ old('phone_number_of_guardian') ? old('phone_number_of_guardian') : $var_guardian->phone_number_of_guardian }}" placeholder="Nhập số điện thoại">
</div>
