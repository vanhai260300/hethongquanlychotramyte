@extends('user.layout.main',['title' => 'Chỉnh sửa bản đăng ký'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Thông tin đăng ký theo dõi sức khỏe tại nhà</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin đăng ký cho bản thân</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route("user.quarantine-athome.update", $object->id) }}" class="p-5" method="POST">
                        @csrf
                        {{ method_field("PUT") }}
                        @include('user.pages.quarantine_athome.edit-detail')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
        </script>
    @endpush
@endsection
