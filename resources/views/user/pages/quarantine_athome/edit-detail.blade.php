<div class="row py-3">
    <div class="mb-3 col-4">
        <label for="guardian" class="form-label">Họ và tên người giám hộ <span
                class="obligatory">(*)</span></label>
        <input type="text" name="guardian" class="form-control" id="guardian" value="{{ old('guardian') ? old('guardian') : $quarantine_athomes->guardian }}" placeholder="Họ và tên người giám hộ">
    </div>
    <div class="col-md-4">
        <label for="relationship_of_guardian" class="form-label">Mối quan hệ<span
                class="obligatory">(*)</span></label>
        <select name="relationship_of_guardian" id="relationship_of_guardian" class="form-control form-select">
            <option value="0">-- Chọn --</option>
            @foreach($relationship as $key => $value)
                <option value="{{ $value['id'] }}" {{ $value["id"] == (old('relationship_of_guardian') ? old('relationship_of_guardian') : $quarantine_athomes->relationship_of_guardian) ? "selected" : "" }}>{{ $value['name'] }}</option>
            @endforeach
        </select>
    </div>
    <div class=" col-4">
        <label for="phone_number_of_guardian" class="form-label">Số điện thoại người giám hộ <span
                class="obligatory">(*)</span></label>
        <input type="number" name="phone_number_of_guardian" class="form-control" id="phone_number_of_guardian" value="{{ old('phone_number_of_guardian') ? old('phone_number_of_guardian') : $quarantine_athomes->phone_of_guardian }}" placeholder="Nhập số điện thoại">
    </div>
    <div class=" col-md-4">
        <label for="injection" class="form-label">Số mũi vaccin đã tiêm<span
                class="obligatory">(*)</span></label>
        <input type="number" class="form-control" name="injection"  value="{{ old('injection') ? old('injection') : $quarantine_athomes->injection }}" min="0" max="3" placeholder="Nhập số mũi vaccin">
    </div>
    <div class=" col-md-4">
        <label for="object_quarantine_id" class="form-label">Đối tượng cách ly<span
                class="obligatory">(*)</span></label>
        <select name="object_quarantine_id" id="object_quarantine_id" class="form-control form-select">
            <option value="0" selected disabled>Chọn loại đối tượng</option>
            @foreach($object_quarantines as $key => $object_quarantine)
                <option value="{{ $object_quarantine->id }}" {{ $object_quarantine->id == (old('object_quarantine_id') ? old('object_quarantine_id') : $quarantine_athomes->object_quarantine_id) ? "selected" : "" }}>{{ $object_quarantine->name }}</option>
            @endforeach
        </select>
    </div>
    <div class=" col-4">
        <label for="quarantine_date" class="form-label">Ngày bắt đầu<span
                class="obligatory">(*)</span></label>
        <input type="date" name="quarantine_date" class="form-control" id="quarantine_date" value="{{ old('quarantine_date') ? old('quarantine_date') : $quarantine_athomes->quarantine_date }}" placeholder="dd/mm/YYY">
    </div>
</div>
<div class="text-center">
    <a href="{{ route("user.quarantine-athome.index") }}" class="btn btn-info text-white">Quay lại</a>
    <button type="submit" class="btn btn-primary btn-success">Xác nhận đăng ký</button>
</div>
