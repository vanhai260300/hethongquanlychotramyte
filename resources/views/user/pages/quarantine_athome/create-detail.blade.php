<div class="row py-3">
    @include("user.pages.shared._guardian")
    <div class=" col-md-4">
        <label for="injection" class="form-label">Số mũi vaccin đã tiêm<span
                class="obligatory">(*)</span></label>
        <input type="number" class="form-control" name="injection" id="injection" min="0" max="3" placeholder="Nhập số mũi vaccin">
    </div>
    <div class=" col-md-4">
        <label for="object_quarantine_id" class="form-label">Đối tượng TDSK<span
                class="obligatory">(*)</span></label>
        <select name="object_quarantine_id" id="object_quarantine_id" class="form-control form-select">
            <option value="0" disabled selected>Chọn loại đối tượng</option>
            @foreach($object_quarantines as $key => $object_quarantine)
                <option value="{{ $object_quarantine->id }}" {{ $object_quarantine->id == old("object_quarantine_id") ? "selected" : "" }}>{{ $object_quarantine->name }}</option>
            @endforeach
        </select>
    </div>
    <div class=" col-4">
        <label for="quarantine_date" class="form-label">Ngày bắt đầu<span
                class="obligatory">(*)</span></label>
        <input type="date" name="quarantine_date" class="form-control" id="quarantine_date" value="{{ old('quarantine_date') }}" placeholder="dd/mm/YYY">
    </div>
</div>
<div class="text-center">
    <a href="{{ route("user.quarantine-athome.index") }}" class="btn btn-info text-white">Quay lại</a>
    <button type="submit" class="btn btn-primary btn-success">Xác nhận đăng ký</button>
</div>
