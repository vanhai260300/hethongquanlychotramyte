@extends('user.layout.main',['title' => 'Theo dõi sức khỏe tại nhà'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item active" aria-current="page">Quản lý theo dõi tại nhà</li>
                </ol>
            </nav>
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Danh sách theo dõi tại nhà</h1>
                </div>
                <div class="declaration-form">
                    <div class="container">
                        <div class="row my-3">
                            <div class="right-option col-12">
                                <div class="row">
                                    <div class="col-2 d-block">
                                        <label for="relationship_of_guardian" class="form-label">.<span
                                                class="obligatory"></span></label>
                                        <div>
                                            <a href="{{ route('user.quarantine-athome.index') }}" style="font-weight: 600" class="btn btn-info text-white" tabindex="0" ><i class="fas fa-retweet"></i><span> Làm mới </span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <form action="{{ route('user.quarantine-athome.redirect') }}" id="formmain" method="POST">
                                            @csrf
                                            <div class="row col-12 d-flex">
                                                <div class="col-6">
                                                    <label for="relationship_of_guardian" class="form-label">Thêm mới đối tượng đã từng khai hộ<span
                                                            class="obligatory"></span></label>
                                                    <select name="object_of_user" id="relationship_of_guardian" class="form-control form-select">
                                                        <option disabled selected>-- Chọn --</option>
                                                        @foreach($object_of_users as $key => $object_of_user)
                                                            @if($object_of_user->id ==  Auth::id())
                                                                <option value="{{ $object_of_user->id }}" {{ $object_of_user->id == old("object_of_user") ? "selected" : "" }}>Đăng ký cho bản thân</option>
                                                            @else
                                                                <option value="{{ $object_of_user->id }}" {{ $object_of_user->id == old("object_of_user") ? "selected" : "" }}>{{ $object_of_user->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class=" col-1">
                                                    <label for="submit" class="form-label">.<span class="obligatory"></span></label>
                                                    <button type="submit" class="btn btn-primary btn-primary" id="formmain"><i class="fas fa-plus"></i></button>
                                                </div>
                                                <div class=" col-3">
                                                    <label for="submit" class="form-label">Hoặc<span class="obligatory"></span></label>
                                                    <div>
                                                        <a href="{{ route('user.quarantine-athome.create-other') }}" type="submit" class="btn btn-primary btn-primary">Thêm mới</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
{{--                        {{ dd($quarantineAthomes) }}--}}
                        <table class="table table-bordered table-hover table-striped inlineEditTable">
                            <thead class="noselect">
                            <tr style="height: 57.5px;">
                                <th scope="col" >STT</th>
                                <th scope="col" >Họ tên</th>
                                <th>Số điện thoại</th>
                                <th>CMND</th>
                                <th>Đối tượng</th>
                                <th>Ngày bắt đầu</th>
                                <th scope="col" >Số ngày/Tổng ngày</th>
                                <th>Trạng thái</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="border-end-0 text-center">
{{--                                    <a href="{{ route('user.quarantine-athome.create') }}" class="text-info"><i class="fas fa-plus-circle"></i></a>--}}
                                </td>

                                <td colspan="9" class="border-start-0 fw-bold">Thông tin của bản thân</td>
                            </tr>

                            @foreach ($quarantineAthomes as $key => $quarantineAthome)
                                @if ($quarantineAthome->object->id == Auth::id())
                                    <tr class="row_{{ $quarantineAthome->object->id }}">
                                        <td style="width: 50px; text-align: center;" data-column-index="1">1</td>
                                        <td data-column-index="2">{{ $quarantineAthome->object->name }}</td>
                                        <td data-column-index="3">{{ $quarantineAthome->object->phone_number }}</td>
                                        <td data-column-index="5">{{ $quarantineAthome->object->CMND }}</td>
                                        <td data-column-index="6">{{ $quarantineAthome->object_quarantine->name ?? "" }}</td>
                                        <td data-column-index="7">
                                            <span class="">{{ date("d/m/Y",strtotime($quarantineAthome->quarantine_date)) }}</span>
                                        </td>
                                        <td class="text-center">{{ $quarantineAthome->healt_monitorings_count }} / {{ $quarantineAthome->total_date }}</td>
                                        @if($quarantineAthome->status == -1)
                                            <td><p class="bg-danger rounded-pill pt-0 px-2 fs-6 text-white">Đã hủy</p></td>
                                        @endif
                                        @if($quarantineAthome->status == 0)
                                            <td><p class="bg-secondary rounded-pill pt-0 px-2 fs-6 text-white">Chờ xác nhận</p></td>
                                        @elseif($quarantineAthome->status == 1)
                                            <td><p class="bg-info rounded-pill pt-0 px-2 fs-6 text-white">Đang theo dõi</p></td>
                                        @elseif($quarantineAthome->status == 2)
                                            <td><p class="bg-success rounded-pill pt-0 px-2 fs-6 text-white">Hoàn tất</p></td>
                                        @endif
                                        <td class="text-center" data-column-index="9">
                                            <a href="{{ route('user.quarantine-athome.show', $quarantineAthome->object->id) }}" class="text-decoration-none text-success" alt="Xem chi tiết">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{ route('user.quarantine-athome.edit', $quarantineAthome->object->id) }}" class="text-decoration-none fs-5" alt="Cập nhật tờ khai">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a onclick="NotiDeleteDeclara({{$quarantineAthome->object->id}})" class="text-danger fs-5" alt="Xóa">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            <tr>
                                <td class="border-end-0 text-center">
{{--                                    <a href="{{ route('user.quarantine-athome.create-other') }}" class="text-info">--}}
{{--                                        <i class="fas fa-plus-circle"></i>--}}
{{--                                    </a>--}}
                                </td>
                                <td colspan="9" class="border-start-0 fw-bold">Thông tin của người thân</td>
                            </tr>
                            @foreach ($quarantineAthomes as $key => $quarantineAthome)
                                @if ($quarantineAthome->object->id != Auth::id())
                                    <tr class="row_{{ $quarantineAthome->object->id }}">
                                        <td style="width: 50px; text-align: center;" data-column-index="1">{{ $key + 1 }}</td>
                                        <td data-column-index="2">{{ $quarantineAthome->object->name }}</td>
                                        <td data-column-index="3">{{ $quarantineAthome->object->phone_number }}</td>
                                        <td data-column-index="5">{{ $quarantineAthome->object->CMND }}</td>
                                        <td data-column-index="6">{{ $quarantineAthome->object_quarantine->name ?? "" }}</td>
                                        <td data-column-index="7">
                                            <span class="">{{ date("d/m/Y",strtotime($quarantineAthome->quarantine_date)) }}</span>
                                        </td>
                                        <td class="text-center">{{ $quarantineAthome->healt_monitorings_count }} / {{ $quarantineAthome->total_date }}</td>
                                        @if($quarantineAthome->status == -1)
                                            <td><p class="bg-danger rounded-pill pt-0 px-2 fs-6 text-white">Đã hủy</p></td>
                                        @endif
                                        @if($quarantineAthome->status == 0)
                                            <td><p class="bg-secondary rounded-pill pt-0 px-2 fs-6 text-white">Chờ xác nhận</p></td>
                                        @elseif($quarantineAthome->status == 1)
                                            <td><p class="bg-info rounded-pill pt-0 px-2 fs-6 text-white">Đang theo dõi</p></td>
                                        @elseif($quarantineAthome->status == 2)
                                            <td><p class="bg-info rounded-pill pt-0 px-2 fs-6 text-success">Hoàn tất</p></td>
                                        @endif
                                        <td class="text-center" data-column-index="9">
                                            <a href="{{ route('user.quarantine-athome.show', $quarantineAthome->object->id) }}" class="text-decoration-none text-success" alt="Xem chi tiết">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{ route('user.quarantine-athome.edit-other', $quarantineAthome->object->id) }}" class="text-decoration-none fs-5" alt="Cập nhật tờ khai">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a onclick="NotiDeleteDeclara({{$quarantineAthome->object->id}})" class="text-danger fs-5" alt="Xóa">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="number-pages card-footer clearfix" >
                {{--                {{ $schedules->links() }}--}}
            </div>
        </div>
    </div>
    @push("custom-scripts")
        <script>
            function NotiDeleteDeclara(id){
                Swal.fire({
                    icon: 'warning',
                    title: 'Bạn chắc chắn muốn xóa?',
                    text: 'Xóa bản khai báo này!',
                    showCancelButton: true,
                    confirmButtonText: '<a style = "Color:White;" onclick="deleteDeclara(' + id + ')">Xóa</a>',
                    cancelButtonText: 'Quay lại'
                });
            }
            function deleteDeclara(object_id){
                $(".row_"+object_id).remove();
                $.ajax({
                    url: '{{ route("user.quarantine-athome.destroy", 1) }}',
                    method: 'DELETE',
                    data: {
                        id: object_id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        console.log(result);
                        $("#row_" + object_id).remove();
                        NotificationSuccess();
                    }
                });
            }
            function NotificationSuccess(){
                Swal.fire({
                    icon: 'success',
                    title: 'Xóa thành công?',
                    confirmButtonText: 'Okey',
                });
            }
        </script>
    @endpush
@endsection
