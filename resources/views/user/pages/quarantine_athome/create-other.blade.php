@extends('user.layout.main',['title' => 'Đăng ký cách ly tại nhà'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Thông tin đăng ký theo dõi sức khỏe tại nhà</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin đăng ký cho người thân</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route("user.quarantine-athome.store-other") }}" class="p-5" method="POST">
                        @csrf
                        @include("user.pages.shared._other-user")
                        @include('user.pages.quarantine_athome.create-detail')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
        </script>
        <script>
            $(document).ready(function(){
                $("#medical_station_id").attr('disabled', true);
                $("#ward").change(function(){
                    console.log($(this).val());
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('medical-station.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            console.log(responsive);
                            let medical_station_id = $("#medical_station_id");
                            medical_station_id.html(`<option value="0">-- Chọn trạm y tế --</option>`);
                            medical_station_id.attr('disabled', false);
                            $.each( responsive, function( key, value ) {
                                $("#medical_station_id").append(`<option value="${value['id']}">  ${value['name']} </option>`);
                            });

                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
