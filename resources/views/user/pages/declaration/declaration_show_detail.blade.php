@extends('user.layout.main',['title' => 'Khai báo y tế chi tiết'])
@section('content')
    <div class="content-container ps-5">
{{--        {{ dd($declaration) }}--}}
        <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Trang chủ</li>
                <li class="breadcrumb-item">Khai báo y tế</li>
                <li class="breadcrumb-item active" aria-current="page">Thông tin chi tiết</li>
            </ol>
        </nav>
        @include("user.shared._button-back")
        <div class="container_form-detail">
            <div class="row">
                <div class="col-9">
                    <div class="national-brand text-center mb-10">
                        <div class="text-uppercase "><b>Thông tin khai báo y tế</b>
                        </div>
                    </div>
                    <div class="text-center text-uppercase mb-4">
                        <div style="color: red;text-transform: none;">Khuyến cáo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam và có thể xử lý hình sự</div>
                    </div>
                    <div class="form-group inline-block form-inline label-width mb-10">
                        <label class="text-bold">Họ tên (ghi chữ IN HOA): </label>
                        <span class="inline-block form-inline text-uppercase">{{ $object->name }}</span>
                    </div>
                    <div class="form-group inline-block form-inline mb-10">
                        <label class="text-bold">Số hộ chiếu / CMND / CCCD: </label>
                        <span class="inline-block form-inline">{{ $object->CMND }}</span>
                    </div>
                    <div class="row ">
                        <div class="col-md-5 col-sm-5  col-xs-12 mb-10">
                            <div class="form-group form-inline gender-box ">
                                <label class="text-bold">Năm sinh: </label>
                                <span class="inline-block form-inline">{{ date('d-m-Y', strtotime($object->birthdate)) }}</span> </div></div><div class="col-md-3 col-sm-3  col-xs-12 ">
                            <div class="form-group form-inline ">
                                <label class="text-bold">Giới tính: </label>
                                <span class="inline-block form-inline">{{ $object->gender == 1 ? "Nam" : "Nữ" }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group form-inline ">
                                <label class="text-bold"></label>
                                <span class="inline-block form-inline"></span> </div></div>
                    </div>
                    <div class="mb-10 fw-bold"><b>Địa chỉ liên lạc tại</b>
                    </div>
                    <div class="row ">
                        <div class="col-md-4 col-sm-4  col-xs-12 mb-10">
                            <div class="form-group  ">
                                <label class="text-bold">Tỉnh / thành: </label>
                                <span class="inline-block form-inline">{{ $object->province->_name }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Quận / huyện: </label>
                                <span class="inline-block form-inline">{{ $object->district->_name }}</span> </div></div><div class="col-md-4 col-sm-4  col-xs-12 ">
                            <div class="form-group  ">
                                <label class="text-bold">Phường / xã: </label>
                                <span class="inline-block form-inline">{{ $object->ward->_name }}</span> </div></div>
                    </div>
                    <div class="form-group  mb-10 ">
                        <label class="text-bold">Số nhà, phố, tổ dân phố/thôn/đội</label>
                        <span class="inline-block form-inline">{{ $object->address }}</span>
                    </div>
                    <div class="row ">
                        <div class="col-md-6 col-sm-6  col-xs-12 mb-10">
                            <div class="form-group inline-block form-inline label-width ">
                                <label class="text-bold">Điện thoại: </label>
                                <span class="inline-block form-inline">{{ $object->phone_number }}</span> </div></div><div class="col-md-6 col-sm-6  col-xs-12 ">
                            <div class="form-group inline-block form-inline label-width ">
                                <label class="text-bold">Email: </label>
                                <span class="inline-block form-inline">{{ $object->email }}</span> </div></div>
                    </div>
                    <div class="form-group  mb-10 ">
                        <label class="text-bold">Khai báo lúc: </label>
                        <span class="inline-block form-inline">{{ date('d-m-Y H:i:s', strtotime( $declaration->updated_at)) }}</span>
                    </div>
                    <div>
                        <table class="table table-bordered tableData2">
                            <thead>
                            <tr>
                                <th scope="col" style="width:80%;">Dấu hiệu</th>
                                <th scope="col" class="text-center">Có</th>
                                <th scope="col" class="text-center">Không</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td >Sốt </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->fever == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->fever == 0 ? "checked" : "") }} class="form-check-input"  >
                                </td>
                            </tr>
                            <tr>
                                <td >Ho </td>
                                <td class="text-center"> <input disabled="" {{ ($declaration->cough == 1 ? "checked" : "") }} type="Radio" class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->cough == 0 ? "checked" : "") }} class="form-check-input"  >
                                </td>
                            </tr>
                            <tr>
                                <td >Đau họng</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->sore_throat == 1 ? "checked" : "") }} class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" {{ ($declaration->sore_throat == 0 ? "checked" : "") }} type="radio" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td >Khó thở </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->difficulty_of_breathing == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->difficulty_of_breathing == 0 ? "checked" : "") }} class="form-check-input" >
                                </td>
                            </tr>
                            <tr>
                                <td >Tiêu chảy </td>
                                <td class="text-center"> <input disabled="" {{ ($declaration->diarrhea == 1 ? "checked" : "") }} type="Radio" class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->diarrhea == 0 ? "checked" : "") }} class="form-check-input" >
                                </td>
                            </tr>
                            <tr>
                                <td >Cảm thấy mệt mỏi</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->tired == 1 ? "checked" : "") }} class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" disabled {{ ($declaration->tired == 0 ? "checked" : "") }} type="radio" >
                                </td>
                            </tr>
                            <tr>
                                <td >Mất mùi mât vị</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->loss_taste == 1 ? "checked" : "") }} class="form-check-input" >
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" {{ ($declaration->loss_taste == 0 ? "checked" : "") }} type="radio"   disabled>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered tableData2">
                            <thead>
                            <tr>
                                <th scope="col" style="width:80%;">Trong vòng 14 ngày qua</th>
                                <th scope="col" class="text-center">Có</th>
                                <th scope="col" class="text-center">Không</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td >1. Bạn có đang mắc COVID-19 không? </td>
                                <td class="text-center">
                                        <input disabled type="Radio" {{ ($declaration->got_covid == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled type="Radio" {{ ($declaration->got_covid == 0 ? "checked" : "") }} class="form-check-input"  >
                                </td>
                            </tr>
                            <tr>
                                <td >2. Bạn đã từng mắc COVID-19 không? </td>
                                <td class="text-center"> <input disabled type="Radio" {{ ($declaration->exposure_covid == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled type="Radio" class="form-check-input" {{ ($declaration->exposure_covid == 0 ? "checked" : "") }}>
                                </td>
                            </tr>
                            <tr>
                                <td >3. Tiếp xúc gần ca nhiễm, ca nghi nhiễm COVID-19 trong vòng 14 ngày qua</td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" class="form-check-input" {{ ($declaration->ever_had_covid == 1 ? "checked" : "") }}>
                                </td>
                                <td class="text-center">
                                    <input class="form-check-input" type="radio" id="flexRadioCheckedDisabled" {{ ($declaration->ever_had_covid == 0 ? "checked" : "") }} disabled>
                                </td>
                            </tr>
                            <tr>
                                <td >4. Bạn có kết thúc cách ly tập trung trong vòng 14 ngày qua không? </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->quarantined == 1 ? "checked" : "") }} class="form-check-input">
                                </td>
                                <td class="text-center">
                                    <input disabled="" type="Radio" {{ ($declaration->quarantined == 0 ? "checked" : "") }} class="form-check-input">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-3">
                    <div class="text-end">
                        <button type="button" class="btn btn-success"><i class="fas fa-print"></i></button>
                    </div>
                    <div class="imgQR-box text-center">
                        <div class="card-body">
                            {!! QrCode::size(230)->generate(route('medical_station.declaration.detail',$declaration->object_id)) !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <a href="{{ route("user.declaration.index") }}" class="btn btn-info text-white">Quay lại</a>
                <a href="{{ route('user.declaration.otherpeople.edit', $declaration->object_id) }}" type="submit" class="btn btn-primary btn-submit btn-success">Cập nhật tờ khai</a>
            </div>
        </div>
    </div>
@endsection
