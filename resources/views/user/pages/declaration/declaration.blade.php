@extends('user.layout.main',['title' => 'Khai báo y tế'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item active" aria-current="page">Danh sách khai báo y tế</li>
                </ol>
            </nav>
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Lịch sử khai báo y tế của bạn</h1>
                </div>
                <div class="declaration-form">
                    <div class="container">
                        <div class="row my-3">
                            <div class="right-option col-12">
                                <div class="row">
                                    <div class="col-2 d-block">
                                        <label for="relationship_of_guardian" class="form-label">.<span
                                                class="obligatory"></span></label>
                                        <div>
                                            <a href="{{ route('user.declaration.index') }}" style="font-weight: 600" class="btn btn-info text-white" tabindex="0" ><i class="fas fa-retweet"></i><span> Làm mới </span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <form action="{{ route('user.declaration.redirect') }}" id="formmain" method="POST">
                                            @csrf
                                            <div class="row col-12 d-flex">
                                                <div class="col-6">
                                                    <label for="relationship_of_guardian" class="form-label">Thêm mới đối tượng đã từng khai hộ<span
                                                            class="obligatory"></span></label>
                                                    <select name="object_of_user" id="relationship_of_guardian" class="form-control form-select">
                                                        <option disabled selected>-- Chọn --</option>
                                                        @foreach($object_of_users as $key => $object_of_user)
                                                            @if($object_of_user->id ==  Auth::id())
                                                                <option value="{{ $object_of_user->id }}" {{ $object_of_user->id == old("object_of_user") ? "selected" : "" }}>Khai báo cho bản thân</option>
                                                            @else
                                                                <option value="{{ $object_of_user->id }}" {{ $object_of_user->id == old("object_of_user") ? "selected" : "" }}>{{ $object_of_user->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class=" col-1">
                                                    <label for="submit" class="form-label">.<span class="obligatory"></span></label>
                                                    <button type="submit" class="btn btn-primary btn-primary" id="formmain"><i class="fas fa-plus"></i></button>
                                                </div>
                                                <div class=" col-3">
                                                    <label for="submit" class="form-label">Hoặc<span class="obligatory"></span></label>
                                                    <div>
                                                        <a href="{{ route('user.declaration.otherpeople') }}" type="submit" class="btn btn-primary btn-primary">Thêm mới</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <table class="table table-bordered table-hover table-striped inlineEditTable">
                            <thead class="noselect">
                                <tr style="height: 57.5px;">
                                    <th scope="col" style="width: 39px;" data-column-index="1">ID</th>
                                    <th scope="col" style="width: 142px;" data-column-index="2">Họ Tên</th>
                                    <th scope="col" style="width: 155px;" data-column-index="3">Số điện thoại</th>
{{--                                    <th scope="col" style="width: 48px;" data-column-index="4">Email</th>--}}
                                    <th scope="col" style="width: 139px;" data-column-index="5">Số CMND</th>
                                    <th scope="col" style="width: 151px;" data-column-index="6">Năm sinh</th>
                                    <th scope="col" style="width: 148px;" data-column-index="7">Giới tính</th>
                                    <th scope="col" style="width: 148px;" data-column-index="8">Ngày cập nhật</th>
{{--                                    <th scope="col" style="width: 141px;" data-column-index="8">Tình trạng</th>--}}
                                    <th scope="col" class="text-center" style="width: 52px;" data-column-index="9">Hành động</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="border-end-0 text-center">
                                        @php
                                            $countobject = 0;
                                        @endphp
                                        @foreach ($healthDeclarations as $key => $healthDeclaration)
                                            @if ($healthDeclaration->object->id == Auth::id())
                                                @php
                                                  $countobject = 1;
                                                @endphp
                                            @endif
                                        @endforeach
                                        @if($countobject == 0)
                                            <a href="{{ route('user.declaration.myself') }}" class="text-info"><i class="fas fa-plus-circle"></i></a>
                                        @endif
                                    </td>
                                    <td colspan="8" class="border-start-0 fw-bold">Khai báo cho bản thân</td>
                                </tr>
                                @foreach ($healthDeclarations as $key => $healthDeclaration)
                                    @if ($healthDeclaration->object->id == Auth::id())
                                    <tr>
                                        <td style="width: 50px; text-align: center;" data-column-index="1">1</td>
                                        <td data-column-index="2">{{ $healthDeclaration->object->name }}</td>
                                        <td data-column-index="3">{{ $healthDeclaration->object->phone_number }}</td>
{{--                                        <td data-column-index="4">{{ $healthDeclaration->object->email }}</td>--}}
                                        <td data-column-index="5">{{ $healthDeclaration->object->CMND }}</td>
                                        <td data-column-index="6">{{ $healthDeclaration->object->birthdate }}</td>
                                        <td data-column-index="7">
                                            <span class="">{{ $healthDeclaration->object->gender == 1 ? 'Nam' : 'Nữ' }}</span>
                                        </td>
                                        <td data-column-index="6">{{ $healthDeclaration->updated_at }}</td>
                                        {{--                                    <td data-column-index="8">Bình thường</td>--}}
                                        <td class="text-center" data-column-index="9">
                                            <a href="{{ route('user.declaration.show-detail', $healthDeclaration->object->id) }}" class="text-decoration-none" alt="Xem chi tiết">
                                                <img src="{{ asset('libs/user/imgs/icon-view.png') }}" alt="">
                                            </a>
                                            <a href="{{ route('user.declaration.myself.edit', $healthDeclaration->object->id) }}" class="text-decoration-none" alt="Cập nhật tờ khai">
                                                <img src="{{ asset('libs/user/imgs/bg-qr-yt.png') }}" alt="">
                                            </a>
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <td class="border-end-0 text-center"><a href="{{ route('user.declaration.otherpeople') }}" class="text-info"><i class="fas fa-plus-circle"></i></a></td>
                                    <td colspan="8" class="border-start-0 fw-bold">Khai báo cho người thân</td>
                                </tr>
                                @foreach ($healthDeclarations as $key => $healthDeclaration)
                                    @if ($healthDeclaration->object->id != \Illuminate\Support\Facades\Auth::id())
                                        <tr class="row_{{ $healthDeclaration->object->id }}">
                                            <td style="width: 50px; text-align: center;" data-column-index="1">{{ $key+1 }}</td>
                                            <td data-column-index="2">{{ $healthDeclaration->object->name }}</td>
                                            <td data-column-index="3">{{ $healthDeclaration->object->phone_number }}</td>
{{--                                            <td data-column-index="4">{{ $healthDeclaration->object->email }}</td>--}}
                                            <td data-column-index="5">{{ $healthDeclaration->object->CMND }}</td>
                                            <td data-column-index="6">{{ $healthDeclaration->object->birthdate }}</td>
                                            <td data-column-index="7">
                                                <span class="">{{ $healthDeclaration->object->gender == 1 ? 'Nam' : 'Nữ' }}</span>
                                            </td>
                                            <td data-column-index="6">{{ $healthDeclaration->updated_at }}</td>
                                            {{--                                    <td data-column-index="8">Bình thường</td>--}}
                                            <td class="text-center" data-column-index="9">
                                                <a href="{{ route('user.declaration.show-detail', $healthDeclaration->object->id) }}" class="text-decoration-none" alt="Xem chi tiết">
                                                    <img src="{{ asset('libs/user/imgs/icon-view.png') }}" alt="">
                                                </a>
                                                <a href="{{ route('user.declaration.otherpeople.edit', $healthDeclaration->object->id) }}" class="text-decoration-none" alt="Cập nhật tờ khai">
                                                    <img src="{{ asset('libs/user/imgs/bg-qr-yt.png') }}" alt="">
                                                </a>
                                                <a onclick="NotiDeleteDeclara({{$healthDeclaration->object->id}})" class="text-danger fs-5" alt="Xóa">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @push("custom-scripts")
        <script>
            function NotiDeleteDeclara(id){
                Swal.fire({
                    icon: 'warning',
                    title: 'Bạn chắc chắn muốn xóa?',
                    text: 'Xóa bản khai báo này!',
                    showCancelButton: true,
                    confirmButtonText: '<a style = "Color:White;" onclick="deleteDeclara(' + id + ')">Xóa</a>',
                    cancelButtonText: 'Quay lại'
                });
            }
            function deleteDeclara(object_id){
                $(".row_"+object_id).remove();
                $.ajax({
                    url: '{{ route("user.declaration.destroy") }}',
                    method: 'DELETE',
                    data: {
                        id: object_id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        console.log(result);
                        $("#row_" + object_id).remove();
                        NotificationSuccess();
                    }
                });
            }
            function NotificationSuccess(){
                Swal.fire({
                    icon: 'success',
                    title: 'Xóa thành công?',
                    confirmButtonText: 'Okey',
                });
            }
        </script>
    @endpush
@endsection
