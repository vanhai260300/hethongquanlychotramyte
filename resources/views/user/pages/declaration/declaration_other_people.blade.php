@extends('user.layout.main',['title' => 'Khai báo y tế chi tiết'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <div id="module9" class="ModuleWrapper">
                <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Trang chủ</li>
                        <li class="breadcrumb-item">Khai báo y tế</li>
                        <li class="breadcrumb-item active" aria-current="page">Thông tin chi tiết</li>
                    </ol>
                </nav>
                @include("user.shared._button-back")
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Khai báo y tế</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin khai báo cho người khác</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('user.declaration.otherpeople') }}" class="p-5" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-6">
                                <label for="name" class="form-label">Họ Tên<span
                                        class="obligatory">(*)</span></label>
                                <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="Nhập họ tên">
                            </div>
                            <div class=" col-md-6">
                                <label for="medical_station_id" class="form-label">Mối quan hệ<span
                                        class="obligatory">(*)</span></label>
                                <select name="relationship" id="relationship" class="form-control form-select">
                                    <option value="0">-- Chọn --</option>
                                    @foreach($relationship as $key => $value)
                                        <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
{{--                            <div class="mb-3 col-6">--}}
{{--                                <label for="email" class="form-label">Email</label>--}}
{{--                                <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" aria-describedby="emailHelp">--}}
{{--                            </div>--}}
                            <div class="mb-3 col-6">
                                <label for="phone_number" class="form-label">Số điện thoại<span
                                        class="obligatory">(*)</span></label>
                                <input type="number" class="form-control" name="phone_number" value="{{ old('phone_number') }}" id="phone_number" placeholder="Nhâp số điện thoại">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="CMND" class="form-label">CMND/CCCD<span
                                        class="obligatory">(*)</span></label>
                                <input type="text" class="form-control" name="CMND" value="{{ old('CMND') }}" id="CMND" placeholder="Nhập CMND/CCCD">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="birthdate" class="form-label">Ngày tháng năm sinh</label>
                                <input type="date" class="form-control" name="birthdate" value="{{ old('birthdate', date('Y-m-d')) }}" id="birthdate" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3 col-6 ">
                                <label for="gender" class="form-label">Giới tính <span
                                        class="obligatory">(*)</span></label>
                                <div class="d-flex">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" @if(old('gender')) checked @endif checked name="gender" id="genderm"
                                               value="1">
                                        <label class="form-check-label" for="genderm">
                                            Nam
                                        </label>
                                    </div>
                                    <div class="form-check mx-4">
                                        <input class="form-check-input" type="radio" @if(old('gender')) checked @endif name="gender" id="genderf" value="2">
                                        <label class="form-check-label" for="genderf">
                                            Nữ
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" @if(old('gender')) checked @endif name="gender" id="gendero" value="3">
                                        <label class="form-check-label" for="gendero">
                                            Khác
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @include('user.pages.location')

                        <div class=" col-md-4 my-3">
                            <label for="medical_station_id" class="form-label">Trạm tế<span
                                    class="obligatory">(*)</span></label>
                            <select name="medical_station_id" id="medical_station_id" class="chosen form-control form-select">
                                <option value="0">-- Chọn trạm y tế --</option>
                            </select>
                        </div>
                        <div class="mb-3 ">
                            <label for="address" class="form-label">Số nhà, phố, tổ dân phố/thôn/đội <span
                                    class="obligatory">(*)</span></label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="Số nhà, phố, tổ dân phố/thôn/đội">
                        </div>
                        @include('user.pages.declaration.declaration_detail')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            $(".btn-refresh").click(function(){
                $.ajax({
                    type:'GET',
                    url:'{{ route('user.refresh_captcha') }}',
                    success:function(data){
                        $(".captcha span").html(data.captcha);
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#medical_station_id").attr('disabled', true);
                $("#ward").change(function(){
                    console.log($(this).val());
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('medical-station.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            console.log(responsive);
                            let medical_station_id = $("#medical_station_id");
                            medical_station_id.html(`<option value="0">-- Chọn trạm y tế --</option>`);
                            medical_station_id.attr('disabled', false);
                            $.each( responsive, function( key, value ) {
                                $("#medical_station_id").append(`<option value="${value['id']}">  ${value['name']} </option>`);
                            });

                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
