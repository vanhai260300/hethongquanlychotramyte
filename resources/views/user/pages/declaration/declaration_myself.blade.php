@extends('user.layout.main',['title' => 'Khai báo y tế chi tiết'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item">Khai báo y tế</li>
                    <li class="breadcrumb-item active" aria-current="page">Khai báo y tế cho bản thân</li>
                </ol>
            </nav>
            @include("user.shared._button-back")
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Khai báo y tế</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin khai báo cho bản thân</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--                    @error('password')--}}
                    {{--                    <small class="text-danger">{{ $message }}</small>--}}
                    {{--                    @enderror--}}
                    <form action="{{ route('user.declaration.myself') }}" class="p-5" method="POST">
                        @csrf
                        @include('user.pages.declaration.declaration_detail')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            $(".btn-refresh").click(function(){
                $.ajax({
                    type:'GET',
                    url:'{{ route('user.refresh_captcha') }}',
                    success:function(data){
                        $(".captcha span").html(data.captcha);
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#medical_station_id").attr('disabled', true);
                $("#ward").change(function(){
                    console.log($(this).val());
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('medical-station.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            console.log(responsive);
                            let medical_station_id = $("#medical_station_id");
                            medical_station_id.html(`<option value="0">-- Chọn trạm y tế --</option>`);
                            medical_station_id.attr('disabled', false);
                            $.each( responsive, function( key, value ) {
                                $("#medical_station_id").append(`<option value="${value['id']}">  ${value['name']} </option>`);
                            });

                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
