@extends('user.layout.main',['title' => 'Cập nhật khai báo y tế'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item">Khai báo y tế</li>
                    <li class="breadcrumb-item active" aria-current="page">Khai báo y tế cho người thân</li>
                </ol>
            </nav>
            @include("user.shared._button-back")
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Cập nhật khai báo y tế</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin khai báo cho bản thân</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('user.declaration.myself.edit', $declaration->object_id) }}" class="p-5" method="POST">
                        @csrf
                        @include('user.pages.declaration.declaration_detail_edit')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            $(".btn-refresh").click(function(){
                $.ajax({
                    type:'GET',
                    url:'{{ route('user.refresh_captcha') }}',
                    success:function(data){
                        $(".captcha span").html(data.captcha);
                    }
                });
            });
        </script>
    @endpush
@endsection
