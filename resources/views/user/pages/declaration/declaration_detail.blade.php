
<table class="table table-bordered">
    <thead class="table-light">
        <tr>
            <th>Dấu hiệu</th>
            <th class="text-bold">Có</th>
            <th class="text-bold">Không</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="col-10">
                Sốt <span class="obligatory ">(*)</span>
            </td>
            <td class="col-1">
                <input class="form-check-input" type="radio" name="fever" id="fever" value="1">
            </td>
            <td class="col-1">
                <input class="form-check-input" type="radio" name="fever" id="fever" checked value="0">
            </td>
        </tr>
        <tr>
            <td>
                Ho <span class="obligatory">(*)</span>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="cough" id="cough" value="1">
            </td>
            <td>
                <input class="form-check-input" type="radio" name="cough" id="cough" checked value="0">
            </td>
        </tr>
        <tr>
            <td>
                Đau họng <span class="obligatory">(*)</span>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="sore_throat" id="sore_throat" value="1">
            </td>
            <td>
                <input class="form-check-input" type="radio" name="sore_throat" id="sore_throat" checked value="0">
            </td>
        </tr>
        <tr>
            <td>
                Khó thở <span class="obligatory">(*)</span>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="difficulty_of_breathing" id="difficulty_of_breathing" value="1">
            </td>
            <td>
                <input class="form-check-input" type="radio" name="difficulty_of_breathing" id="difficulty_of_breathing" checked value="0">
            </td>
        </tr>
        <tr>
            <td>
                Tiêu chảy <span class="obligatory">(*)</span>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="diarrhea" id="diarrhea" value="1">
            </td>
            <td>
                <input class="form-check-input" type="radio" name="diarrhea" id="diarrhea" checked value="0">
            </td>
        </tr>
        <tr>
            <td>
                Cảm thấy mệt mỏi <span class="obligatory">(*)</span>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="tired" id="tired" value="1">
            </td>
            <td>
                <input class="form-check-input" type="radio" name="tired" id="tired" checked value="0">
            </td>
        </tr>
        <tr>
            <td>
                Mất mùi/Mất vị <span class="obligatory">(*)</span>
            </td>
            <td>
                <input class="form-check-input" type="radio" name="loss_taste" id="loss_taste" value="1">
            </td>
            <td>
                <input class="form-check-input" type="radio" name="loss_taste" id="loss_taste" checked value="0">
            </td>
        </tr>

    </tbody>
</table>
<table class="table table-bordered">
    <thead class="table-light">
    <tr>
        <th>Trong thời gian vừa qua</th>
        <th class="text-bold">Có</th>
        <th class="text-bold">Không</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="col-10">
            1. Bạn có đang mắc COVID-19 không? <span class="obligatory ">(*)</span>
        </td>
        <td class="col-1">
            <input class="form-check-input" type="radio" name="got_covid" id="got_covid" value="1">
        </td>
        <td class="col-1">
            <input class="form-check-input" type="radio" name="got_covid" id="got_covid" checked value="0">
        </td>
    </tr>
    <tr>
        <td>
            2. Bạn đã từng mắc COVID-19 không? <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="ever_had_covid" id="ever_had_covid" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="ever_had_covid" id="ever_had_covid" checked value="0">
        </td>
    </tr>
    <tr>
        <td>
            3. Tiếp xúc gần ca nhiễm, ca nghi nhiễm COVID-19 trong vòng 14 ngày qua
            <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="exposure_covid" id="exposure_covid" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="exposure_covid" id="exposure_covid" checked value="0">
        </td>
    </tr>
    <tr>
        <td>
            4. Bạn có kết thúc cách ly tập trung trong vòng 14 ngày qua không? (*) <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="quarantined" id="quarantined" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="quarantined" id="quarantined" checked value="0">
        </td>
    </tr>

    </tbody>
</table>
<div class="mb-3 ">
    <label for="address" class="form-label">Ghi chú</label>
    <textarea class="form-control" name="note" id="note" cols="" placeholder="Nhập ghi chú" rows="5"></textarea>
{{--                            <input type="text" class="form-control" name="address" id="address" aria-describedby="emailHelp">--}}
</div>
<div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
    <label for="password" class="col-md-4 control-label py-2">Mã bảo mật<span
            class="obligatory">(*)</span></label>
    <div class="col-md-6 ">
        <div class="row">
            <div class="captcha col-sm-4">
                <span>{!! captcha_img() !!}</span>
                <button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="col-sm-8">
                <input id="captcha" type="text" class="form-control" placeholder="Nhập mã bảo mật" name="captcha">

            </div>

        </div>
        @if ($errors->has('captcha'))
            <span class="help-block">
          <strong>{{ $errors->first('captcha') }}</strong>
      </span>
        @endif
    </div>
</div>
<p class="text-danger py-4">Dữ liệu bạn cung cấp hoàn toàn bảo mật và chỉ phục vụ cho việc phòng chống dịch,
    thuộc quản lý của Ban chỉ đạo quốc gia về Phòng chống dịch Covid-19. Khi bạn nhấn nút "Gửi" là bạn đã
    hiểu và đồng ý.</p>
<div class="text-center">
    <a href="{{ route("user.declaration.index") }}" class="btn btn-info text-white">Quay lại</a>
    <button type="submit" class="btn btn-primary btn-submit btn-success">Gửi tờ khai</button>
</div>

