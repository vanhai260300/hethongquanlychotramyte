@extends('user.layout.main',['title' => 'Chỉnh sửa nhật ký điểm đến'])
@section('content')
    <div class="content py-5">
{{--        {{ dd($schedule) }}--}}
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item">Quản lý điểm đến</li>
                    <li class="breadcrumb-item active" aria-current="page">Chỉnh sửa thông tin</li>
                </ol>
            </nav>
            @include("user.shared._button-back")
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Chỉnh sửa nhật ký điểm đến</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin địa điểm</b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('user.destination.update', $schedule->id) }}" class="p-5" method="POST">
                        @csrf
                        {{ method_field("PUT") }}
                        <div class="row">
                            <div class="mb-3 col-6">
                                <label for="name" class="form-label">Tên địa điểm<span
                                        class="obligatory">(*)</span></label>
                                <input type="text" name="name" class="form-control" id="name" value="{{ old('name') ? old('name') : $schedule->name }}" placeholder="Nhập tên địa điểm">
                            </div>
                            <div class="mb-3 col-6"></div>
                            <div class="mb-3 col-6">
                                <label for="address" class="form-label">Thời gian đến<span
                                        class="obligatory">(*)</span></label>
                                <input type="datetime-local" class="form-control" name="from_date" value="{{ date('m/d/Y H:i', strtotime($schedule->from_date)) }}" id="from_date" placeholder="dd/mm/yyy hh:mm">
                            </div>
{{--{{ dd(date('m/d/Y H:i:sP', strtotime($schedule->from_date))) }}--}}
                            <div class="mb-3 col-6">
                                <label for="address" class="form-label">Thời gian đi<span
                                        class="obligatory">(*)</span></label>
                                <input type="datetime-local" class="form-control" name="to_date" value="{{ old('to_date') }}" id="to_date" placeholder="dd/mm/yyy hh:mm">
                            </div>
                        </div>

                        <div class="row my-4">
                            <div class=" col-md-4">
                                <label for="province" class=" form-label">Tỉnh thành<span
                                        class="obligatory">(*)</span></label>
                                <select name="province_id" id="province" class="chosen form-control  form-select">
                                    <option value="0">-- Chọn tỉnh/thành --</option>
                                    @foreach($provinces as $key => $province)
                                        <option value="{{ $province->id }}" {{ $schedule->province_id == $province->id ? "selected" : "" }}> {{$province->_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-md-4">
                                <label for="district" class="form-label">Quận/huyện <span
                                        class="obligatory">(*)</span></label>
                                <select name="district_id" id="district" class="chosen form-select ">
                                    <option value="0">-- Chọn Quận/huyện --</option>
                                    @foreach($districts as $key => $district)
                                        <option value="{{ $district->id }}" {{ $schedule->district_id == $district->id ? "selected" : "" }}> {{ $district->_prefix." ".$district->_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-md-4">
                                <label for="ward_id" class="form-label">Phường/xã <span
                                        class="obligatory">(*)</span></label>
                                <select name="ward_id" id="ward" class="chosen form-control form-select">
                                    <option value="0">-- Chọn Phường/xã --</option>
                                    @foreach($wards as $key => $ward)
                                        <option value="{{ $ward->id }}" {{ $schedule->ward_id == $ward->id ? "selected" : "" }}> {{ $ward->_prefix." ".$ward->_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="mb-3 ">
                            <label for="address" class="form-label">Số nhà, phố, tổ dân phố/thôn/đội <span
                                    class="obligatory">(*)</span></label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') ? old('address') : $schedule->address }}" id="address" placeholder="Nhập Số nhà, phố, tổ dân phố/thôn/đội ">
                        </div>
                        <div class="mb-3 ">
                            <label for="address" class="form-label">Ghi chú</label>
                            <textarea class="form-control" name="note" id="note" cols="" rows="5" placeholder="Ghi chú địa điểm">{{ old('note') ? old('note') : $schedule->note }}</textarea>
                        </div>
                        <div class="text-center">
                            <a href="{{ route("user.destination.index") }}" class="btn btn-info text-white">Quay lại</a>
                            <button type="submit" class="btn btn-primary btn-submit btn-success">Xác nhận</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function(){
                $("#province").change(function(){
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('district.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            $("#district").html(`<option value="0">-- Chọn Quận/Huyện --</option>`);
                            $("#ward").html(`<option value="0">-- Chọn Phường/Xã --</option>`);
                            // $("#district").attr('disabled', false);

                            $.each( responsive, function( key, value ) {
                                $("#district").append(`<option value="${value['id']}"> ${value['_prefix']} ${value['_name']} </option>`);
                            });
                        }
                    });
                });
                $("#district").change(function(){
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('ward.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            console.log(responsive);
                            $("#ward").html(`<option value="0">-- Chon Phường/Xã --</option>`);
                            // $("#ward").attr('disabled', false);
                            $.each( responsive, function( key, value ) {
                                $("#ward").append(`<option value="${value['id']}"> ${value['_prefix']} ${value['_name']} </option>`);
                            });
                        }
                    });
                });
            });
            $('.chosen').select2({
                selectOnClose: true
            });
        </script>
    @endpush
@endsection
