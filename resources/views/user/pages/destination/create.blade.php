@extends('user.layout.main',['title' => 'Thêm nhật ký điểm đến'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item">Quản lý điểm đến</li>
                    <li class="breadcrumb-item active" aria-current="page">Thêm mới điểm đến</li>
                </ol>
            </nav>
            @include("user.shared._button-back")
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Thêm nhật ký điểm đến</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin địa điểm</b>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form action="{{ route('user.destination.store') }}" class="p-5" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-6">
                                <label for="name" class="form-label">Tên địa điểm<span
                                        class="obligatory">(*)</span></label>
                                <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}" placeholder="Nhập tên địa điểm">
                            </div>
                            <div class="mb-3 col-6"></div>
                            <div class="mb-3 col-6">
                                <label for="address" class="form-label">Thời gian đến<span
                                        class="obligatory">(*)</span></label>
                                <input type="datetime-local" class="form-control" name="from_date" value="{{ old('from_date', date('Y-m-d ; hh:mm')) }}" id="from_date" placeholder="dd/mm/yyy hh:mm">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="address" class="form-label">Thời gian đi<span
                                        class="obligatory">(*)</span></label>
                                <input type="datetime-local" class="form-control" name="to_date" value="{{ old('to_date', date('Y-m-d ; hh:mm')) }}" id="to_date" placeholder="dd/mm/yyy hh:mm">
                            </div>
                        </div>

                        @include('user.pages.location')
                        <div class="mb-3 ">
                            <label for="address" class="form-label">Số nhà, phố, tổ dân phố/thôn/đội <span
                                    class="obligatory">(*)</span></label>
                            <input type="text" class="form-control" name="address" value="{{ old("address") }}" id="address" placeholder="Nhập Số nhà, phố, tổ dân phố/thôn/đội ">
                        </div>
                        <div class="mb-3 ">
                            <label for="address" class="form-label">Ghi chú</label>
                            <textarea class="form-control" name="note" value="{{ old("note") }}" id="note" cols="" rows="5" placeholder="Ghi chú địa điểm"></textarea>
                        </div>
                        <div class="text-center">
                            <a href="{{ route("user.destination.index") }}" class="btn btn-info text-white">Quay lại</a>
                            <button type="submit" class="btn btn-primary btn-submit btn-success">Xác nhận</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
