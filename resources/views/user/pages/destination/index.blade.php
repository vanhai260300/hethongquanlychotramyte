@extends('user.layout.main',['title' => 'Quản lý địa điểm'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item active" aria-current="page">Quản lý điểm đến</li>
                </ol>
            </nav>
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Quản lý địa điểm</h1>
                </div>
                <div class="declaration-form">
                    <div class="container">
                        <div class="row my-3">
                            <div class="right-option col-12">
                                <form action="" class="row" method="get">
                                    <div class="mb-3 col-4">
                                        <label for="name" class="form-label">Tên địa điểm<span
                                                class="obligatory">(*)</span></label>
                                        <input type="text" name="name" class="form-control" id="name" value="{{ $name }}" placeholder="Nhập tên địa điểm">
                                    </div>
                                    <div class="mb-3 col-3">
                                        <label for="from_date" class="form-label">Từ ngày:<span
                                                class="obligatory">(*)</span></label>
                                        <input type="date" name="from_date" class="form-control" id="from_date" value="{{ $from_date }}" >
                                    </div>
                                    <div class="mb-3 col-3">
                                        <label for="name" class="form-label">Đến ngày:<span
                                                class="obligatory">(*)</span></label>
                                        <input type="date" name="to_date" class="form-control" id="to_date" value="{{ $to_date }}">
                                    </div>
                                    <div class="mb-3 col-2">
                                        <label for="name" class="form-label">.</label>
                                        <input type="submit" class="form-control btn btn-md btn-primary" value="Tìm kiếm">
                                    </div>
                                    <div class="col-6 d-flex">
                                        <a href="{{ route('user.destination.create') }}" style="font-weight: 600" class="btn btn-primary" tabindex="0" ><i  class="anticon anticon-plus ng-star-inserted" style="display: inline-block;"><svg viewBox="64 64 896 896" fill="currentColor" width="1em" height="1em" data-icon="plus" aria-hidden="true"><defs><style></style></defs><path d="M482 152h60q8 0 8 8v704q0 8-8 8h-60q-8 0-8-8V160q0-8 8-8z"></path><path d="M176 474h672q8 0 8 8v60q0 8-8 8H176q-8 0-8-8v-60q0-8 8-8z"></path></svg></i><span> Thêm mới địa điểm </span></a>
                                        <a href="{{ route('user.destination.index') }}" style="font-weight: 600" class="btn btn-info text-white ms-3" tabindex="0" ><i class="fas fa-retweet"></i><span> Làm mới </span></a>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <table class="table table-bordered table-hover table-striped inlineEditTable">
                            <thead class="noselect">
                            <tr style="height: 57.5px;">
                                <th scope="col" >STT</th>
                                <th scope="col" >Tên địa điểm</th>
                                <th scope="col" >Thời gian đến</th>
                                <th scope="col">Thời gian đi</th>
                                <th scope="col">Ghi chú</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($schedules as $key => $schedule)
                                    <tr>
                                        <td style=" text-align: center;">{{ $key + 1 }}</td>
                                        <td >
                                            <p><b>{{ $schedule->name }}</b></p>
                                            <p>{{ $schedule->address.", ".$schedule->ward->_name.", ".$schedule->district->_name.", ".$schedule->province->_name }}</p>
                                        </td>
                                        <td >{{ date('d-m-Y H:i:s', strtotime($schedule->from_date)) }}</td>
                                        <td >{{ date('d-m-Y H:i:s', strtotime($schedule->to_date)) }}</td>
                                        <td>{{ $schedule->note }}</td>
                                        <td class="text-center" >
{{--                                            <a href="{{ route('user.destination.index') }}" class="fs-5 text-decoration-none text-success" alt="Xem">--}}
{{--                                                <i class="fas fa-eye"></i>--}}
{{--                                            </a>--}}
                                            <a href="{{ route('user.destination.edit', $schedule->id) }}" class="fs-5 text-decoration-none" alt="Sửa">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="{{ route('user.destination.index') }}" class="fs-5 text-decoration-none text-danger" alt="Xóa">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>{{ $schedules->links() }}</div>
                    </div>
                </div>

            </div>
            <div class="number-pages card-footer clearfix" >
{{--                {{ $schedules->links() }}--}}
            </div>
        </div>
    </div>

@endsection
