@extends('user.layout.main',['title' => 'Trang Chủ'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="content content-News">
                <div class="container container-news">
                    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Trang chủ</li>
                            <li class="breadcrumb-item active" aria-current="page">Tin tức đáng chú ý và thông báo</li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-8">
                            <div class=" magazine-featured-links m-2">
                                <div class=" row magazine-section-heading">
                                    <h5 class="line-head">
                                        Tin tức đáng chú ý
                                    </h5>
                                </div>
                                <div class="row list-magazine">
                                    @foreach($news as $key => $new)
                                    <div class="col-sm-4">
                                        <div class="card">
                                            @if ($new->image != NULL)
                                                <a href="{{ route("user.news_detail", $new->id) }}">
                                                    <img src="{{ $new->image }}" class="card-img-top" alt="...">
                                                </a>
                                            @else
                                                <a href="{{ route("user.news_detail", $new->id) }}">
                                                    <img src="{{ asset("libs/admin/images/news/news.jpg") }}" class="card-img-top" alt="...">
                                                </a>
                                            @endif
                                            <div class="card-body">
                                                <div class="article-title card-title">
                                                    <p itemprop="name" class="cut-title">
                                                        <a id="" href="{{ route("user.news_detail", $new->id) }}">{{ $new->title }}</a>
                                                    </p>
                                                </div>
{{--                                                {!! $new->content !!}--}}
                                                <div class=" card-text magazine-item-ct">
                                                    <p>⏰ Thời gian: {{ $new->created_at }}</p>
                                                    <p class="cut-content"><i class="fas fa-clinic-medical"></i> <a href="{{ route("user.news_detail", $new->id) }}" class="text-black">{{ $new->article->name }}</a></p>
                                                    <p class="cut-content"><a href="{{ route("user.news_detail", $new->id) }}" class="text-black">{{ $new->content }}</a></p>
                                                </div>
                                                <div class="magazine-item-btn">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div>
                                    <div>{{ $news->links() }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 ">
                            <div class="magazine-featured-links m-2">
                                <div class=" row magazine-section-heading">
                                    <h5 class="line-head">
                                        Thông báo
                                    </h5>
                                </div>
                                <div class="row magazine-list">
                                    <div class=" list-group list-group-info">
                                        @foreach($notifications as $key => $notification)
                                        <a href="#" class="list-group-item list-group-item-action">
                                            <div class="d-flex w-100 justify-content-between cut-title">
                                                <h5 class="mb-1">{{ $notification->title }}</h5>
                                            </div>
                                            <div class="cut-title">
                                                <p class="mb-1">{{ $notification->content }}</p>
                                            </div>
                                            <small class="text-muted">{{ date('d-m-Y', strtotime($notification->created_at)) }}</small>
                                        </a>
                                        @endforeach
                                    </div>
                                    <div class="btn-see-more">
                                        <button class="btn btn-primary ">
                                            XEM THÊM
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
