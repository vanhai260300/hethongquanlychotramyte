<div class="modal fade" id="update" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <div class="text-cente">
                    <h5 class="modal-title fw-bold text-white" id="staticBackdropLabel">Cập nhật thông tin ngày</h5>
                </div>
                <button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route("user.health_monitoring.update") }}" method="POST">
                @csrf
                <div class="modal-body row">
                    <div class=" col-4 py-2">
                        <input type="hidden" name="quarantine_id" value="{{ $quarantine_athomes->id }}">
                        <input type="hidden" value="{{ $object->id }}" id="object_id" name="object_id">
                        <input type="hidden" name="id" id="id_health_monitoring">
                        <label for="date_moni" class="form-label fw-bold">Chọn ngày<span class="obligatory"></span></label>
                        <input type="date" name="date_moni" class="form-control date_moni" id="date_moni_ud" min="{{ $quarantine_athomes->quarantine_date }}" value="" placeholder="dd/mm/YYY">
                    </div>
                    <div class=" col-4 py-2">
                        <label for="temperature" class="form-label fw-bold">Nhiệt độ (&ordm;C) <span class="obligatory"></span></label>
                        <input type="number" name="temperature" class="form-control" step="0.01" id="temperature_ud" value="{{ old('temperature') }}" placeholder="&ordm; C">
                    </div>
                    <div class=" col-4 py-2">
                        <label for="breathing" class="form-label fw-bold">Nhịp thở<span class="obligatory"></span></label>
                        <input type="number" name="breathing" class="form-control" step="0.01" id="breathing_ud" value="{{ old('breathing') }}" placeholder="Nhịp thở">
                    </div>
                    <div class=" col-4 py-2">
                        <label for="blood_pressure" class="form-label fw-bold">Huyết áp (mmhg)<span class="obligatory"></span></label>
                        <input type="number" name="blood_pressure" class="form-control" step="0.01" id="blood_pressure_ud" value="{{ old('blood_pressure') }}" placeholder="mmhg">
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="fever" class="form-label fw-bold">Sốt<span class="obligatory"></span></label>
                        <select name="fever" id="fever	" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="cough" class="form-label fw-bold">Ho<span class="obligatory"></span></label>
                        <select name="cough" id="cough" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="difficulty_of_breathing" class="form-label fw-bold">Khó thở<span class="obligatory"></span></label>
                        <select name="difficulty_of_breathing" id="difficulty_of_breathing" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="diarrhea" class="form-label fw-bold">Tiêu chảy<span class="obligatory"></span></label>
                        <select name="diarrhea" id="diarrhea" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="chills" class="form-label fw-bold">Ớn lạnh<span class="obligatory"></span></label>
                        <select name="chills" id="chills	" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="tired" class="form-label fw-bold">Mệt mỏi<span class="obligatory"></span></label>
                        <select name="tired" id="tired" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="loss_taste" class="form-label fw-bold">Mất vị giác<span class="obligatory"></span></label>
                        <select name="loss_taste" id="loss_taste" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-md-4 py-2">
                        <label for="suspicious" class="form-label fw-bold">Khả nghi<span class="obligatory"></span></label>
                        <select name="suspicious" id="suspicious" class="form-control form-select">
                            <option value="0">Không</option>
                            <option value="1">Có</option>
                        </select>
                    </div>
                    <div class=" col-12 py-2">
                        <label for="last_injection" class="form-label fw-bold">Ghi chú<span class="obligatory"></span></label>
                        <textarea class="form-control" name="note" id="note" rows="3">{{ old("note") }}</textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Đóng</button>
                    <input type="submit" class="btn btn-primary" value="Xác nhận">
                </div>
            </form>
        </div>
    </div>
</div>
