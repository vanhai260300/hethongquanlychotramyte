@extends('user.layout.main',['title' => 'Đăng ký'])
@section('content')
    <div class="content py-5">
        <div class="form_health_declaration">
            <div class="title">
                <div class="national-brand text-center mb-15">
                    <div id="wrapper9" class="passenger-wrapper">
                        <h1 class="text-center mtb-20">Nhập mã xác thực</h1>
                    </div>
                    <div class="">
                        <p>Chúng tôi vừa gửi 1 mã qua email {{ session('verification') }}, nhập mã để xác thực tài khoản</p>
                    </div>
                </div>
            </div>
        </div>
        <form action="{{ route('user.auth.verification') }}" method="POST">
            @csrf
{{--            {{ method_field('PUT') }}--}}
            <div class="mb-3 offset-4 col-4">
                <label for="name" class="form-label">Nhập mã xác thực tài khoản<span
                        class="obligatory">(*)</span></label>
                <input type="hidden" name="email" value="{{ session('verification') }}">
                <input type="text" name="token" class="form-control" id="name" value="{{ old('token') }}" placeholder="Nhập mã xác thực">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary btn-submit btn-success" >Xác thực</button>
            </div>
        </form>
    </div>
@endsection
