@extends('user.layout.main',['title' => 'Đăng ký'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Đăng ký tài khoản</h1>
                </div>
                <div class="declaration-form container px-5">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Thông tin đăng ký</b>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    @error('password')--}}
{{--                    <small class="text-danger">{{ $message }}</small>--}}
{{--                    @enderror--}}
                    <form action="{{ route('user.auth.register') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="mb-3 col-6">
                                <label for="name" class="form-label">Họ Tên<span
                                        class="obligatory">(*)</span></label>
                                @php
                                    $isvalid = '';
                                @endphp
                                @error('name')
                                    @php
                                        $isvalid = 'is-invalid';
                                    @endphp
                                @enderror
                                <input type="text" name="name" class="form-control {{ $isvalid }}" id="name" value="{{ old('name') }}" placeholder="Nhập họ tên">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="email" class="form-label">Email<span
                                        class="obligatory"></span></label>
                                @php
                                    $isvalid = '';
                                @endphp
                                @error('email')
                                    @php
                                        $isvalid = 'is-invalid';
                                    @endphp
                                @enderror
                                <input type="email" class="form-control {{ $isvalid }}" name="email" id="email" value="{{ old('email') }}" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="password" class="form-label">Mật khẩu<span
                                        class="obligatory">(*)</span></label>
                                @php
                                    $isvalid = '';
                                @endphp
                                @error('password')
                                    @php
                                        $isvalid = 'is-invalid';
                                    @endphp
                                @enderror
                                <input type="password" class="form-control {{ $isvalid }}" name="password" value="{{ old('password') }}" id="password" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="password_confirmation" class="form-label">Nhập lại mật khẩu<span
                                        class="obligatory">(*)</span></label>

                                <input type="password" class="form-control"  name="password_confirmation" value="{{ old('password_confirmation') }}" id="password_confirmation" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3 col-6">

                                <label for="phone_number" class="form-label">Số điện thoại<span
                                        class="obligatory">(*)</span></label>
                                @php
                                    $isvalid = '';
                                @endphp
                                @error('phone_number')
                                    @php
                                        $isvalid = 'is-invalid';
                                    @endphp
                                @enderror
                                <input type="number" class="form-control {{ $isvalid }}" name="phone_number" value="{{ old('phone_number') }}" id="phone_number" placeholder="Nhập số điện thaoi">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="CMND" class="form-label">CMND/CCCD<span
                                        class="obligatory">(*)</span></label>
                                @php
                                    $isvalid = '';
                                @endphp
                                @error('CMND')
                                    @php
                                        $isvalid = 'is-invalid';
                                    @endphp
                                @enderror
                                <input type="text" class="form-control {{ $isvalid }}" name="CMND" value="{{ old('CMND') }}" id="CMND" placeholder="Nhập CMND/CCCD">
                            </div>
                            <div class="mb-3 col-6">
                                <label for="birthdate" class="form-label">Ngày tháng năm sinh</label>
                                <input type="date" class="form-control" name="birthdate" value="{{ old('birthdate', date('Y-m-d')) }}" id="birthdate" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3 col-6 ">
                                <label for="gender" class="form-label">Giới tính <span
                                        class="obligatory">(*)</span></label>
                                <div class="d-flex">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" @if(old('gender') == 1) checked @endif name="gender" id="genderm"
                                               checked value="1">
                                        <label class="form-check-label" for="genderm">
                                            Nam
                                        </label>
                                    </div>
                                    <div class="form-check mx-4">
                                        <input class="form-check-input" type="radio" @if(old('gender') == 2) checked @endif name="gender" id="genderf" value="2">
                                        <label class="form-check-label" for="genderf">
                                            Nữ
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" @if(old('gender') == 3) checked @endif name="gender" id="gendero" value="3">
                                        <label class="form-check-label" for="gendero">
                                            Khác
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @include('user.pages.location')

                        <div class=" col-md-4 my-3">
                            <label for="medical_station_id" class="form-label">Trạm tế<span
                                    class="obligatory">(*)</span></label>
                            @php
                                $isvalid = '';
                            @endphp
                            @error('medical_station_id')
                                @php
                                    $isvalid = 'is-invalid';
                                @endphp
                            @enderror
                            <select name="medical_station_id" id="medical_station_id" class="chosen form-control form-select {{ $isvalid }}">
                                <option value="0">-- Chọn trạm y tế --</option>
                            </select>
                        </div>
                        <div class="mb-3 ">
                            <label for="address" class="form-label">Số nhà, phố, tổ dân phố/thôn/đội <span
                                    class="obligatory">(*)</span></label>
                            @php
                                $isvalid = '';
                            @endphp
                            @error('address')
                                @php
                                    $isvalid = 'is-invalid';
                                @endphp
                            @enderror
                            <input type="text" class="form-control {{ $isvalid }}" name="address" id="address" placeholder="Số nhà, phố, tổ dân phố/thôn/đội">
                        </div>
                        <p class="text-danger">Dữ liệu bạn cung cấp hoàn toàn bảo mật và chỉ phục vụ cho việc phòng chống dịch,
                            thuộc quản lý của Ban chỉ đạo quốc gia về Phòng chống dịch Covid-19. Khi bạn nhấn nút "Gửi" là bạn đã
                            hiểu và đồng ý.</p>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-submit btn-success">Đăng Ký</button>
                     </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script>
            $(document).ready(function(){
                $("#medical_station_id").attr('disabled', true);
                $("#ward").change(function(){
                    console.log($(this).val());
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('medical-station.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            console.log(responsive);
                            $("#medical_station_id").html(`<option value="0">-- Chọn trạm y tế --</option>`);
                            $("#medical_station_id").attr('disabled', false);
                            $.each( responsive, function( key, value ) {
                                $("#medical_station_id").append(`<option value="${value['id']}">  ${value['name']} </option>`);
                            });

                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
