@extends('user.layout.main',['title' => 'Danh sách đăng ký tiêm vacxin'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Trang chủ</li>
                    <li class="breadcrumb-item active" aria-current="page">Đăng ký tiêm vaccin</li>
                </ol>
            </nav>
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Đăng ký tiêm Vacxin</h1>
                </div>
                <div class="declaration-form">
                    <div class="container">
                        <div class="row my-3">
                            <div class="right-option col-12">
                                <div class="row">
                                    <div class="col-2 d-block">
                                        <label for="relationship_of_guardian" class="form-label">.<span
                                                class="obligatory"></span></label>
                                        <div>
                                            <a href="{{ route('user.vaccination.index') }}" style="font-weight: 600" class="btn btn-info text-white" tabindex="0" ><i class="fas fa-retweet"></i><span> Làm mới </span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <form action="{{ route('user.vaccination.redirect') }}" id="formmain" method="POST">
                                            @csrf
                                            <div class="row col-12 d-flex">
                                                <div class="col-6">
                                                    <label for="relationship_of_guardian" class="form-label">Thêm mới đối tượng đã từng khai hộ<span
                                                            class="obligatory"></span></label>
                                                    <select name="object_of_user" id="relationship_of_guardian" class="form-control form-select">
                                                        <option disabled selected>-- Chọn --</option>
                                                        @foreach($object_of_users as $key => $object_of_user)
                                                            @if($object_of_user->id ==  Auth::id())
                                                                <option value="{{ $object_of_user->id }}" {{ $object_of_user->id == old("object_of_user") ? "selected" : "" }}>Đăng ký cho bản thân</option>
                                                            @else
                                                                <option value="{{ $object_of_user->id }}" {{ $object_of_user->id == old("object_of_user") ? "selected" : "" }}>{{ $object_of_user->name }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class=" col-1">
                                                    <label for="submit" class="form-label">.<span class="obligatory"></span></label>
                                                    <button type="submit" class="btn btn-primary btn-primary" id="formmain"><i class="fas fa-plus"></i></button>
                                                </div>
                                                <div class=" col-3">
                                                    <label for="submit" class="form-label">Hoặc<span class="obligatory"></span></label>
                                                    <div>
                                                        <a href="{{ route('user.vaccination.create-other') }}" type="submit" class="btn btn-primary btn-primary">Thêm mới</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <table class="table table-bordered table-hover table-striped inlineEditTable">
                            <thead class="noselect">
                            <tr style="height: 57.5px;">
                                <th scope="col" >STT</th>
                                <th scope="col" >Họ tên</th>
                                <th>Số điện thoại</th>
                                <th>CMND</th>
                                <th>Năm Sinh</th>
                                <th scope="col" >Mũi tiêm thứ</th>
                                <th scope="col">Thời gian mũi gần nhất</th>
                                <th scope="col">Loại Vacxin</th>
                                <th>Đăng ký lúc</th>
                                <th scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="border-end-0 text-center">
                                        @php
                                            $countobject = 0;
                                        @endphp
                                        @foreach ($registerVaccinations as $key => $registerVaccination)
                                            @if ($registerVaccination->object->id == Auth::id())
                                                @php
                                                    $countobject = 1;
                                                @endphp
                                            @endif
                                        @endforeach
                                        @if($countobject == 0)
                                            <a href="{{ route('user.vaccination.create') }}" class="text-info"><i class="fas fa-plus-circle"></i></a>
                                        @endif
                                    </td>
                                    <td colspan="9" class="border-start-0 fw-bold">Đăng ký cho bản thân</td>
                                </tr>
                                @foreach ($registerVaccinations as $key => $registerVaccination)
                                    @if ($registerVaccination->object->id == Auth::id())
                                        <tr class="row_{{ $registerVaccination->object->id }}">
                                            <td style="width: 50px; text-align: center;" data-column-index="1">1</td>
                                            <td data-column-index="2">{{ $registerVaccination->object->name }}</td>
                                            <td data-column-index="3">{{ $registerVaccination->object->phone_number }}</td>
                                            <td data-column-index="5">{{ $registerVaccination->object->CMND }}</td>
                                            <td data-column-index="6">{{ $registerVaccination->object->birthdate }}</td>
                                            <td data-column-index="7">
                                                <span class="">{{ $registerVaccination->injection == 1 ? 'Mũi đầu' : $registerVaccination->injection }}</span>
                                            </td>
                                            <td data-column-index="6">{{ $registerVaccination->last_injection }}</td>
                                            <td>{{ $registerVaccination->type_vaccine }}</td>
                                            <td>{{ $registerVaccination->updated_at }}</td>
                                            {{--                                    <td data-column-index="8">Bình thường</td>--}}
                                            <td class="text-center" data-column-index="9">
                                                <a href="{{ route('user.vaccination.show', $registerVaccination->object->id) }}" class="text-decoration-none" alt="Xem chi tiết">
                                                    <img src="{{ asset('libs/user/imgs/icon-view.png') }}" alt="">
                                                </a>
                                                <a href="{{ route('user.vaccination.edit', $registerVaccination->object->id) }}" class="text-decoration-none" alt="Cập nhật tờ khai">
                                                    <img src="{{ asset('libs/user/imgs/bg-qr-yt.png') }}" alt="">
                                                </a>
                                                <a onclick="NotiDeleteDeclara({{$registerVaccination->object->id}})" class="text-danger fs-5" alt="Xóa">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <td class="border-end-0 text-center"><a href="{{ route('user.vaccination.create-other') }}" class="text-info"><i class="fas fa-plus-circle"></i></a></td>
                                    <td colspan="9" class="border-start-0 fw-bold">Đăng ký cho người thân</td>
                                </tr>
                                @foreach ($registerVaccinations as $key => $registerVaccination)
                                    @if ($registerVaccination->object->id != Auth::id())
                                        <tr class="row_{{ $registerVaccination->object->id }}">
                                            <td style="width: 50px; text-align: center;" data-column-index="1">{{ $key+1 }}</td>
                                            <td data-column-index="2">{{ $registerVaccination->object->name }}</td>
                                            <td data-column-index="3">{{ $registerVaccination->object->phone_number }}</td>
                                            <td data-column-index="5">{{ $registerVaccination->object->CMND }}</td>
                                            <td data-column-index="6">{{ $registerVaccination->object->birthdate }}</td>
                                            <td data-column-index="7">
                                                <span class="">{{ $registerVaccination->injection == 1 ? 'Mũi đầu' : $registerVaccination->injection }}</span>
                                            </td>
                                            <td data-column-index="6">{{ $registerVaccination->last_injection }}</td>
                                            <td>{{ $registerVaccination->type_vaccine }}</td>
                                            <td>{{ $registerVaccination->updated_at }}</td>
                                            {{--                                    <td data-column-index="8">Bình thường</td>--}}
                                <td class="text-center" data-column-index="9">
                                    <a href="{{ route('user.vaccination.show', $registerVaccination->object->id) }}" class="text-decoration-none" alt="Xem chi tiết">
                                        <img src="{{ asset('libs/user/imgs/icon-view.png') }}" alt="">
                                    </a>
                                    <a href="{{ route('user.vaccination.edit-other', $registerVaccination->object->id) }}" class="text-decoration-none" alt="Cập nhật tờ khai">
                                        <img src="{{ asset('libs/user/imgs/bg-qr-yt.png') }}" alt="">
                                    </a>
                                    <a onclick="NotiDeleteDeclara({{$registerVaccination->object->id}})" class="text-danger fs-5" alt="Xóa">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="number-pages card-footer clearfix" >
                {{--                {{ $schedules->links() }}--}}
            </div>
        </div>
    </div>
    @push("custom-scripts")
        <script>
            function NotiDeleteDeclara(id){
                Swal.fire({
                    icon: 'warning',
                    title: 'Bạn chắc chắn muốn xóa?',
                    text: 'Xóa bản khai báo này!',
                    showCancelButton: true,
                    confirmButtonText: '<a style = "Color:White;" onclick="deleteDeclara(' + id + ')">Xóa</a>',
                    cancelButtonText: 'Quay lại'
                });
            }
            function deleteDeclara(object_id){
                $(".row_"+object_id).remove();
                $.ajax({
                    url: '{{ route("user.vaccination.destroy", 1) }}',
                    method: 'DELETE',
                    data: {
                        id: object_id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(result) {
                        console.log(result);
                        $("#row_" + object_id).remove();
                        NotificationSuccess();
                    }
                });
            }
            function NotificationSuccess(){
                Swal.fire({
                    icon: 'success',
                    title: 'Xóa thành công?',
                    confirmButtonText: 'Okey',
                });
            }
        </script>
    @endpush
@endsection
