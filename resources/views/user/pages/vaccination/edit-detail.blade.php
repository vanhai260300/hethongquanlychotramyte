
<div class="row py-3">
    @include("user.pages.shared._guardian-edit", ["var_guardian" => $registerVaccine])
    <div class=" col-md-4">
        <label for="medical_station_id" class="form-label">Đăng kí mũi tiêm thứ <span
                class="obligatory">(*)</span></label>
        <select name="injection" id="injection" class="form-control form-select">
            <option value="1" {{ (old("injection") ? old("injection") : $registerVaccine->injection) == 1 ? "selected" : "" }}>Mũi tiêm thứ nhất</option>
            <option value="2" {{ (old("injection") ? old("injection") : $registerVaccine->injection) == 2 ? "selected" : "" }}>Mũi tiếp tiếp theo</option>
        </select>
    </div>
    <div class=" col-md-4">
        <label for="vaccin_id" class="form-label">Loại vacxin đã tiêm<span
                class="obligatory"></span></label>
        <select name="vaccin_id" id="vaccin_id" class="form-control form-select">
            <option value="0">Chọn loại</option>
            @foreach($vaccines as $key => $vaccine)
                <option value="{{ $vaccine->id }}" {{ $vaccine->id == (old("vaccin_id") ? old("vaccin_id") : $registerVaccine->type_vaccine) ? "selected" : "" }}>{{ $vaccine->name }}</option>
            @endforeach
        </select>
    </div>
    <div class=" col-4">
        <label for="last_injection" class="form-label">Thời gian mũi tiêm gần nhất <span
                class="obligatory"></span></label>
        <input type="date" name="last_injection" class="form-control" id="last_injection" value="{{ (old("last_injection") ? old("last_injection") : $registerVaccine->last_injection) }}" placeholder="dd/mm/YYY">
    </div>
</div>
<table class="table table-bordered">
    <thead class="table-light">
    <tr>
        <th>Bệnh nền</th>
        <th class="text-bold">Có</th>
        <th class="text-bold">Không</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="col-10">
            1. Tiền sử phản vệ từ độ 2 trở lên <span class="obligatory ">(*)</span>
        </td>
        <td class="col-1">
            <input class="form-check-input" type="radio" name="tsphanve" {{ (old("tsphanve") ? old("tsphanve") : $registerVaccine->tsphanve) == 1 ? "checked" : "" }} id="tsphanve" value="1">
        </td>
        <td class="col-1">
            <input class="form-check-input" type="radio" name="tsphanve" {{ (old("tsphanve") ? old("tsphanve") : $registerVaccine->tsphanve) == 0 ? "checked" : "" }} id="tsphanve"  value="0">
        </td>
    </tr>
    <tr>
        <td>
            2. Tiền sử bị COVID-19 trong vòng 6 tháng <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="tscovid" {{ (old("tscovid") ? old("tscovid") : $registerVaccine->tscovid) == 1 ? "checked" : "" }} id="tscovid" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="tscovid" {{ (old("tscovid") ? old("tscovid") : $registerVaccine->tscovid) == 0 ? "checked" : "" }} id="tscovid"  value="0">
        </td>
    </tr>
    <tr>
        <td>
            3. Tiền sử suy giảm miễn dịch, ung thư giai đoạn cuối, cắt lách, xơ gan mất bù ... <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="tssuygiammd" {{ (old("tssuygiammd") ? old("tssuygiammd") : $registerVaccine->tsphanve) == 1 ? "checked" : "" }} id="tssuygiammd" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="tssuygiammd" {{ (old("tssuygiammd") ? old("tssuygiammd") : $registerVaccine->tsphanve) == 0 ? "checked" : "" }} id="tssuygiammd"  value="0">
        </td>
    </tr>
    <tr>
        <td>
            4. Bệnh cấp tính <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="benhcaptinh" {{ (old("benhcaptinh") ? old("benhcaptinh") : $registerVaccine->benhcaptinh) == 1 ? "checked" : "" }} id="benhcaptinh" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="benhcaptinh" {{ (old("benhcaptinh") ? old("benhcaptinh") : $registerVaccine->benhcaptinh) == 0 ? "checked" : "" }} id="benhcaptinh"  value="0">
        </td>
    </tr>
    <tr>
        <td>
            5. Tiền sử bệnh mạn tính, đang tiến triển <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="tsmantinh" {{ (old("tsmantinh") ? old("tsmantinh") : $registerVaccine->tsmantinh) == 1 ? "checked" : "" }} id="tsmantinh" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="tsmantinh" {{ (old("tsmantinh") ? old("tsmantinh") : $registerVaccine->tsmantinh) == 0 ? "checked" : "" }} id="tsmantinh"  value="0">
        </td>
    </tr>
    <tr>
        <td>
            6. Độ tuổi: ≥65 tuổi <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="age" {{ (old("age") ? old("age") : $registerVaccine->age) == 1 ? "checked" : "" }} id="age" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="age" {{ (old("age") ? old("age") : $registerVaccine->age) == 0 ? "checked" : "" }} id="age"  value="0">
        </td>
    </tr>
    <tr>
        <td>
            7. Đang mang thai, phụ nữ đang nuôi con bằng sữa mẹn <span class="obligatory">(*)</span>
        </td>
        <td>
            <input class="form-check-input" type="radio" name="mangthai" {{ (old("mangthai") ? old("mangthai") : $registerVaccine->mangthai) == 1 ? "checked" : "" }} id="mangthai" value="1">
        </td>
        <td>
            <input class="form-check-input" type="radio" name="mangthai" {{ (old("mangthai") ? old("mangthai") : $registerVaccine->mangthai) == 0 ? "checked" : "" }} id="mangthai"  value="0">
        </td>
    </tr>

    </tbody>
</table>
<div class="mb-3 ">
    <label for="address" class="form-label">Ghi chú (Tiền sử dị ứng với các dị nguyên khác)</label>
    <textarea class="form-control" name="note" id="note" cols="" rows="5">{{ old('note') ? old('note') : $registerVaccine->note }}</textarea>
</div>
<div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
    <label for="password" class="col-md-4 control-label py-2">Mã bảo mật<span
            class="obligatory">(*)</span></label>
    <div class="col-md-6 ">
        <div class="row">
            <div class="captcha col-sm-4">
                <span>{!! captcha_img() !!}</span>
                <button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
            </div>
            <div class="col-sm-8">
                <input id="captcha" type="text" class="form-control" placeholder="Nhập mã bảo mật" name="captcha">

            </div>

        </div>
        @if ($errors->has('captcha'))
            <span class="help-block">
          <strong>{{ $errors->first('captcha') }}</strong>
      </span>
        @endif
    </div>
</div>
<ul class="py-4">
    <p class="text-danger fw-bold">Lưu ý</p>
    <li class="text-danger">Việc đăng ký thông tin hoàn toàn bảo mật và phục vụ cho chiến dịch tiêm chủng Vắc xin COVID - 19</li>
    <li class="text-danger">Bằng việc nhấn nút "Xác nhận", bạn hoàn toàn hiểu và đồng ý chịu trách nhiệm với các thông tin đã cung cấp.</li>
    <li class="text-danger">Nếu bạn dưới 18 tuổi, vui lòng nhập số điện thoại của người giám hộ để tra cứu</li>
</ul>

<div class="text-center">
    <a href="{{ route("user.vaccination.index") }}" class="btn btn-info text-white">Quay lại</a>
    <button type="submit" class="btn btn-primary btn-success">Xác nhận đăng ký</button>
</div>
@push('custom-scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            let injection = $("#injection");
            let vaccin_id = $("#vaccin_id");
            let last_injection = $("#last_injection");
            // vaccin_id.attr('disabled', true);
            // last_injection.attr('disabled', true);
            console.log(injection.value);
            if (injection.val() == 1){
                vaccin_id.hide();
                last_injection.hide();
                // vaccin_id.attr('disabled', true);
                // last_injection.attr('disabled', true);
                // $('#vaccin_id option[value="0"]').attr('selected', true);
                // last_injection.html(`<input type="date" name="last_injection" class="form-control" value="NULL" id="last_injection" placeholder="dd/mm/YYY">`);
            } else {
                vaccin_id.show();
                last_injection.show();
                // $("#vaccin_id").attr('disabled', false);
                // $("#last_injection").attr('disabled', false);
                // $('#vaccin_id option[value="0"]').attr('selected', false);
            }
            injection.change(function (e){
                if(this.value == 1){
                    // console.log(injection.val());
                    vaccin_id.hide();
                    last_injection.hide();
                    // vaccin_id.attr('disabled', true);
                    // last_injection.attr('disabled', true);
                    // $('#vaccin_id option[value="0"]').attr('selected', true);
                    // last_injection.html(`<input type="date" name="last_injection" class="form-control" value="NULL" id="last_injection" placeholder="dd/mm/YYY">`);
                } else {
                    vaccin_id.show();
                    last_injection.show();
                    // $("#vaccin_id").attr('disabled', false);
                    // $("#last_injection").attr('disabled', false);
                    // $('#vaccin_id option[value="0"]').attr('selected', false);
                }
            })
        });
    </script>
@endpush

