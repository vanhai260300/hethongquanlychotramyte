@extends('user.layout.main',['title' => 'Đăng ký tiêm vaccin'])
@section('content')
    <div class="content py-5">
        <div class="container-fluid">
            <div id="module9" class="ModuleWrapper">
                <div id="wrapper9" class="passenger-wrapper">
                    <h1 class="text-center mtb-20">Cập nhật tiêm vacxin</h1>
                </div>
                <div class="declaration-form px-2">
                    <div class="form_health_declaration">
                        <div class="title">
                            <div class="national-brand text-center mb-15">
                                <div class="text-uppercase">
                                    <b>Cập nhật thông tin đăng ký cho bản thân</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route("user.vaccination.update", $object->id) }}" class="p-5" method="POST">
                        @csrf
                        {{ method_field("PUT") }}
                        @include('user.pages.vaccination.edit-detail')
                    </form>
                </div>
            </div>
        </div>
    </div>
    @push('custom-scripts')
        <script type="text/javascript">
            $(".btn-refresh").click(function(){
                $.ajax({
                    type:'GET',
                    url:'{{ route('user.refresh_captcha') }}',
                    success:function(data){
                        $(".captcha span").html(data.captcha);
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function(){
                $("#medical_station_id").attr('disabled', true);
                $("#ward").change(function(){
                    console.log($(this).val());
                    let id = $(this).val();
                    $.ajax({
                        url: '{{ route('medical-station.get') }}',
                        method:'POST',
                        data: {
                            id : id,
                            _token: '{{csrf_token()}}'
                        },
                        success: function (responsive){
                            console.log(responsive);
                            let medical_station_id = $("#medical_station_id");
                            medical_station_id.html(`<option value="0">-- Chọn trạm y tế --</option>`);
                            medical_station_id.attr('disabled', false);
                            $.each( responsive, function( key, value ) {
                                $("#medical_station_id").append(`<option value="${value['id']}">  ${value['name']} </option>`);
                            });

                        }
                    });
                });
            });
        </script>
    @endpush
@endsection
