@extends('user.layout.main',['title' => 'Chi tiết tin tức'])
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="content content-News">
                <div class="container container-news">
                    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Trang chủ</li>
                            <li class="breadcrumb-item active" aria-current="page">Tin tức</li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-8">
                        <div class=" magazine-featured-links m-2">
                            <h2 class="line-head">
                                {{ $news->title }}
                            </h2>
                            <p>{{ $news->created_at }}</p>
                            <p>{{ $news->article->name }}</p>
                            <div class="row list-magazine">
                                {!! $news->content !!}
                            </div>
                            </div>
                        </div>
                        <div class="col-4 ">
                            <div class="magazine-featured-links m-2">
                                <div class="row list-magazine">
                                    @foreach($newses as $key => $new)
                                        <div class="col-sm-12">
                                            <div class="">
                                                @if ($new->image != NULL)
                                                    <a href="{{ route("user.news_detail", $new->id) }}">
                                                        <img src="{{ $new->image }}" class="card-img-top" width="100px" height="100px" alt="...">
                                                    </a>
                                                @else
                                                    <a href="{{ route("user.news_detail", $new->id) }}">
                                                        <img src="{{ asset("libs/admin/images/news/news.jpg") }}" width="10px" height="140px" class="card-img-top" alt="...">
                                                    </a>
                                                @endif
                                                <div class="">
                                                    <div>
                                                        <p itemprop="name" class="">
                                                            <h5 id="" href="{{ route("user.news_detail", $new->id) }}">{{ $new->title }}</h5>
                                                        </p>
                                                    </div>
                                                    {{--                                                {!! $new->content !!}--}}
                                                    <div >
                                                        <p>⏰ Thời gian: {{ $new->created_at }}</p>
                                                        <p><i class="fas fa-clinic-medical"></i> <a href="{{ route("user.news_detail", $new->id) }}" class="text-black">{{ $new->article->name }}</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
