<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/district', [\App\Http\Controllers\LocationController::class, 'district'])->name('district.get');
Route::post('/ward', [\App\Http\Controllers\LocationController::class, 'ward'])->name('ward.get');
Route::post('/medical-station', [\App\Http\Controllers\LocationController::class, 'medicalStation'])->name('medical-station.get');

//-----------------User----------------
Route::group([
    'as' => 'user.'
],function (){
    Route::get('/',[\App\Http\Controllers\User\HomeController::class, 'index'])->name('home');
    Route::get('/news/{id}',[\App\Http\Controllers\User\HomeController::class, 'newsDetail'])->name('news_detail');
    //---------------- User Auth --------------
    Route::group([
        'as' => 'auth.'
    ],function (){
        Route::get('/login',[\App\Http\Controllers\User\Auth\LoginController::class, 'index'])->name('login');
        Route::post('/login',[\App\Http\Controllers\User\Auth\LoginController::class, 'login'])->name('login');
        Route::get('/register',[\App\Http\Controllers\User\Auth\RegisterController::class, 'index'])->name('register');
        Route::post('/register',[\App\Http\Controllers\User\Auth\RegisterController::class, 'register'])->name('register');
        //----------------Verification Email--------------
        Route::middleware('verifacition')->group(function (){
            Route::get('/verification',[\App\Http\Controllers\User\Auth\RegisterController::class, 'verificationByCode'])
                ->name('verification');
            Route::post('/verification',[\App\Http\Controllers\User\Auth\RegisterController::class, 'verificationByCodePost'])
                ->name('verification');
        });
    });
    Route::middleware('auth')->group(function (){
//        Route::get('/',[\App\Http\Controllers\User\HomeController::class, 'index'])->name('index');
        Route::group([
            'as' => 'declaration.'
        ],function (){
//            Route::get('/',[\App\Http\Controllers\User\DeclarationController::class, 'index'])->name('index');
            Route::get('/declaration',[\App\Http\Controllers\User\DeclarationController::class, 'index'])->name('index');
            Route::get('/declaration-myself',[\App\Http\Controllers\User\DeclarationController::class, 'declarationMyself'])->name('myself');
            Route::post('/declaration-myself',[\App\Http\Controllers\User\DeclarationController::class, 'declarationMyselfPost'])->name('myself');
            Route::get('/declaration-myself-edit/{id}',[\App\Http\Controllers\User\DeclarationController::class, 'declarationMyselfEdit'])->name('myself.edit');
            Route::post('/declaration-myself-edit/{id}',[\App\Http\Controllers\User\DeclarationController::class, 'declarationMyselfPostEdit'])->name('myself.edit');
            Route::get('/declaration-other-people',[\App\Http\Controllers\User\DeclarationController::class, 'declarationOtherPeople'])->name('otherpeople');
            Route::post('/declaration-other-people',[\App\Http\Controllers\User\DeclarationController::class, 'declarationOtherPeoplePost'])->name('otherpeople');
            Route::get('/declaration-other-people-edit/{id}',[\App\Http\Controllers\User\DeclarationController::class, 'declarationOtherPeopleEdit'])->name('otherpeople.edit');
            Route::post('/declaration-other-people-edit/{id}',[\App\Http\Controllers\User\DeclarationController::class, 'declarationOtherPeoplePostEdit'])->name('otherpeople.edit');
            Route::get('/declaration-show-detail/{id}',[\App\Http\Controllers\User\DeclarationController::class, 'declarationShowDetail'])->name('show-detail');
            Route::delete('/declaration-delete',[\App\Http\Controllers\User\DeclarationController::class, 'destroy'])->name('destroy');
            Route::post('/declaration-redirect',[\App\Http\Controllers\User\DeclarationController::class, 'redirect'])->name('redirect');

        });
        Route::resource('vaccination', '\App\Http\Controllers\User\VaccinationController');
        Route::group([
            'as' => 'vaccination.'
        ],function (){
            Route::get('/vaccination-other',[\App\Http\Controllers\User\VaccinationController::class, 'createOther'])->name('create-other');
            Route::post('/vaccination-other',[\App\Http\Controllers\User\VaccinationController::class, 'createOtherPost'])->name('create-other');
            Route::post('/vaccination-redirect',[\App\Http\Controllers\User\VaccinationController::class, 'redirect'])->name('redirect');
            Route::get('/vaccination-other-edit/{id}',[\App\Http\Controllers\User\VaccinationController::class, 'editOther'])->name('edit-other');
            Route::post('/vaccination-other-edit/{id}',[\App\Http\Controllers\User\VaccinationController::class, 'updateOtherPost'])->name('update-other');
            Route::delete('/vaccination-delete',[\App\Http\Controllers\User\VaccinationController::class, 'destroy'])->name('destroy');
        });
        Route::resource('quarantine-athome', '\App\Http\Controllers\User\QuarantineAthomeController');
        Route::group([
            'as' => 'quarantine-athome.'
        ],function (){
            Route::get('/quarantine-athome-other',[\App\Http\Controllers\User\QuarantineAthomeController::class, 'createOther'])->name('create-other');
            Route::post('/quarantine-athome-other',[\App\Http\Controllers\User\QuarantineAthomeController::class, 'storeOther'])->name('store-other');
            Route::post('/quarantine-athome-redirect',[\App\Http\Controllers\User\QuarantineAthomeController::class, 'redirect'])->name('redirect');
            Route::get('/quarantine-athome-other-edit/{id}',[\App\Http\Controllers\User\QuarantineAthomeController::class, 'editOther'])->name('edit-other');
            Route::post('/quarantine-athome-other-edit/{id}',[\App\Http\Controllers\User\QuarantineAthomeController::class, 'updateOther'])->name('update-other');
            Route::delete('/quarantine-athome-delete',[\App\Http\Controllers\User\QuarantineAthomeController::class, 'destroy'])->name('destroy');
        });
        Route::resource('health_monitoring', '\App\Http\Controllers\User\HealthMonitoringController');
        Route::post("health_monitoring/update",[\App\Http\Controllers\User\HealthMonitoringController::class, "update"])->name("health_monitoring.update");
        Route::resource('destination', '\App\Http\Controllers\User\DestinationController');
        Route::get('/refresh_captcha', [\App\Http\Controllers\User\DeclarationController::class, 'refreshCaptcha'])->name('refresh_captcha');
        Route::get('/logout',[\App\Http\Controllers\User\Auth\LoginController::class, 'logout'])->name('logout');
    });
});

//-----------------Medical Station----------------
Route::group([
    'as' => 'medical_station.',
    'prefix' => 'medical-station'
    ],function (){
        Route::group([
            'as' => 'auth.'
        ],function (){
            Route::get('login',[\App\Http\Controllers\MedicalStation\Auth\LoginController::class, 'index'])->name('login');
            Route::post('login',[\App\Http\Controllers\MedicalStation\Auth\LoginController::class, 'login'])->name('login');
//            Route::get('/register',[\App\Http\Controllers\User\Auth\RegisterController::class, 'index'])->name('register');
//            Route::post('/register',[\App\Http\Controllers\User\Auth\RegisterController::class, 'register'])->name('register');
            //----------------Verification Email--------------
//            Route::middleware('verifacition')->group(function (){
//                Route::get('/verification',[\App\Http\Controllers\User\Auth\RegisterController::class, 'verificationByCode'])
//                    ->name('verification');
//                Route::post('/verification',[\App\Http\Controllers\User\Auth\RegisterController::class, 'verificationByCodePost'])
//                    ->name('verification');
//            });
        });
        Route::middleware('medicalstation')->group(function (){
            Route::get("dashboard",[\App\Http\Controllers\MedicalStation\DashboardController::class, "index"])->name("dashboard.index");
            Route::get("/",[\App\Http\Controllers\MedicalStation\DashboardController::class, "index"])->name("dashboard.index");
            // Management Health Declaration
            Route::get("mng-declaration", [\App\Http\Controllers\MedicalStation\MNGDeclarationController::class, "index"])->name("declaration.index");
            Route::get("mng-declaration/{iddeclaration}", [\App\Http\Controllers\MedicalStation\MNGDeclarationController::class, "declarationDetail"])->name("declaration.detail");
            Route::get("export", [\App\Http\Controllers\MedicalStation\MNGDeclarationController::class, "export"])->name("declaration.export");
            // Management Object Register Vaccination
            Route::get("mng-register-vaccination", [\App\Http\Controllers\MedicalStation\MNGVaccinationController::class, "index"])->name("vaccination.index");
            Route::get("mng-register-vaccination/{idvaccination}", [\App\Http\Controllers\MedicalStation\MNGVaccinationController::class, "vaccinationDetail"])->name("vaccination.detail");
            Route::get("export-vaccination", [\App\Http\Controllers\MedicalStation\MNGVaccinationController::class, "export"])->name("vaccination.export");

            Route::get("mng-quantine-athome", [\App\Http\Controllers\MedicalStation\MNGQuarantineAthomeController::class, "index"])->name("quanrantine_athome.index");
            Route::get("mng-quantine-athome/{idquarantine}", [\App\Http\Controllers\MedicalStation\MNGQuarantineAthomeController::class, "quaratineDetail"])->name("quanrantine_athome.detail");
            Route::post("mng-update_medical-staff", [\App\Http\Controllers\MedicalStation\MNGQuarantineAthomeController::class, "updateMedicalStaff"])->name("quanrantine_athome.update_medical_staff");
            Route::post("mng-update_status_quarantine", [\App\Http\Controllers\MedicalStation\MNGQuarantineAthomeController::class, "updateStatusQuarantine"])->name("quanrantine_athome.update_status_quarantine");
            Route::get("mng-update_status_confirm", [\App\Http\Controllers\MedicalStation\MNGQuarantineAthomeController::class, "confirmmm"])->name("quanrantine_athome.confirmmm");
            Route::get("export-quantine-athome", [\App\Http\Controllers\MedicalStation\MNGQuarantineAthomeController::class, "export"])->name("quanrantine_athome.export");

//            Manage Medical Staff
            Route::resource("medical-staff", \App\Http\Controllers\MedicalStation\MNGMedicalStaffController::class);
            Route::resource("medical-station", \App\Http\Controllers\MedicalStation\MNGMedicalStationController::class);
//            Manage News
            Route::resource("news", \App\Http\Controllers\MedicalStation\MNGNewsController::class);
//            Manage Notification
            Route::resource("notification", \App\Http\Controllers\MedicalStation\MNGNotificationController::class);

            Route::get('/logout',[\App\Http\Controllers\MedicalStation\Auth\LoginController::class, 'logout'])->name('logout');
        });
});
