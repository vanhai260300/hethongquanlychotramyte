<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DeclarationExport implements FromCollection, WithHeadings
{
    private $declarations;

    public function __construct($declarations)
    {
        $this->declarations = $declarations;
    }


    public function headings(): array
    {
        return [
                "STT",
                "Ma",
                "Ho Ten",
                "So Dien Thoai",
                "CMND/CCCD",
                "Gioi Tinh",
                "Dia Chi",
                "Khai Bao Luc",
                "Sot",
                "Ho",
                "Dau hong",
                "kho tho",
                "Tieu chay",
                "Mat vi giac",
                "Nguy co"
                ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $declarationss = $this->declarations;
        foreach ($declarationss as $key => $row) {
            $declaration[] = array(
                '0' => $key+1,
                '1' => $row->id,
                '2' => $row->name,
                '3' => $row->phone_number,
                '4' => $row->CMND,
                '5' => $row->gender,
                '6' => $row->address,
                '7' => $row->declaration_date,
                '8' => $row->fever == 1 ? "c" : "k",
                '9' => $row->cough == 1 ? "c" : "k",
                '10' => $row->sore_throat == 1 ? "c" : "k",
                '11' => $row->difficulty_of_breathing == 1 ? "c" : "k",
                '12' => $row->diarrhea == 1 ? "c" : "k",
                '13' => $row->loss_taste == 1 ? "c" : "k",
                '14' => $row->rate_lavel,
            );
        }
        return (collect($declaration));
    }
}
