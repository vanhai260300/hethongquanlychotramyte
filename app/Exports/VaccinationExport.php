<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VaccinationExport implements FromCollection, WithHeadings
{
    private $vaccinations;

    public function __construct($vaccinations)
    {
        $this->vaccinations = $vaccinations;
    }


    public function headings(): array
    {
        return [
            "STT",
            "Mã",
            "Họ tên",
            "Sinh năm",
            "Số điện thoại",
            "CMND/CCCD",
            "Giới tính",
            "Địa chỉ",
            "Đăng ký lúc",
            "Người giám hộ",
            "Số điện thoại",
            "Tiền sử phản vệ",
            "Tiền sử suy giảm miễn dịch",
            "Bệnh cấp tính",
            "Tiền sử bệnh mản tính",
            "Ghi chú"
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $vaccinationss = $this->vaccinations;
        foreach ($vaccinationss as $key => $row) {
            $vaccination[] = array(
                '0' => $key+1,
                '1' => $row->id,
                '2' => $row->name,
                '3' => $row->birthdate,
                '4' => $row->phone_number,
                '5' => $row->CMND,
                '6' => $row->gender,
                '7' => $row->address,
                '8' => $row->date_register,
                '9' => $row->guardian,
                '10' => $row->phone_number_of_guardian,
                '11' => $row->tsphanve == 1 ? "c" : "k",
                '12' => $row->tssuygiammd == 1 ? "c" : "k",
                '13' => $row->benhcaptinh == 1 ? "c" : "k",
                '14' => $row->tsmantinh == 1 ? "c" : "k",
                '15' => $row->note,
            );
        }

        return (collect($vaccination));
    }
}
