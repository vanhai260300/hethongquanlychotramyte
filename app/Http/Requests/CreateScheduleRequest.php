<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'to_date' => 'required',
            'province_id' => 'required',
            'from_date' => 'required',
            'district_id' => 'required',
            'ward_id' => 'required',
            'address' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Yêu cầu nhập tên điểm đến ',
            'to_date.required' => 'Yêu cầu nhập thời gian đi',
            'from_date.required' => 'Yêu cầu nhập thời gian đến ',
            'province_id.required' => 'Yêu cầu chọn tình thành',
            'district_id.required' => 'Yêu cầu chọn Quận huyện',
            'ward_id.required' => 'Yêu cầu chọn phường xã',
            'address.required' => 'Yêu cầu nhập địa chỉ',
        ];
    }
}
