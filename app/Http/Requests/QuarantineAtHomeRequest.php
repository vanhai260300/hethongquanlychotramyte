<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuarantineAtHomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone_number' => 'required',
            'gender' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'ward_id' => 'required',
            'medical_station_id' => 'required|integer|between:0,9999999',
            'address' => 'required',
            'guardian' => 'required',
            'relationship' => 'required|integer|between:0,9999999',
            'phone_number_of_guardian' => 'required',
            'injection' => "required"
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Yêu cầu nhập tên',
            'phone_number.required' => 'Yêu cầu nhập số điện thoại',
            'province_id.required' => 'Yêu cầu chọn tỉnh thành',
            'district_id.required' => 'Yêu cầu chọn quận huyện',
            'ward_id.required' => 'Yêu cầu chọn xã',
            'medical_station_id.required' => 'Yêu cầu chọn trạm y tế',
            'medical_station_id.between' => 'Yêu cầu chọn trạm y tế',
            'address.required' => 'Yêu cầu nhập địa chỉ',
            'guardian.required'=>'Yêu cầu nhập tên người dám hộ.',
            'phone_number_of_guardian.required'=>'Yêu cầu nhập số điện thoại của người dám hộ.',
            'injection.required'=>'Nhập số mũi vaccin.',
        ];
    }
}
