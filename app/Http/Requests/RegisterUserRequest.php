<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
            'phone_number' => 'required',
            'gender' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'ward_id' => 'required',
            'medical_station_id' => 'required|integer|between:0,9999999',
            'address' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Yêu cầu nhập tên',
            'email.required' => 'Yêu cầu điền Email',
            'email.email' => 'Yêu cầu điền Email',
            'email.unique' => 'Email đã đăng ký tài khoản',
            'password.required' => 'Yêu cầu nhập Mật khẩu',
            'password.confirmed' => 'Mật khẩu không khớp',
            'phone_number.required' => 'Yêu cầu nhập Mật khẩu số điện thoại',
            'province_id.required' => 'Yêu cầu chọn tỉnh thành',
            'district_id.required' => 'Yêu cầu chọn quận huyện',
            'ward_id.required' => 'Yêu cầu chọn xã',
            'medical_station_id.required' => 'Yêu cầu chọn trạm y tế',
            'medical_station_id.between' => 'Yêu cầu chọn trạm y tế',
            'address.required' => 'Yêu cầu nhập địa chỉ',
        ];
    }
}
