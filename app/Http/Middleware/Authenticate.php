<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!Auth::check()) {
            session()->flash('notification', 'Vui Lòng đăng nhập');
            return route('user.auth.login');
        }
        elseif(Auth::user()->status != 1){
            session()->flash('notification', 'Tài khoản chưa được kích hoạt');
            return redirect()->route('user.auth.login');
        }
    }
}
