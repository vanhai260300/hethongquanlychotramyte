<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()){
            session()->flash('notification', 'Vui Lòng đăng nhập');
            return redirect()->route('user.auth.login');
        } elseif(Auth::user()->status == 1){
                return $next($request);
        } else {
            session()->flash('notification', 'Tài khoản chưa được kích hoạt');
            return redirect()->route('user.auth.login');
        }

    }
}
