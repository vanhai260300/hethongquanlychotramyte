<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MedicalstationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::guard("medical_station")->check())
        {
            return $next($request);
        } else{

        }
        session()->flash('notification', 'Vui Lòng đăng nhập');
        return redirect()->route('medical_station.auth.login');
    }
}
