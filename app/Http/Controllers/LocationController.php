<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\MedicalStaff;
use App\Models\MedicalStation;
use App\Models\Ward;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function district(Request $request)
    {
        $districts = District::where('_province_id', $request->id)->orderBy('_name')->get();
        return $districts;
    }
    public function ward(Request $request)
    {
        $wards = Ward::where('_district_id', $request->id)->orderBy('_name')->get();
        return $wards;
    }
    public function medicalStation(Request $request){
        $medicalstation = MedicalStation::where('ward_id', $request->id)->orderBy('name')->get();
        return $medicalstation;
    }
}
