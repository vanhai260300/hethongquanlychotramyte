<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('user.pages.auth.login');
    }
    public function login(LoginRequest $request){
        if (Auth::attempt($request->validated())) {
            if (Auth::user()->status == 1)
            {
                session()->flash('success', 'Đăng nhập thành công');
                return redirect()->intended('/');
            } else {
                session()->flash('notification', 'Tài khoản chưa được kích hoạt');
                return redirect()->route('user.auth.login');
            }
        }
        return back()->withErrors([
            'message' => 'Thông tin đăng nhập không đúng, vui lòng kiểm tra lại.'
        ])->withInput();
    }
    public function logout()
    {
        Auth::logout();
        session()->flush();
        return redirect()->route('user.auth.login');
    }
}
