<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Models\Province;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function index(){
        $provinces = Province::orderBy('_name')->get();
        $data['provinces'] = $provinces;
        return view('user.pages.auth.register',$data);
    }
    public function register(RegisterUserRequest $request)
    {
        $request->validated();
        if (User::createUser($request))
        {
//            session()->flash('verification', );
            session(['verification' => $request->email]);
            return redirect()->route('user.auth.verification');
        }
        return back()->withErrors(
            ['message' => 'Đăng ký thất bại kiểm tra lại thông tin.']
        )->withInput();
    }
    public function verificationByCode(){
        return view('user.pages.auth.verification');
    }
    public function verificationByCodePost(Request $request){
        $request->validate([
           'token' => 'required'
        ],
        [
            'token.required' => 'Nhập mã xác nhận'
        ]);
//        dd($request->all());
//        $result = User::where([['email', $request->email],['token', $request->token]])->update(['status' => 1]);
        if(User::where([['email', $request->email],['token', $request->token]])->update(['status' => 1]))
        {
            session()->flash('success', 'Xác thực tài khoản thành công đăng nhập để sử dụng.');
            session()->forget('verification');
            return redirect()->route('user.auth.login');
        } else {
            return back()->withErrors(['message' => 'Mã xác nhận không chính xác'])->withInput();
        }


    }
}
