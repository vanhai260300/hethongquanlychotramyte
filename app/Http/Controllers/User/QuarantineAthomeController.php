<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuarantineAtHomeRequest;
use App\Models\District;
use App\Models\MedicalStation;
use App\Models\ObjectQuarantine;
use App\Models\Province;
use App\Models\QuarantineAthome;
use App\Models\RegisterVaccination;
use App\Models\Relationship;
use App\Models\TypeVaccine;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuarantineAthomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $user = QuarantineAthome::where('user_id', Auth::id())->select('object_id')->get()->toArray();
        $quarantineAthomes = QuarantineAthome::with("object")
            ->with("object_quarantine")
//            ->whereHas("object_quarantine", function ($query){
//                $query->groupBy("date_moni")->distinct('date_moni');
//            })
            ->with("healtMonitorings")
            ->withCount(['healtMonitorings' => function ($query) {
                $query->select(DB::raw('count(distinct(date_moni))'));
            }])
            ->where('user_id', Auth::id())->get();
//        dd($quarantineAthomes);
        $object_of_users = User::where("user_id", Auth::id())->whereNotIn('id', $user)->get();
        $data["object_of_users"] = $object_of_users;
        $data["quarantineAthomes"] = $quarantineAthomes;
        return view("user.pages.quarantine_athome.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $relationship = Relationship::getAll();
        $object_quarantines = ObjectQuarantine::get();
        $data["relationship"] = $relationship;
        $data["object_quarantines"] = $object_quarantines;
        return view("user.pages.quarantine_athome.create", $data);
    }

    public function createOther()
    {
        $relationship = Relationship::getAll();
        $object_quarantines = ObjectQuarantine::get();
        $data["relationship"] = $relationship;
        $data["object_quarantines"] = $object_quarantines;
        $data['provinces'] = Province::getProvince();
        return view("user.pages.quarantine_athome.create-other", $data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect(Request $request){
        try {
            if($request->object_of_user == Auth::id()){
                return redirect()->route("user.quarantine-athome.create");
            } else {
                (QuarantineAthome::createWhenRedirect($request));
            return redirect()->route("user.quarantine-athome.edit-other", $request->object_of_user);
            }
        } catch (\Exception $e){
            return back()->withErrors([
                'message' => 'Vui lòng chọn đối tượng.'
            ])->withInput();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'guardian' => 'required',
            'relationship_of_guardian' => 'required|not_in:0',
            'phone_number_of_guardian' => 'required',
        ],
            [
                'guardian.required'=>'Yêu cầu nhập tên người dám hộ.',
                'relationship_of_guardian.required'=>'Yêu cầu chọn quan hệ.',
                'phone_number_of_guardian.required'=>'Yêu cầu nhập số điện thoại của người dám hộ.',

            ]);
        $result = QuarantineAthome::create($request);
        if($result != false){
            session()->flash('success', "Đăng ký thành công.");
            return redirect()->route("user.quarantine-athome.index");
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeOther(QuarantineAtHomeRequest $request)
    {
        $request->validated();
        $result = QuarantineAthome::createOther($request);
        if($result != false){
            session()->flash('success', "Đăng ký thành công.");
            return redirect()->route("user.quarantine-athome.index");
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $object = User::with("province")->with("district")->with("ward")->with("medicalStation")->find($id);
        $quarantine_athomes = QuarantineAthome::with("object_quarantine")
            ->with("healtMonitorings")
            ->with("relationshipgua")
            ->withCount(['healtMonitorings' => function ($query) {
                $query->select(DB::raw('count(distinct(date_moni))'));
            }])
            ->where("object_id", $id)->first();
//        dd($quarantine_athomes);
        $data["quarantine_athomes"] = $quarantine_athomes;
        $data['object'] = $object;
        return view("user.pages.health_monitoring.index", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = User::find($id);
        $quarantine_athomes = QuarantineAthome::where("object_id", $id)->first();
        $relationship = Relationship::getAll();
        $object_quarantines = ObjectQuarantine::get();
        $data['object'] = $object;
        $data["quarantine_athomes"] = $quarantine_athomes;
        $data["relationship"] = $relationship;
        $data["object_quarantines"] = $object_quarantines;
        return view("user.pages.quarantine_athome.edit", $data);
    }
    public function editOther($id)
    {
        $object = User::find($id);
        $quarantine_athomes = QuarantineAthome::where("object_id", $id)->first();
        $districts = District::where('_province_id', $object->province_id)->get();
        $wards = Ward::where('_district_id', $object->district_id)->get();
        $medical_stations = MedicalStation::where('ward_id', $object->ward_id)->get();
        $data["quarantine_athomes"] = $quarantine_athomes;
        $data['provinces'] = Province::getProvince();
        $data['districts'] = $districts;
        $data['wards'] = $wards;
        $data['medical_stations'] = $medical_stations;
        $data['object'] = $object;
        $relationship = Relationship::getAll();
        $object_quarantines = ObjectQuarantine::get();
        $data["relationship"] = $relationship;
        $data["object_quarantines"] = $object_quarantines;
        return view("user.pages.quarantine_athome.edit-other", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $result = QuarantineAthome::updates($request, $id);
        if($result != false){
            session()->flash('success', "Cập nhật thành công.");
            return redirect()->route("user.quarantine-athome.index");
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }

    public function updateOther(QuarantineAtHomeRequest $request, $id)
    {
//        dd($request->all());
        $result = QuarantineAthome::updatesOther($request, $id);
        if($result != false){
            session()->flash('success', "Cập nhật thành công.");
            return redirect()->route("user.quarantine-athome.index");
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return bool|\Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        return QuarantineAthome::deleteQuarantineAthome($request->id);
    }
}
