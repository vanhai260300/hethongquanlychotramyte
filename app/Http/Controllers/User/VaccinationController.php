<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserOtherRequest;
use App\Http\Requests\RegisterVaccineRequest;
use App\Models\District;
use App\Models\HealthDeclaration;
use App\Models\MedicalStation;
use App\Models\Province;
use App\Models\RegisterVaccination;
use App\Models\Relationship;
use App\Models\TypeVaccine;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VaccinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $user = RegisterVaccination::where('user_id', Auth::id())->select('object_id')->get()->toArray();
        $object_of_users = User::where("user_id", Auth::id())->whereNotIn('id', $user)->get();
        $registerVaccinations = RegisterVaccination::with('object')->where('user_id', Auth::id())->get();
        $data["object_of_users"] = $object_of_users;
        $data['registerVaccinations'] = $registerVaccinations;
        return view("user.pages.vaccination.index", $data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect(Request $request){
        if(isset($request->object_of_user)){
            if($request->object_of_user == Auth::id()){
                return redirect()->route("user.vaccination.create");
            } else {
//                dd($request->all());
                RegisterVaccination::createWhenRedirect($request);
                return redirect()->route("user.vaccination.edit-other", $request->object_of_user);
            }
        } else {
            return back()->withErrors([
                'message' => 'Vui lòng chọn đối tượng.'
            ])->withInput();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $relationship = Relationship::getAll();
        $vaccines = TypeVaccine::getAll();
        $data["vaccines"] = $vaccines;
        $data["relationship"] = $relationship;
        return view("user.pages.vaccination.create", $data);
    }
    public function createOther()
    {
        $relationship = Relationship::getAll();
        $vaccines = TypeVaccine::getAll();
        $data["vaccines"] = $vaccines;
        $data['provinces'] = Province::getProvince();
        $data["relationship"] = $relationship;
        return view("user.pages.vaccination.create-other", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'guardian' => 'required',
            'relationship_of_guardian' => 'required|not_in:0',
            'phone_number_of_guardian' => 'required',
            'captcha' => 'required|captcha'
        ],
        [
            'guardian.required'=>'Yêu cầu nhập tên người dám hộ.',
            'relationship_of_guardian.required'=>'Yêu cầu chọn quan hệ.',
            'phone_number_of_guardian.required'=>'Yêu cầu nhập số điện thoại của người dám hộ.',
            'captcha.captcha'=>'Mã bảo mật chưa chính xác.',
            'captcha.required'=>'Yêu cầu nhập mã bảo mật.',
        ]);
        $registervaccin = RegisterVaccination::create($request);
        if ($registervaccin != false){
            session()->flash('success', "Đăng ký thành công.");
            return redirect()->route("user.vaccination.show", $registervaccin);
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }

    public function createOtherPost(RegisterVaccineRequest $request)
    {
        $request->validated();
        $registervaccin = RegisterVaccination::createOther($request);
        if ($registervaccin != false){
            session()->flash('success', "Đăng ký thành công.");
            return redirect()->route("user.vaccination.show", $registervaccin);
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($object_id)
    {
        $registerVaccin = RegisterVaccination::with("relationship_guardian")->with("typeVaccin")->where("object_id", $object_id)->first();
        $object = User::with("province")->with("district")->with("ward")->with("medicalStation")->find($object_id);
        if($registerVaccin || $object){
            $data['registerVaccin'] = $registerVaccin;
            $data['object'] = $object;
            return view("user.pages.vaccination.show", $data);
        } else {
            return abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registerVaccine = RegisterVaccination::with("relationship_guardian")->where("object_id", $id)->first();
        if($registerVaccine){
            $data["object"] = User::find($id);
            $relationship = Relationship::getAll();
            $vaccines = TypeVaccine::getAll();
            $data["vaccines"] = $vaccines;
            $data["relationship"] = $relationship;
            $data["registerVaccine"] = $registerVaccine;
            return view("user.pages.vaccination.edit", $data);
        } else {
            return abort(404);
        }
    }

    public function editOther($id)
    {
        $object = User::find($id);
        $districts = District::where('_province_id', $object->province_id)->get();
        $wards = Ward::where('_district_id', $object->district_id)->get();
        $medical_stations = MedicalStation::where('ward_id', $object->ward_id)->get();
        $registerVaccine = RegisterVaccination::with("relationship_guardian")->where("object_id", $id)->first();
        $relationship = Relationship::getAll();
        $vaccines = TypeVaccine::getAll();
        $data['provinces'] = Province::getProvince();
        $data['districts'] = $districts;
        $data['wards'] = $wards;
        $data['medical_stations'] = $medical_stations;
        $data['object'] = $object;
        $data["object"] = User::find($id);
        $data["vaccines"] = $vaccines;
        $data["relationship"] = $relationship;
        $data["registerVaccine"] = $registerVaccine;
        return view("user.pages.vaccination.edit-other", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'guardian' => 'required',
            'relationship_of_guardian' => 'required|not_in:0',
            'phone_number_of_guardian' => 'required',
            'captcha' => 'required|captcha'
        ],
        [
            'guardian.required'=>'Yêu cầu nhập tên người dám hộ.',
            'relationship_of_guardian.required'=>'Yêu cầu chọn quan hệ.',
            'phone_number_of_guardian.required'=>'Yêu cầu nhập số điện thoại của người dám hộ.',
            'captcha.captcha'=>'Mã bảo mật chưa chính xác.',
            'captcha.required'=>'Yêu cầu nhập mã bảo mật.',
        ]);
        $registervaccin = RegisterVaccination::updates($request, $id);
        if ($registervaccin != false){
            session()->flash('success', "Cập nhật thành công.");
            return redirect()->route("user.vaccination.show", $registervaccin);
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }
    public function updateOtherPost(RegisterVaccineRequest $request, $id)
    {
//        dd($request->all());
        $request->validated();
        $registervaccin = RegisterVaccination::updateOther($request, $id);
        if ($registervaccin != false){
            session()->flash('success', "Cập nhật thành công.");
            return redirect()->route("user.vaccination.show", $registervaccin);
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin đăng ký.'
            ])->withInput();
        }
    }

    public function destroy(Request $request){

        return RegisterVaccination::deleteVaccine($request->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
