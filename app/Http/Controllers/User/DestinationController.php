<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateScheduleRequest;
use App\Models\District;
use App\Models\Province;
use App\Models\Schedule;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nette\Schema\Schema;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schedules = Schedule::with("province")->with("district")->with("ward")->orderBy("from_date", "DESC");
        $schedules->where("name","LIKE", "%".$request->name."%")->where("user_id", Auth::id());
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        if ($from_date != null){
            $schedules->whereDate("from_date",">=", $from_date);
        }
        if ($to_date != null){
            $schedules->whereDate("to_date","<=", $to_date);
        }
        $name = $request->name;
        $data["name"] = $name;
        $data["from_date"] = $from_date;
        $data["to_date"] = $to_date;
        $data['schedules'] =$schedules->paginate(5);
        return view("user.pages.destination.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $data['provinces'] = Province::getProvince();
        return view("user.pages.destination.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(CreateScheduleRequest $request)
    {
        $request->validated();
        if($request->province_id == 0 || $request->district_id == 0 || $request->ward_id == 0)
        {
            session()->flash("error", "Yêu cầu nhập địa chỉ");
        } else {
            $result = Schedule::create($request);
            if ($result == true){
                session()->flash("success", "Thêm địa chỉ thành công");
                return redirect()->route('user.destination.index');
            } else {
                session()->flash("error", "Thêm địa chỉ thất bại");
                return back();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = Schedule::find($id);
        $data['schedule'] = $schedule;
        $districts = District::where('_province_id', $schedule->province_id)->get();
        $wards = Ward::where('_district_id', $schedule->district_id)->get();
        $data['provinces'] = Province::getProvince();
        $data['districts'] = $districts;
        $data['wards'] = $wards;
        return view("user.pages.destination.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateScheduleRequest $request, $id)
    {
        $request->validated();
        $result = Schedule::updates($id, $request);
        if ($result == true){
            session()->flash("success", "Cập nhật địa chỉ thành công");
            return redirect()->route('user.destination.index');
        } else {
            session()->flash("error", "Cập nhật địa chỉ thất bại");
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
