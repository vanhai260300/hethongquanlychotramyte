<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserOtherRequest;
use App\Models\District;
use App\Models\HealthDeclaration;
use App\Models\MedicalStation;
use App\Models\Province;
use App\Models\Relationship;
use App\Models\User;
use App\Models\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeclarationController extends Controller
{
    //
    public function index(){
        $user = HealthDeclaration::where('user_id', Auth::id())->select('object_id')->get()->toArray();
        $object_of_users = User::where("user_id", Auth::id())->whereNotIn('id', $user)->get();
//        dd($object_of_users);
        $data["object_of_users"] = $object_of_users;
        $healthDeclarations = HealthDeclaration::with('object')->where('user_id', Auth::id())->get();
        $data['healthDeclarations'] = $healthDeclarations;
        return view('user.pages.declaration.declaration', $data);
    }

    public function declarationMyself(){
        $data['provinces'] = Province::getProvince();
        return view('user.pages.declaration.declaration_myself', $data);
    }

    public function declarationMyselfPost(Request $request){
        $request->validate([
                'captcha' => 'required|captcha'
            ],
            [   'captcha.captcha'=>'Mã bảo mật chưa chính xác.',
                'captcha.required'=>'Yêu cầu nhập mã bảo mật.',
                ]);
        $idDeclaration = HealthDeclaration::declarationMyself($request);
        if($idDeclaration){
            session()->flash('success', "Tạo khai báo thành công.");
        }
        return redirect()->route('user.declaration.show-detail', $idDeclaration);
    }

    public function declarationMyselfEdit($id){
        $declaration = HealthDeclaration::where('object_id', $id)->first();
        $data['declaration'] = $declaration;
        return view('user.pages.declaration.declaration_myself_edit', $data);
    }

    public function declarationMyselfPostEdit($id, Request $request){
        $request->validate([
                'captcha' => 'required|captcha'
            ],
            [   'captcha.captcha'=>'Mã bảo mật chưa chính xác.',
                'captcha.required'=>'Yêu cầu nhập mã bảo mật.',
            ]);
        $idDeclaration = HealthDeclaration::declarationMyselfEdit($id, $request);
        if($idDeclaration){
            session()->flash('success', "Cập nhật khai báo thành công.");
        }
        return redirect()->route('user.declaration.show-detail', $idDeclaration);
    }

    public function declarationOtherPeople(){
        $relationship = Relationship::get();
        $data['relationship'] = $relationship;
        $data['provinces'] = Province::getProvince();
        return view('user.pages.declaration.declaration_other_people', $data);
    }

    public function declarationOtherPeoplePost(CreateUserOtherRequest $request){
        $request->validated();
        $contentEmail = 'Bạn vừa dùng mail này để khai báo y tế';
        $idDeclaration = HealthDeclaration::declarationOtherPeople($request, $contentEmail);
        if ($idDeclaration != false){
            session()->flash('success', "Tạo khai báo thành công.");
            return redirect()->route('user.declaration.show-detail', $idDeclaration);
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin khai báo.'
            ])->withInput();
        }

    }

    public function declarationOtherPeopleEdit($id){
        $object = User::find($id);
        $districts = District::where('_province_id', $object->province_id)->get();
        $wards = Ward::where('_district_id', $object->district_id)->get();
        $medical_stations = MedicalStation::where('ward_id', $object->ward_id)->get();
        $declaration = HealthDeclaration::where('object_id', $id)->first();
        $relationship = Relationship::get();
        $data['relationship'] = $relationship;
        $data['provinces'] = Province::getProvince();
        $data['districts'] = $districts;
        $data['wards'] = $wards;
        $data['medical_stations'] = $medical_stations;
        $data['declaration'] = $declaration;
        $data['object'] = $object;
        return view('user.pages.declaration.declaration_other_people_edit', $data);
    }

    public function declarationOtherPeoplePostEdit($id, CreateUserOtherRequest $request){
        $request->validated();
        $idDeclaration = HealthDeclaration::declarationOtherPeopleEdit($id, $request);
        if($idDeclaration != false){
            session()->flash('success', "Cập nhật khai báo thành công.");
            return redirect()->route('user.declaration.show-detail', $idDeclaration);
        } else {
            return back()->withErrors([
                'message' => 'Kiểm tra lại thông tin khai báo.'
            ])->withInput();
        }

    }

    public function declarationShowDetail($idDeclaration){
        $declaration = HealthDeclaration::with("object")->where("object_id", $idDeclaration)->first();
        $object = User::with("province")->with("district")->with("ward")->with("medicalStation")->find($idDeclaration);
        $data['declaration'] = $declaration;
//        dd($declaration);
        $data['object'] = $object;
        return view('user.pages.declaration.declaration_show_detail', $data);
    }

    public function redirect(Request $request){
        if(isset($request->object_of_user)){
            if($request->object_of_user == Auth::id()){
                return redirect()->route("user.declaration.myself");
            } else {
                HealthDeclaration::createWhenRedirect($request);
                return redirect()->route("user.declaration.otherpeople.edit", $request->object_of_user);
            }
        } else {
            return back()->withErrors([
                'message' => 'Vui lòng chọn đối tượng.'
            ])->withInput();
        }

    }

    public function destroy(Request $request){

        return HealthDeclaration::deleteDeclaration($request->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
