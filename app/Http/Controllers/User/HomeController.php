<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Notification;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $news = News::with("article")->orderBy('created_at', "DESC")->paginate(6);
        $notifications = Notification::getNotification();
        $data['news'] = $news;
        $data['notifications'] = $notifications;
        return view('user.pages.home', $data);
    }

    public function newsDetail($id){
        $news = News::where("id", $id)->with("article")->first();
        $newses = News::with("article")->orderBy('created_at', "DESC")->paginate(6);
        $notifications = Notification::getNotification();
        $data['notifications'] = $notifications;
        if($news){
            News::updateViews($id, $news->view);
            $data['news'] = $news;
            $data['newses'] = $newses;
            return view('user.pages.news_show', $data);
        }
        else
        {
            return back()->withErrors([
                'message' => 'Tin không tồn tại.'
            ])->withInput();
        }
    }
}
