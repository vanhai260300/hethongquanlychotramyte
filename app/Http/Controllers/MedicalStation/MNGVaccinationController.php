<?php

namespace App\Http\Controllers\MedicalStation;

use App\Exports\DeclarationExport;
use App\Exports\VaccinationExport;
use App\Http\Controllers\Controller;
use App\Models\HealthDeclaration;
use App\Models\RegisterVaccination;
use App\Models\User;
use Carbon\Carbon;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MNGVaccinationController extends Controller
{
    public static function index(Request $request){

        $vaccinations = RegisterVaccination::getByMedicalId($request);
        $data['vaccinations'] = $vaccinations;
        $data['name'] = $request->name;
        $data["injection"] = $request->injection;
        $data['phone_number'] = $request->phone_number;
        return view('medicalstation.pages.manage_register_vaccination.index', $data);
    }

    public function vaccinationDetail($object_id){
        $registerVaccin = RegisterVaccination::with("relationship_guardian")->with("typeVaccin")->where("object_id", $object_id)->first();
        $object = User::with("province")->with("district")->with("ward")->with("medicalStation")->find($object_id);
        if($registerVaccin || $object){
            $data['registerVaccin'] = $registerVaccin;
            $data['object'] = $object;
            return view("medicalstation.pages.manage_register_vaccination.detail", $data);
        } else {
            return abort(404);
        }

    }

    public static function export(Request $request){
        $from_age = "";
        $to_age = "";
        $injection = "";
        if($request->from_age){
            $from_age = trim($request->from_age);
        }
        if($request->to_age){
            $to_age = trim($request->to_age);
        }
        if($request->injection){
            $injection = trim($request->injection);
        }
        if ($from_age <= $to_age){
            $vaccinnations = RegisterVaccination::dataExport(Auth::guard("medical_station")->id(), $request);
            return Excel::download(new VaccinationExport($vaccinnations), 'list-register-vaccin-'.$from_age.'-'.$to_age.'-'.$injection.'.csv');
        } else {
            return back()->withErrors([
                'message' => 'Chọn độ tuổi chưa phù hợp.'
            ])->withInput();
        }

    }
}
