<?php

namespace App\Http\Controllers\MedicalStation;

use App\Exports\DeclarationExport;
use App\Http\Controllers\Controller;
use App\Models\HealthDeclaration;
use App\Models\MedicalStaff;
use App\Models\ObjectQuarantine;
use App\Models\QuarantineAthome;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class MNGQuarantineAthomeController extends Controller
{
    public static function index(Request $request){
        $quanrantine_athomes = QuarantineAthome::getByMedicalId( $request);
        $object_quarantines = ObjectQuarantine::all();
        $mediccal_staffs = MedicalStaff::where("medical_station_id", Auth::guard("medical_station")->id())->get();
        $data['quanrantine_athomes'] = $quanrantine_athomes;
        $data['object_quarantines'] = $object_quarantines;
        $data['mediccal_staffs'] = $mediccal_staffs;
        $data['name'] = $request->name;
        $data['phone_number'] = $request->phone_number;
        $data['medical_staff_id'] = $request->medical_staff;
        $data['object_quarantine_id'] = $request->object_quarantine;
        return view('medicalstation.pages.manage_quarantine_athome.index', $data);
    }

    public function quaratineDetail($idQuarantine){
        $quanratine = QuarantineAthome::getById($idQuarantine);
//        dd($quanratine);
        if($quanratine){
            $data['quanratine_athome'] = $quanratine;
            return view('medicalstation.pages.manage_health_monitor.index', $data);
        } else {
            return abort(404);
        }
    }

    public function updateMedicalStaff(Request $request){
        $quarantine = QuarantineAthome::updateMedicalStaff($request);
        return $quarantine;
    }
    public function updateStatusQuarantine(Request $request){
        $quarantine = QuarantineAthome::updateStatusQuarantine($request);
        return $quarantine;
    }
    public function confirmmm(){
        $quarantine = QuarantineAthome::confirmmm();
        return redirect()->route("medical_station.quanrantine_athome.index");
    }

//    public static function export(){
//        $declarations = HealthDeclaration::dataExport(Auth::guard("medical_station")->id());
//        return Excel::download(new DeclarationExport($declarations), 'listdeclaration.csv');
//    }
}
