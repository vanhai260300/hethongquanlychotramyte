<?php

namespace App\Http\Controllers\MedicalStation\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('medicalstation.auth.login');
    }
    public function login(LoginRequest $request){
        if (Auth::guard('medical_station')->attempt($request->validated())) {
            session()->flash('success', 'Đăng nhập thành công');
            return redirect()->intended('medical-station');
        }
        return back()->withErrors([
            'message' => 'Thông tin đăng nhập không đúng, vui lòng kiểm tra lại.'
        ])->withInput();
    }
    public function logout()
    {
        Auth::guard('medical_station')->logout();
        session()->flush();
        return redirect()->route('medical_station.auth.login');
    }
}
