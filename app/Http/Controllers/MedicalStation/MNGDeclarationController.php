<?php

namespace App\Http\Controllers\MedicalStation;

use App\Exports\DeclarationExport;
use App\Http\Controllers\Controller;
use App\Models\HealthDeclaration;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class MNGDeclarationController extends Controller
{
    public static function index(Request $request){

        $declarations = HealthDeclaration::getByMedicalId($request);
        foreach ($declarations as $key => $declaration){
            $declaration->declaration_date = HealthDeclaration::caculateDate( $declaration->declaration_date);
        }
        $data['declarations'] = $declarations;
        $data['name'] = $request->name;
        $data['phone_number'] = $request->phone_number;
        $data['rate_lavel'] = $request->rate_lavel;
//        dd($request->all());
        return view('medicalstation.pages.manage_declaration.index', $data);
    }

    public function declarationDetail($idDeclaration){
        $declaration = HealthDeclaration::with("object")->where("object_id", $idDeclaration)->first();
        $object = User::with("province")->with("district")->with("ward")->with("medicalStation")->find($idDeclaration);
        if($declaration || $object){
            $data['declaration'] = $declaration;
            $data['object'] = $object;
            return view('medicalstation.pages.manage_declaration.detail', $data);
        } else {
            return abort(404);
        }

    }

    public static function export(){
        $declarations = HealthDeclaration::dataExport(Auth::guard("medical_station")->id());
        // dd($declarations);
        return Excel::download(new DeclarationExport($declarations), 'listdeclaration.csv');
    }


}
