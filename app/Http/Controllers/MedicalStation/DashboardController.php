<?php

namespace App\Http\Controllers\MedicalStation;

use App\Http\Controllers\Controller;
use App\Models\Province;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $provinces = Province::orderBy('_name')->get();
        $data['provinces'] = $provinces;
        return view('medicalstation.pages.dashboard', $data);
    }
}
