<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RegisterVaccination extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function object(){
        return $this->belongsTo(User::class, 'object_id', 'id');
    }

    public function relationship(){
        return $this->belongsTo(Relationship::class, 'relationship', 'id');
    }

    public function relationship_guardian(){
        return $this->belongsTo(Relationship::class, 'relationship_of_guardian', 'id');
    }

    public function typeVaccin(){
        return $this->belongsTo(TypeVaccine::class, 'type_vaccine', 'id');
    }

    public static function getAll(){
        return RegisterVaccination::get();
    }

    public static function getPersonal(){
        return RegisterVaccination::Where("user_id", Auth::id())->get();
    }

    public static function create($request){
        try{
        $registerVaccination = new RegisterVaccination();
        $registerVaccination->user_id = Auth::id();
        $registerVaccination->object_id = Auth::id();
        $registerVaccination->relationship = 0;
        RegisterVaccination::setData($registerVaccination, $request);
        $registerVaccination->save();
        return $registerVaccination->object_id;
        } catch(\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function getByMedicalId($request){
        $vaccinations = User::join("register_vaccinations", "register_vaccinations.object_id", "=", "users.id")
            ->orderBy("register_vaccinations.date_register", "DESC");
        if (Auth::guard("medical_station")->user()->role == 0)
            $vaccinations->where("medical_station_id", Auth::guard("medical_station")->id());
        $name = "";
        $phone_number = "";
        $injection = "";
        if($request->name){
            $name = trim($request->name);
        }
        if($request->phone_number){
            $phone_number = trim($request->phone_number);
        }
        if($request->injection){
            $injection = trim($request->injection);
        }

        if ($name != null){
            $vaccinations->where("users.name", "LIKE", "%".$name."%");
        }
        if ($phone_number != null){
            $vaccinations->where("users.phone_number", "LIKE", "%".$phone_number."%");
        }
        if ($injection != null){
            $vaccinations->where("register_vaccinations.injection", $injection);
        }

        return $vaccinations->get();
    }

    public static function createOther($request){
        try{
            DB::beginTransaction();
            $contenEmail = "Email vừa đăng ký vaccin";
            $idObject = User::createUserWhenOther($request, $contenEmail);
            $registerVaccination = new RegisterVaccination();
            $registerVaccination->user_id = Auth::id();
            $registerVaccination->object_id = $idObject;
            $registerVaccination->relationship = $request->relationship;
            RegisterVaccination::setData($registerVaccination, $request);
            $registerVaccination->save();
            DB::commit();
            return $registerVaccination->object_id;
        } catch(\Exception $e){
            DB::rollBack();
            return false;
        }

    }

    public static function updates($request, $id){

        try{
            $registerVaccination = RegisterVaccination::where("object_id", $id)->first();
            RegisterVaccination::setData($registerVaccination, $request);
            $registerVaccination->save();
            return $registerVaccination->object_id;
        } catch(\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function updateOther($request, $id){
        try{
            DB::beginTransaction();
            $idObject = User::updateUserWhenOther($id, $request);
            $registerVaccination = RegisterVaccination::where("object_id", $id)->first();
            $registerVaccination->relationship = $request->relationship;
            RegisterVaccination::setData($registerVaccination, $request);
            $registerVaccination->save();
            DB::commit();
            return $registerVaccination->object_id;
        } catch(\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function deleteVaccine($id){
        try {
            DB::beginTransaction();
            $registerVaccination = RegisterVaccination::where("object_id", $id)->first();
            $registerVaccination->delete();
            DB::commit();
            return true;
        }catch(\Exception $e){
            DB::rollBack();
            return false;
        }

    }

    public static function createWhenRedirect($request){
        $registerVaccination = new RegisterVaccination();
        $registerVaccination->user_id = Auth::id();
        $registerVaccination->object_id = $request->object_of_user;
        $registerVaccination->relationship_of_guardian = 7;
        $registerVaccination->guardian = "";
        $registerVaccination->injection = 1;
        $registerVaccination->relationship = 0;
        $registerVaccination->date_register = now();
        $registerVaccination->tsphanve =0;
        $registerVaccination->tscovid = 0;
        $registerVaccination->tssuygiammd = 0;
        $registerVaccination->benhcaptinh = 0;
        $registerVaccination->tsmantinh = 0;
        $registerVaccination->mangthai = 0;
        $registerVaccination->age = 0;
        $registerVaccination->save();
        return $registerVaccination->object_id;
    }

    public static function dataExport($medical_station_id, $request){
        $vaccinations = User::where("medical_station_id", $medical_station_id)
            ->join("register_vaccinations", "register_vaccinations.object_id", "=", "users.id")
            ->orderBy("register_vaccinations.date_register", "DESC");
        if ($medical_station_id == 25)
        {
            $vaccinations = User::join("register_vaccinations", "register_vaccinations.object_id", "=", "users.id")
            ->orderBy("register_vaccinations.date_register", "DESC");
        }
        $from_age = 0;
        $to_age = 9999;
        $injection = "";
        if($request->from_age){
            $from_age = trim($request->from_age);
        }
        if($request->to_age){
            $to_age = trim($request->to_age);
        }
        if($request->injection){
            $injection = trim($request->injection);
        }
        if ($injection != null){
            $vaccinations->where("register_vaccinations.injection", $injection);
        }
        $vaccinations = $vaccinations->get();
        $vaccinations = $vaccinations->filter(function ($vaccination, $key) use ($to_age, $from_age) {
            $age = RegisterVaccination::caculateAge($vaccination->birthdate);
            return $age >= $from_age && $age <= $to_age;
        });

//
//        foreach ($vaccinations as $key => $vaccination){
//            $vaccination->age = RegisterVaccination::caculateAge( $vaccination->birthdate);
//            if($vaccination->age < $from_age || $vaccination->age > $to_age)
//                unset($vaccinations[$key]);
//        }
        return $vaccinations;
    }

    public static function caculateAge($bob){
        $years = Carbon::parse($bob)->age;
        return $years;
    }

    public static function setData($registerVaccination, $request){
        $registerVaccination->guardian = $request->guardian;
        $registerVaccination->relationship_of_guardian = $request->relationship_of_guardian;
        $registerVaccination->phone_number_of_guardian = $request->phone_number_of_guardian;
        $registerVaccination->injection = $request->injection;
        $registerVaccination->date_register = now();
        $registerVaccination->type_vaccine = $request->vaccin_id;
        $registerVaccination->last_injection = $request->last_injection;
        $registerVaccination->tsphanve = $request->tsphanve;
        $registerVaccination->tscovid = $request->tscovid;
        $registerVaccination->tssuygiammd = $request->tssuygiammd;
        $registerVaccination->benhcaptinh = $request->benhcaptinh;
        $registerVaccination->tsmantinh = $request->tsmantinh;
        $registerVaccination->mangthai = $request->mangthai;
        $registerVaccination->age = $request->age;
        $registerVaccination->note = $request->note;
        $registerVaccination->status = 0;
    }
}
