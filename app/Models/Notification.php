<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notification extends Model
{
    use HasFactory;

    public function article(){
        return $this->belongsTo(MedicalStation::class, "articler_id", "id");
    }

    public static function getNotification()
    {
        $notifications = Notification::orderBy('created_at', "DESC");
        if (Auth::check()){
            $notifications->where("articler_id", Auth::user()->medical_station_id);
        }
        if (Auth::guard("medical_station")->check()){
            if (Auth::guard("medical_station")->user()->role == 0)
                $notifications->where("articler_id", Auth::guard("medical_station")->id());
        }
        return $notifications->paginate(6);
    }

    public static function getNotificationByMedical()
    {
        $notifications = Notification::orderBy('created_at', "DESC");
        if (Auth::guard("medical_station")->check()){
            if (Auth::guard("medical_station")->user()->role == 0)
                $notifications->where("articler_id", Auth::guard("medical_station")->id());
        }
        return $notifications->get();
    }

    public static function create_notification($request){
        $notification = New Notification();
        $notification->articler_id = Auth::guard("medical_station")->id();
        $notification->title = $request->title;
        $notification->content = $request->content;
        return $notification->save();
    }

}
