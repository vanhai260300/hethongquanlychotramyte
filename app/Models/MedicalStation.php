<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class MedicalStation extends Authenticatable
{
    use HasFactory, Notifiable;

    public function province(){
        return $this->belongsTo(Province::class, "province_id");
    }

    public function district(){
        return $this->belongsTo(District::class, "district_id");
    }

    public function ward(){
        return $this->belongsTo(Ward::class, "ward_id");
    }

    public function users(){
        return $this->hasMany(User::class, "medical_station_id");
    }

    public static function getMedicalStation(){
        $medicalStations = MedicalStation::with('ward')->with('province')->with('district')->with('users')->get();
        return $medicalStations; 
    }

    protected $table = 'medical_stations';

    protected $guarded = 'medical_station';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
