<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MedicalStaff extends Model
{
    use HasFactory;

    public function object_quarantines(){
        return $this->hasMany(QuarantineAthome::class, "medical_staff_id");
    }

    public static function getMedicalStaff(){
        $medical_staffs = MedicalStaff::with("object_quarantines");
        if (Auth::guard("medical_station")->user()->role == 0)
            $medical_staffs->where("medical_station_id", Auth::guard("medical_station")->id());
        return $medical_staffs->get();
    }
}
