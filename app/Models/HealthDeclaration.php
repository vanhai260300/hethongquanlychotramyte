<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HealthDeclaration extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function object(){
        return $this->belongsTo(User::class, 'object_id', 'id');
    }

    public static function getByMedicalId($request){
        $declarations = User::join("health_declarations", "health_declarations.object_id", "=", "users.id")
            ->orderBy("health_declarations.declaration_date", "DESC");
        $name = "";
        $phone_number = "";
        $rate_lavel = "";
        if($request->name){
            $name = trim($request->name);
        }
        if($request->phone_number){
            $phone_number = trim($request->phone_number);
        }
        if($request->rate_lavel){
            $rate_lavel = trim($request->rate_lavel);
        }

        if ($name != null){
            $declarations->where("users.name", "LIKE", "%".$name."%");
        }
        if ($phone_number != null){
            $declarations->where("users.phone_number", "LIKE", "%".$phone_number."%");
        }
        if ($rate_lavel != 0){
            $declarations->where("health_declarations.rate_lavel", $rate_lavel);
        }
        if (Auth::guard("medical_station")->user()->role == 0)
            $declarations -> where("medical_station_id", Auth::guard("medical_station")->id());
        return $declarations->get();
    }

    public static function dataExport($medical_station_id)
    {
        $declarations = User::where("medical_station_id", $medical_station_id)
            ->join("health_declarations", "health_declarations.object_id", "=", "users.id")
            ->orderBy("health_declarations.declaration_date", "DESC")
            ->get();
        if ($medical_station_id == 25)
        {
            $declarations = User::join("health_declarations", "health_declarations.object_id", "=", "users.id")
            ->orderBy("health_declarations.declaration_date", "DESC")
            ->get();
        }
        return $declarations;
    }

    public static function declarationMyself($request){
        $declaration = new HealthDeclaration();
        $declaration->user_id = Auth::id();
        $declaration->object_id = Auth::id();
        $declaration->relationship = 0;
        HealthDeclaration::declaration($declaration, $request);
        $declaration->save();
        return $declaration->object_id;
    }

    public static function declarationMyselfEdit($id, $request){
        $declaration = HealthDeclaration::where('object_id', $id)->first();
        HealthDeclaration::declaration($declaration, $request);
        $declaration->save();
        return $declaration->object_id;
    }

    public static function declarationOtherPeople($request, $contentEmail){
        try{
            DB::beginTransaction();
            $idObject = User::createUserWhenOther($request, $contentEmail);
            $declaration = new HealthDeclaration();
            $declaration->user_id = Auth::id();
            $declaration->object_id = $idObject;
            $declaration->relationship = $request->relationship;
            HealthDeclaration::declaration($declaration, $request);
            $declaration->save();
            DB::commit();
            return $declaration->object_id;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }

    }

    public static function declarationOtherPeopleEdit($id, $request){
        try{
            DB::beginTransaction();
            $idObject = User::updateUserWhenOther($id, $request);
            $declaration = HealthDeclaration::where('object_id', $id)->first();
            $declaration->relationship = $request->relationship;
            HealthDeclaration::declaration($declaration, $request);
            $declaration->save();
            DB::commit();
            return $declaration->object_id;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function deleteDeclaration($id){
        try {
            DB::beginTransaction();
            $healthDeclaration = HealthDeclaration::where("object_id", $id)->first();
            $healthDeclaration->delete();
            DB::commit();
            return true;
        }catch(\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function createWhenRedirect($request){
        $healthDeclaration = new HealthDeclaration();
        $healthDeclaration->user_id = Auth::id();
        $healthDeclaration->object_id = $request->object_of_user;
        $healthDeclaration->relationship = 7;
        $healthDeclaration->declaration_date = now();
        $healthDeclaration->fever = 0;
        $healthDeclaration->cough = 0;
        $healthDeclaration->sore_throat = 0;
        $healthDeclaration->difficulty_of_breathing = 0;
        $healthDeclaration->diarrhea = 0;
        $healthDeclaration->tired = 0;
        $healthDeclaration->loss_taste = 0;
        $healthDeclaration->got_covid = 0;
        $healthDeclaration->ever_had_covid = 0;
        $healthDeclaration->exposure_covid = 0;
        $healthDeclaration->quarantined = 0;
        $healthDeclaration->save();
        try {

            return $healthDeclaration->object_id;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function caculateDate($start)
    {

        $startTime = Carbon::parse($start);
        $endTime = Carbon::parse(now());

        $totalDuration = $startTime->diffInDays();
        return $totalDuration;
    }

    public static function declaration($declaration, $request){
        $declaration->declaration_date = now();
        $declaration->fever = $request->fever;
        $declaration->cough = $request->cough;
        $declaration->sore_throat = $request->sore_throat;
        $declaration->difficulty_of_breathing = $request->difficulty_of_breathing;
        $declaration->diarrhea = $request->diarrhea;
        $declaration->tired = $request->tired;
        $declaration->loss_taste = $request->loss_taste;
        $declaration->got_covid = $request->got_covid;
        $declaration->ever_had_covid = $request->ever_had_covid;
        $declaration->exposure_covid = $request->exposure_covid;
        $declaration->quarantined = $request->quarantined;
        $declaration->note = $request->note;
        $declaration->rate_lavel = HealthDeclaration::checkgroup($request->fever,$request->cough,$request->sore_throat,$request->difficulty_of_breathing,$request->diarrhea,$request->tired,$request->loss_taste);
    }

    public static function checkgroup($fever,$cough,$sore_throat,$difficulty_of_breathing,$diarrhea,$tired,$loss_taste){
        $countitem = 0;
        if($fever == 1){
            $countitem++;
        }
        if($cough == 1){
            $countitem++;
        }
        if($sore_throat == 1){
            $countitem++;
        }
        if($difficulty_of_breathing == 1){
            $countitem++;
        }
        if($diarrhea == 1){
            $countitem++;
        }
        if($tired == 1){
            $countitem++;
        }
        if($loss_taste == 1){
            $countitem++;
        }

        if($countitem >= 5) {
            return 3;
        } elseif ($countitem >= 3) {
            return 2;
        } elseif ($countitem >= 1) {
            return 1;
        } else {
            return 0;
        }
    }
}
