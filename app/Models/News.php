<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class News extends Model
{
    use HasFactory;

    public function article(){
        return $this->belongsTo(MedicalStation::class, "articler_id", "id");
    }

    public static function getNews(){
        $news = News::orderBy("updated_at", "DESC");
        if (Auth::guard("medical_station")->user()->role == 0)
            $news->where("articler_id", Auth::guard("medical_station")->id());
        return $news->get();
    }

    public static function create_news($request){
        $news = New News();
        $news->articler_id = Auth::guard("medical_station")->id();
        $news->title = $request->title;
        $news->content = $request->content;
        return $news->save();
    }

    public static function updateViews($id, $view)
    {
        News::where('id', $id)->update(array('view' => ($view + 1)));
    }
}
