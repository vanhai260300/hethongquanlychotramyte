<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuarantineAthome extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function object(){
        return $this->belongsTo(User::class, 'object_id', 'id')
            ->with("province")->with("district")->with("ward");
    }

    public function object_quarantine(){
        return $this->belongsTo(ObjectQuarantine::class, 'object_quarantine_id', 'id');
    }

    public function relationship(){
        return $this->belongsTo(Relationship::class, 'relationship', 'id');
    }

    public function medicalStaff(){
        return $this->belongsTo(MedicalStaff::class, 'medical_staff_id', 'id');
    }

    public function relationshipgua(){
        return $this->belongsTo(Relationship::class, 'relationship_of_guardian', 'id');
    }

    public function healtMonitorings(){
        return $this->hasMany(HealthMonitoring::class, 'quarantine_id', 'id')->orderBy("date_moni", "DESC");
    }

    public static function getById($id){
        $quanrantine_athome = QuarantineAthome::with("object_quarantine")
            ->with("healtMonitorings")
            ->withCount(['healtMonitorings' => function ($query) {
                $query->select(DB::raw('count(distinct(date_moni))'));
            }])
            ->with("medicalStaff")
            ->with("object")
            ->find($id);
        return $quanrantine_athome;
    }

    public static function getByMedicalId($request){
        $keyword = Auth::guard("medical_station")->id();
        $quanrantine_athomes = QuarantineAthome::with("object_quarantine")
            ->with("healtMonitorings")
            ->orderBy("medical_staff_id")
            ->withCount(['healtMonitorings' => function ($query) {
                $query->select(DB::raw('count(distinct(date_moni))'));
            }])
            ->with("medicalStaff")
            ->with("object");
        if (Auth::guard("medical_station")->user()->role == 0)
            $quanrantine_athomes->whereHas('object', function ($query) use ($keyword) {
                $query->where('medical_station_id', $keyword);
            });
        $name = "";
        $phone_number = "";
        $medical_staff = 0;
        $object_quarantine = 0;
        if($request->name){
            $name = trim($request->name);
        }
        if($request->phone_number){
            $phone_number = trim($request->phone_number);
        }
        if($request->medical_staff){
            $medical_staff = trim($request->medical_staff);
        }
        if($request->object_quarantine){
            $object_quarantine = trim($request->object_quarantine);
        }

        if ($name != null){
            $quanrantine_athomes->whereHas('object', function ($query) use ($name) {
                $query->where('name', "LIKE", "%".$name."%");
            });
        }
        if ($phone_number != null){
            $quanrantine_athomes->whereHas('object', function ($query) use ($phone_number) {
                $query->where('phone_number', "LIKE", "%".$phone_number."%");
            });
        }
        if ($object_quarantine != 0){
            $quanrantine_athomes->where("object_quarantine_id", $object_quarantine);
        }
        if ($medical_staff != 0 && $medical_staff != 9999999){
            $quanrantine_athomes->where("medical_staff_id", $medical_staff);
        } else if ($medical_staff == 9999999) {
            $quanrantine_athomes->WhereNull("medical_staff_id");
        }

        return $quanrantine_athomes->get();
    }

    public static function create($request){
        try {
            $quarantineAthome = new QuarantineAthome();
            $quarantineAthome->user_id = Auth::id();
            $quarantineAthome->object_id = Auth::id();
            QuarantineAthome::setData($quarantineAthome, $request);
            $quarantineAthome->save();
            return $quarantineAthome->id;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function createWhenRedirect($request){
        try {
            $quarantineAthome = new QuarantineAthome();
            $quarantineAthome->user_id = Auth::id();
            $quarantineAthome->object_id = $request->object_of_user;
            $quarantineAthome->object_quarantine_id = 0;
            $quarantineAthome->quarantine_date = now();
            $quarantineAthome->save();
            return $quarantineAthome->id;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function createOther($request){
        try {
            $contenEmail = "Email vừa đăng ký đăng ký tại nhà";
            $idObject = User::createUserWhenOther($request, $contenEmail);
            $quarantineAthome = new QuarantineAthome();
            $quarantineAthome->user_id = Auth::id();
            $quarantineAthome->object_id = $idObject;
            QuarantineAthome::setData($quarantineAthome, $request);
            $quarantineAthome->relationship = $request->relationship;
            $quarantineAthome->save();
            return $quarantineAthome->id;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function updates($request, $id){
        try {
            $quarantineAthome = QuarantineAthome::where("object_id", $id)->first();
            QuarantineAthome::setData($quarantineAthome, $request);
            $quarantineAthome->save();
            return $quarantineAthome->id;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function updatesOther($request, $id){
        try {
            $idObject = User::updateUserWhenOther($id, $request);
            $quarantineAthome = QuarantineAthome::where("object_id", $id)->first();
            $quarantineAthome->relationship = $request->relationship;
            QuarantineAthome::setData($quarantineAthome, $request);
            $quarantineAthome->save();
            return $quarantineAthome->id;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function deleteQuarantineAthome($id){
        try {
            DB::beginTransaction();
            $quarantineAthome = QuarantineAthome::where("object_id", $id)->first();
            $quarantineAthome->delete();
            $quarantineAthome->healtMonitorings()->delete();

            DB::commit();
            return true;
        }catch(\Exception $e){
            DB::rollBack();
            return false;
        }

    }

    public static function updateMedicalStaff($request){
        try {
            QuarantineAthome::where('id', $request->id)
                ->update(['medical_staff_id' => $request->medical_staff_id]);
            return true;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function updateStatusQuarantine($request){
        try {
            QuarantineAthome::where('id', $request->id)
                ->update(['status' => $request->status]);
            return true;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function confirmmm(){
        try {
            QuarantineAthome::where('status', 0)->update(['status' => 1]);
            return true;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function setData($quarantineAthome, $request){
        $injection = $request->injection;
        $object_quarantine_id = $request->object_quarantine_id;
        $quarantineAthome->guardian = $request->guardian;
        $quarantineAthome->relationship_of_guardian = $request->relationship_of_guardian;
        $quarantineAthome->phone_of_guardian = $request->phone_number_of_guardian;
        $quarantineAthome->injection = $injection;
        $quarantineAthome->object_quarantine_id = $object_quarantine_id;
        $quarantineAthome->total_date = QuarantineAthome::calulateTotalDate($injection, $object_quarantine_id);
        $quarantineAthome->quarantine_date = $request->quarantine_date;
    }

    public static function calulateTotalDate($injection, $object_quarantine_id){
        if ( $object_quarantine_id == 0){
            return 0;
        }
        if ( $object_quarantine_id == 8){
            return 7;
        } elseif ($object_quarantine_id == 5 && $injection < 2 || $object_quarantine_id == 7 && $injection < 2 || $object_quarantine_id == 2){
            return 14;
        } else {
            return 7;
        }
    }
}
