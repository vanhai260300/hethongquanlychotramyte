<?php

namespace App\Models;

use App\Http\Requests\RegisterUserRequest;
use http\Env\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;


class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    public function province(){
        return $this->belongsTo(Province::class, "province_id", "id");
    }
    public function district(){
        return $this->belongsTo(District::class, "district_id", "id");
    }
    public function medicalStation(){
        return $this->belongsTo(MedicalStation::class, "medical_station_id", "id");
    }
    public function ward(){
        return $this->belongsTo(Ward::class, "ward_id", "id");
    }
    public function HealthDeclarations(){
        return $this->belongsToMany(User::class, HealthDeclaration::class, 'user_id', 'object_id');
    }

    public function HealthDeclaration(){
        return $this->hasOne(HealthDeclaration::class, 'object_id');
    }

    public static function createUser($request){
        try {
            DB::beginTransaction();
            $token = strtoupper(Str::random(6));
            $user = new User();
            $user->password = bcrypt($request->password);
            User::create($user, $request);
            $user->token = $token;
            $user->save();
            $user = User::find($user->id);
            $user->user_id = $user->id;
            $user->save();
            DB::commit();
            return true;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function createUserWhenOther($request, $contentEmail)
    {
        try {
            DB::beginTransaction();
            $token = $contentEmail;
            $user = new User();
            User::create($user, $request);
            $user->email = "";
            $user->token = $token;
            $user->user_id = Auth::id();
            $user->save();
            DB::commit();
            return $user->id;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function updateUserWhenOther($id, $request)
    {
        try {
            DB::beginTransaction();
            $token = "Update";
            $user = User::find($id);
            User::create($user, $request);
            $user->token = $token;
            $user->save();
            DB::commit();
            return $user->id;
        } catch (\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    public static function create($user, $request){
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->CMND = $request->CMND;
        $user->birthdate = $request->birthdate;
        $user->gender = $request->gender;
        $user->province_id = $request->province_id;
        $user->district_id = $request->district_id;
        $user->ward_id = $request->ward_id;
        $user->medical_station_id = $request->medical_station_id;
        $user->address = $request->address;
    }
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
