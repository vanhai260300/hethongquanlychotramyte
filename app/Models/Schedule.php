<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Schedule extends Model
{
    use HasFactory;

    public function province(){
        return $this->belongsTo(Province::class, "province_id", "id");
    }

    public function district(){
        return $this->belongsTo(District::class, "district_id", "id");
    }

    public function ward(){
        return $this->belongsTo(Ward::class, "ward_id", "id");
    }

    public static function create($request){
        try {
            $schedule = new Schedule();
            $schedule->user_id = Auth::id();
            $schedule->name = $request->name;
            $schedule->from_date = $request->from_date;
            $schedule->to_date = $request->to_date;
            $schedule->province_id = $request->province_id;
            $schedule->district_id = $request->district_id;
            $schedule->ward_id = $request->ward_id;
            $schedule->address = $request->address;
            $schedule->note = $request->note;
            $schedule->save();
            return true;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function updates($id, $request){
        try {
            $schedule = Schedule::find($id);
            $schedule->name = $request->name;
            $schedule->from_date = $request->from_date;
            $schedule->to_date = $request->to_date;
            $schedule->province_id = $request->province_id;
            $schedule->district_id = $request->district_id;
            $schedule->ward_id = $request->ward_id;
            $schedule->address = $request->address;
            $schedule->note = $request->note;
            $schedule->save();
            return true;
        } catch (\Exception $e){
            return false;
        }

    }
}
