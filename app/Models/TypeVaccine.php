<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeVaccine extends Model
{
    use HasFactory;
    public $timestamps = false;

    public static function getAll(){
        return TypeVaccine::orderBy("name")->get();
    }
}
