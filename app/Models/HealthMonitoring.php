<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HealthMonitoring extends Model
{
    use HasFactory;

    public static function create($request){
        try {
            $healthMonitoring = new HealthMonitoring();
            $healthMonitoring->quarantine_id = $request->quarantine_id;
            HealthMonitoring::setData($healthMonitoring, $request);
            $healthMonitoring->save();
            return true;
        } catch (\Exception $e){
            return false;
        }

    }

    public static function updates($request){
        try {
            $healthMonitoring = HealthMonitoring::find($request->id);
            HealthMonitoring::setData($healthMonitoring, $request);
            $healthMonitoring->save();
            return true;
        } catch (\Exception $e){
            return false;
        }
    }

    public static function setData($healthMonitoring, $request){
        $healthMonitoring->date_moni = $request->date_moni;
        $healthMonitoring->temperature = $request->temperature;
        $healthMonitoring->breathing = $request->breathing;
        $healthMonitoring->blood_pressure = $request->blood_pressure;
        $healthMonitoring->fever = $request->fever;
        $healthMonitoring->cough = $request->cough;
        $healthMonitoring->difficulty_of_breathing = $request->difficulty_of_breathing;
        $healthMonitoring->diarrhea = $request->diarrhea;
        $healthMonitoring->chills = $request->chills;
        $healthMonitoring->tired = $request->tired;
        $healthMonitoring->loss_taste = $request->loss_taste;
        $healthMonitoring->suspicious = $request->suspicious;
        $healthMonitoring->note = $request->note;
    }
}
